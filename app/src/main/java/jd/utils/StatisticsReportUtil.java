package jd.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Process;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import base.net.NetConfig;
import base.net.open.RequestEntity;
import base.utils.StringToolBox;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLEncoder;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.List;

import cn.o2o.jingdongdaojia.MainActivity;
import jd.MyInfoShippingAddress;
import org.json.JSONException;
import org.json.JSONObject;

public class StatisticsReportUtil {
    private static final String DEFAULT_DEVICE_ID = "daojia";
    private static boolean already = false;
    static String deviceModel = "";
    static String mAndroidId = "";
    static String mBuildSerial = "";
    static String mImei = "";
    static String mProvidersName = "";
    static String mTraceid = "";
    private static String macAddress = null;
    static String netString = "";
    static String pageLevel = "";
    static BigInteger sUUIDMD5Decimal = null;
    static String screen = "";
    static String showVersionName = "";
    static String uuidMd5 = "";
    static String versionCode = "";
    static String versionName = "";

    public static void destroyDeviceInfoStr() {
        netString = "";
        uuidMd5 = "";
        versionName = "";
        deviceModel = "";
    }

    public static void initDeviceInfoStr(RequestEntity requestEntity) {
        String str = "";
        if (requestEntity.getParams() != null) {
            str = requestEntity.getParams().get("flutterVersion");
        }
        requestEntity.putParam("deviceId", getUUIDMD5());
        requestEntity.putParam("deviceToken", getUUIDMD5());
        requestEntity.putParam("brand", Build.BRAND);
        requestEntity.putParam("imei", getImei());
        requestEntity.putParam("androidId", getAndroidId());
        if (!TextUtils.isEmpty(str)) {
            requestEntity.putParam("flutterVersion", str);
            requestEntity.putParam("appVersion", str);
        } else {
            requestEntity.putParam("appVersion", getSimpleVersionName());
        }
        requestEntity.putParam("platCode", "android");
        requestEntity.putParam("appName", "Paidaojia");
        requestEntity.putParam("deviceModel", getdeviceModel());
        requestEntity.putParam("platVersion", Build.VERSION.RELEASE);
        requestEntity.putParam("channel", "");
        requestEntity.putParam("screen", getscreen());
        requestEntity.putParam("partner", "");
        requestEntity.putParam("traceId", getTranceid());
        MyInfoShippingAddress myInfoShippingAddress = null;
        if (myInfoShippingAddress != null) {
            requestEntity.putParam("lat", myInfoShippingAddress.getLatitude() + "");
            requestEntity.putParam("lng", myInfoShippingAddress.getLongitude() + "");
            requestEntity.putParam("poi", myInfoShippingAddress.getPoi() + "");
            if (myInfoShippingAddress.getCityId() > 0) {
                requestEntity.putParam("city_id", myInfoShippingAddress.getCityId() + "");
            }
        }
        requestEntity.putParam("networkType", "wifi");
        MyInfoShippingAddress myInfoShippingAddress2 = null;
        if (myInfoShippingAddress2 != null) {
            requestEntity.putParam("lat_pos", myInfoShippingAddress2.getLatitude() + "");
            requestEntity.putParam("lng_pos", myInfoShippingAddress2.getLongitude() + "");
        }
        NetConfig.subVersion = getVersionName();
        NetConfig.networkType = "wifi";
        NetConfig.deviceId = getUUIDMD5();
        NetConfig.deviceModel = Build.BRAND;
        NetConfig.carrierName = getProvidersName();
        NetConfig.currentChannle = "xiaomi";
    }

    public static String getdeviceModel() {
        if (TextUtils.isEmpty(deviceModel)) {
            deviceModel = spilitSubString(Build.MODEL, 12);
        }
        return deviceModel;
    }

    public static String getscreen() {
//        if (TextUtils.isEmpty(screen)) {
//            Display defaultDisplay = ((WindowManager) JDApplication.getInstance().getSystemService("window")).getDefaultDisplay();
//            screen = defaultDisplay.getWidth() + "*" + defaultDisplay.getHeight();
//        }
        return "";
    }

    public static void initDeviceInfoStr(JSONObject jSONObject) {
        try {
            jSONObject.put("deviceId", getUUIDMD5());
            jSONObject.put("deviceToken", getUUIDMD5());
            jSONObject.put("appVersion", getSimpleVersionName());
            jSONObject.put("platCode", "android");
            jSONObject.put("appName", "Paidaojia");
            jSONObject.put("platVersion", Build.VERSION.RELEASE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void initDeviceInfoStrSimplify(JSONObject jSONObject) {
        try {
            jSONObject.put("appVersion", getSimpleVersionName());
            jSONObject.put("appName", "Paidaojia");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
            return null;
        } catch (SocketException unused) {
            return null;
        }
    }

    public static String getUUIDMD5() {
        if (TextUtils.isEmpty(uuidMd5)) {
            uuidMd5 = MD5Calculator.calculateMD5(getUUID());
        }
        return uuidMd5;
    }

    public static String getPageLevelMD5() {
        if (TextUtils.isEmpty(pageLevel)) {
            pageLevel = MD5Calculator.calculateMD5(getImei() + System.currentTimeMillis());
        }
        return pageLevel;
    }

    public static String getUUIDMD5Decimal() {
        if (sUUIDMD5Decimal != null) {
            sUUIDMD5Decimal = new BigInteger(getUUIDMD5(), 16);
        }
        return sUUIDMD5Decimal.toString(10);
    }

    public static String getDeviceId() {
        String str;
//        try {
//            str = ((TelephonyManager) JDApplication.getInstance().getSystemService("phone")).getDeviceId();
//        } catch (Exception e) {
//            ThrowableExtension.printStackTrace(e);
//            str = null;
//        }
//        if (TextUtils.isEmpty(str)) {
//            str = getAndroidId();
//        }
//        if (!TextUtils.isEmpty(str)) {
//            return str;
//        }
        return DEFAULT_DEVICE_ID + System.currentTimeMillis();
    }

    public static String getAndroidId() {
//        if (TextUtils.isEmpty(mAndroidId)) {
//            try {
//                mAndroidId = Settings.Secure.getString(JDApplication.getInstance().getContentResolver(), "android_id");
//            } catch (Exception e) {
//                ThrowableExtension.printStackTrace(e);
//            }
//        }
        return mAndroidId;
    }

    public static String getBuildSerial() {
//        if (TextUtils.isEmpty(mBuildSerial)) {
//            if (Build.VERSION.SDK_INT >= 28) {
//                try {
//                    mBuildSerial = Build.getSerial();
//                } catch (Exception e) {
//                    ThrowableExtension.printStackTrace(e);
//                    CrashUtils.postBuildSerialError(e);
//                }
//            } else {
                mBuildSerial = Build.SERIAL;
//            }
//        }
        return mBuildSerial;
    }

    private static String getUUID() {
        return getDeviceId() + getBuildSerial();
    }

    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo[] allNetworkInfo;
//        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
//        if (!(connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null)) {
//            for (NetworkInfo state : allNetworkInfo) {
//                if (state.getState() == NetworkInfo.State.CONNECTED) {
//                    return true;
//                }
//            }
//        }
        return true;
    }

    public static String getSimCountryCode() {
//        try {
//            return ((TelephonyManager) JDApplication.getInstance().getSystemService("phone")).getSimCountryIso();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "";
//        }
        return "";
    }

    public static String getNetworkTypeName(Context context) {
        if (context == null) {
            return "UNKNOWN";
        }
        if (TextUtils.isEmpty(netString)) {
//            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
//            NetworkInfo[] allNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getAllNetworkInfo();
            int i = 0;
//            while (i < allNetworkInfo.length) {
//                try {
//                    if (allNetworkInfo[i].isConnected()) {
//                        if (allNetworkInfo[i].getTypeName().toUpperCase().contains("MOBILE")) {
//                            netString = telephonyManager.getNetworkType() + "";
//                        } else if (allNetworkInfo[i].getTypeName().toUpperCase().contains(k.f)) {
//                            netString = k.f;
//                        } else {
//                            netString = "UNKNOWN";
//                        }
//                    }
//                    i++;
//                } catch (Exception unused) {
//                    netString = "UNKNOWN";
//                }
//            }
            if (netString == null) {
                netString = "UNKNOWN";
            }
        }
        return netString;
    }

    public static final boolean isGpsOPen(Context context) {
//        return ((LocationManager) context.getSystemService(MsgType.SERVER_LOCATION)).isProviderEnabled("gps");
        return false;
    }

    private static String spilitSubString(String str, int i) {
        if (str == null) {
            return str;
        }
        try {
            if (str.length() > i) {
                return str.substring(0, i);
            }
            return str;
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }

    private static boolean isValidDeviceUUID(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String[] split = str.split("-");
        if (split.length > 1) {
            return !TextUtils.isEmpty(split[1]);
        }
        return false;
    }

    public static String getSimpleVersionName(Context context) {
        if (!TextUtils.isEmpty(versionName)) {
            return versionName;
        }
        PackageInfo packageInfo = getPackageInfo(context);
        if (packageInfo == null) {
            return "";
        }
        versionName = packageInfo.versionName;
        return versionName;
    }

    public static String getSimpleVersionName() {
        return "Paidaojia";
    }

    public static String getVersionName() {
        return "7.9.0";
    }

    public static String getVersionName(Context context) {
        return "7.9.0";
    }

    public static int getSoftwareVersionCode() {
        PackageInfo packageInfo = getPackageInfo();
        if (packageInfo == null) {
            return 0;
        }
        return packageInfo.versionCode;
    }

    private static PackageInfo getPackageInfo() {
        try {
            return getPackageInfo();
        } catch (Exception unused) {
            return null;
        }
    }

    private static PackageInfo getPackageInfo(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (Exception unused) {
            return null;
        }
    }

    public static String getBrand() {
        try {
            return spilitSubString(Build.MANUFACTURER, 12).replaceAll(" ", "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getModel() {
        try {
            return spilitSubString(Build.MODEL, 12).replaceAll(" ", "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static int getScreenWidth() {
        return 1080;
    }

    public static int getScreenHeight() {
        return 2110;
    }

    public static String getProcessName(Context context) {
        int myPid = Process.myPid();
//        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
//        if (runningAppProcesses == null) {
//            return null;
//        }
//        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
//            if (next.pid == myPid) {
//                return next.processName;
//            }
//        }
        return null;
    }



    public static String getImei() {
        if (!TextUtils.isEmpty(mImei)) {
            return mImei;
        }
        mImei = getDeviceId();
        return mImei;
    }

    public static String getTranceid() {
        if (!TextUtils.isEmpty(mTraceid)) {
            return mTraceid + System.currentTimeMillis();
        }
        mTraceid = getUUIDMD5() + System.currentTimeMillis();
        return mTraceid;
    }

    public static String getSign() {
        return "30819f300d06092a864886f70d010101050003818d00308189028181008c470af7c751ee12edbae8dd9e7c98fa60d3c631efa0f7172ed36c86bb85c8288391e718c05fdbef008d61f2e8fce4ef4457a69ae5a2fa53ead0c806c18f8b475847c07bf4451d82845efc30d5fc4aa2500f4bc84234a36749e83a9361c9ec89771a762e3d791eebf3154c2e95d06df95be68b4a4dcff33ef1ba5d6d90758b6d0203010001";
    }

    public static String getProvidersName() {
        if (!TextUtils.isEmpty(mProvidersName)) {
            return mProvidersName;
        }
        String str = null;
        try {
//            String subscriberId = ((TelephonyManager) JDApplication.getInstance().getApplicationContext().getSystemService("phone")).getSubscriberId();
//            if (subscriberId == null) {
//                return "unknow";
//            }
//            if (!subscriberId.startsWith("46000")) {
//                if (!subscriberId.startsWith("46002")) {
//                    if (subscriberId.startsWith("46001")) {
//                        str = "\u4e2d\u56fd\u8054\u901a";
//                    } else if (subscriberId.startsWith("46003")) {
//                        str = "\u4e2d\u56fd\u7535\u4fe1";
//                    }
//                    return URLEncoder.encode("" + str, "UTF-8");
//                }
//            }
            str = "\u4e2d\u56fd\u79fb\u52a8";
            return URLEncoder.encode("" + str, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}