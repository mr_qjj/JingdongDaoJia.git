package jd.utils;

import android.text.TextUtils;
import base.utils.StringToolBox;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Calculator {
    private static MessageDigest _digest;

    public static String calculateMD5(String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return "";
            }
            return calculateMD5(str.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static synchronized String calculateMD5(byte[] bArr) {
        synchronized (MD5Calculator.class) {
            try {
                _digest = MessageDigest.getInstance("MD5");
                _digest.reset();
                _digest.update(bArr);
                String hexString = StringToolBox.getHexString(_digest.digest());
                return hexString;
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return "";
            } catch (Exception e2) {
                e2.printStackTrace();
                return "";
            }
        }
    }
}