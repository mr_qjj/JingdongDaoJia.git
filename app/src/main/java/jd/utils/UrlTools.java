package jd.utils;

import android.text.TextUtils;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import org.json.JSONObject;

public class UrlTools {
    public static final String BODY = "body";
    public static final String DJ_SCHEME = "openApp.jddj:";
    public static final String DJ_WX_SCHEME = "wxe9aee36de8c7cb82:";
    public static final String NEED_LOGIN = "needLogin";
    public static final String NEED_PIN = "needPin";
    public static final String SHARE = "share";
    public static final String SHARE_URL = "shareUrl";
    public static final String TOKEN = "token";

    public static String getValue(String str, String str2) {
        int i;
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        String str3 = str.split("#")[0];
        int indexOf = str3.indexOf("?");
        String[] strArr = null;
        if (indexOf > 0 && indexOf < str3.length() - 1) {
            strArr = str3.substring(indexOf + 1).split("&");
        }
        if (strArr == null || strArr.length == 0) {
            return "";
        }
        for (String str4 : strArr) {
            int indexOf2 = str4.indexOf(61);
            if (indexOf2 >= 1 && str4.substring(0, indexOf2).equals(str2) && (i = indexOf2 + 1) < str4.length()) {
                try {
                    return URLDecoder.decode(str4.substring(i), "utf-8");
                } catch (Exception e) {
                   e.printStackTrace();
                }
            }
        }
        return "";
    }

    public static String getToken(String str) {
        try {
            String value = getValue(str, BODY);
            Log.e("getToken", "bodyStr=== " + value);
            JSONObject jSONObject = new JSONObject(value);
            if (!jSONObject.has("params")) {
                return "";
            }
            JSONObject jSONObject2 = new JSONObject(jSONObject.getString("params"));
            if (jSONObject2.has("token")) {
                return jSONObject2.getString("token");
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getValue1(String str, String str2) {
        try {
            str = URLDecoder.decode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        String str3 = str.split("#")[0];
        int indexOf = str3.indexOf("?");
        String[] strArr = null;
        if (indexOf > 0 && indexOf < str3.length() - 1) {
            strArr = str3.substring(indexOf + 1).split(",");
        }
        if (strArr == null || strArr.length == 0) {
            return "";
        }
        for (String str4 : strArr) {
            int indexOf2 = str4.indexOf(58);
            if (indexOf2 >= 1 && str4.substring(1, indexOf2 - 1).equals(str2)) {
                return str4.substring(indexOf2 + 2, str4.length() - 2);
            }
        }
        return "";
    }

    public static boolean isNeed(String str, String str2) {
        String value = getValue(str, str2);
        if (!TextUtils.isEmpty(value) && !"no".equals(value)) {
            return true;
        }
        return false;
    }

    public static String getShakeValue(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        int indexOf = str.indexOf("?");
        String[] split = (indexOf <= 0 || indexOf >= str.length() - 1) ? null : str.substring(indexOf + 1).split("&");
        if (split == null || split.length == 0) {
            return null;
        }
        for (String split2 : split) {
            String[] split3 = split2.split("=");
            if (split3 != null && split3.length == 2 && split3[0].equals(str2)) {
                return split3[1];
            }
        }
        return null;
    }
}