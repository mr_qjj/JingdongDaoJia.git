package jd.net;

import android.text.TextUtils;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

public class ASCIISortUtil {
    public static String formatQueryParaMap(Map<String, String> map, boolean b) {
        try {
            ArrayList arrayList = new ArrayList(map.entrySet());
            Collections.sort(arrayList, new Comparator<Map.Entry<String, String>>() {
                public int compare(Map.Entry<String, String> entry, Map.Entry<String, String> entry2) {
                    return entry.getKey().toString().compareTo(entry2.getKey());
                }
            });
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < arrayList.size(); i++) {
                Map.Entry entry = (Map.Entry) arrayList.get(i);
                if (entry.getKey() != null && !"".equals(entry.getKey()) && !TextUtils.isEmpty((CharSequence) entry.getValue())) {
                    String str = (String) entry.getKey();
                    String str2 = (String) entry.getValue();
                    if (b) {
                        str2 = URLEncoder.encode(str2, "UTF-8").replace("+", "%20");
                    }
                    if (!"functionId".equals(str) && !z.KEY_NEW_SIGN.equals(str)) {
                        stringBuffer.append(str2 + "&");
                    }
                }
            }
            String stringBuffer2 = stringBuffer.toString();
            return (stringBuffer2 == null || "".equals(stringBuffer2)) ? stringBuffer2 : stringBuffer2.substring(0, stringBuffer2.length() - 1);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}