package jd.net;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;

public class z {

    public static String host = "";
    public static final String KEY_NEW_SIGN = "signKeyV1";
    private static final String PLACE_HOLDER = "                                 ";
    String k1;

    public static native void a(Object obj);

    public static native int flushlog();

    public static native int initlog(String str, String str2, int i);

    public static native String jdk();

    public static native String k(byte[] bArr);

    public static native String k2(byte[] bArr);

    public static native String readlog(String str);

    public static native int wlog(int i, String str, long j);

    static {
        System.loadLibrary("jdpdj");
    }
    //计算签名
    public static void b(String body,Map<String,String> params) {
        params.put("signKey", k((body + PLACE_HOLDER).getBytes()));
        params.put("t", System.currentTimeMillis() + "");
        params.put("subVersion", "7.9.0.5");
        params.put(KEY_NEW_SIGN, k2((ASCIISortUtil.formatQueryParaMap(params, false) + PLACE_HOLDER).getBytes()));
    }


    public static String c() {
        return k2("{}                                 ".getBytes());
    }
    //获取最终url入口函数,调用此函数直接返回最终请求的url结果
    public static String returnResult(String url){
        host = url.substring(0,url.indexOf("?")+1);
        url = url.substring(url.indexOf("?")+1);
        Map<String,String> m = stringToMap(url);
        String body = m.get("body");
        b(body,m);
        return mapToString(m);
    }
    //String转Map
    public static Map<String,String> stringToMap(String str){
        Map<String,String> map = new HashMap<String,String>();
        String[] strings = str.split("&");
        for (String s : strings){
            map.put(s.split("=")[0],s.split("=")[1]);
        }
        return map;
    }
    //map转String
    public static String mapToString(Map<String,String> map){
        StringBuffer sb = new StringBuffer();
        for (String key : map.keySet()){
            sb.append(key+"="+map.get(key)+"&");
        }
        return host+sb.toString().substring(0,sb.toString().length()-1);
    }
}