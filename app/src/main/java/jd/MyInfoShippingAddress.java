package jd;

import java.io.Serializable;

public class MyInfoShippingAddress implements Serializable {
    public static final int FROM_LOCATION = 0;
    public static final int FROM_MY = 1;
    public static final int FROM_USER = 2;
    private static final long serialVersionUID = 1;
    private String adcode;
    private String addressDetail;
    private String addressName;
    private int addressType;
    public boolean canDelivery;
    private int cityId = -1;
    private String cityName;
    private String coordType;
    private int countyId = -1;
    private String countyName;
    private String createPin;
    private String createTime;
    private String email;
    private int from = 1;
    private String fullAddress;
    private String groupName;
    private int id;
    private long lastUsedTime;
    private double latitude;
    private double longitude;
    private String mobile;
    private String name;
    private String phone;
    private String pin;
    private String poi;
    private String postCode;
    private int quantityOfGoods;
    public boolean showTitle;
    private String tags;
    private String ts;
    private String updateTime;
    private int yn;

    public String getTags() {
        return this.tags;
    }

    public void setTags(String str) {
        this.tags = str;
    }

    public int getQuantityOfGoods() {
        return this.quantityOfGoods;
    }

    public void setQuantityOfGoods(int i) {
        this.quantityOfGoods = i;
    }

    public String getCoordType() {
        return this.coordType;
    }

    public void setCoordType(String str) {
        this.coordType = str;
    }

    public String getPostCode() {
        return this.postCode;
    }

    public void setPostCode(String str) {
        this.postCode = str;
    }

    public String getCreateTime() {
        return this.createTime;
    }

    public int getFrom() {
        return this.from;
    }

    public void setFrom(int i) {
        this.from = i;
    }

    public void setCreateTime(String str) {
        this.createTime = str;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void setGroupName(String str) {
        this.groupName = str;
    }

    public String getFullAddress() {
        return this.fullAddress;
    }

    public void setFullAddress(String str) {
        this.fullAddress = str;
    }

    public String getTs() {
        return this.ts;
    }

    public void setTs(String str) {
        this.ts = str;
    }

    public String getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(String str) {
        this.updateTime = str;
    }

    public int getCityId() {
        return this.cityId;
    }

    public void setCityId(int i) {
        this.cityId = i;
    }

    public String getCityName() {
        return this.cityName;
    }

    public void setCityName(String str) {
        this.cityName = str;
    }

    public int getAddressType() {
        return this.addressType;
    }

    public void setAddressType(int i) {
        this.addressType = i;
    }

    public String getAddressDetail() {
        return this.addressDetail;
    }

    public void setAddressDetail(String str) {
        this.addressDetail = str;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int i) {
        this.id = i;
    }

    public String getPoi() {
        return this.poi;
    }

    public void setPoi(String str) {
        this.poi = str;
    }

    public String getCountyName() {
        return this.countyName;
    }

    public void setCountyName(String str) {
        this.countyName = str;
    }

    public String getPin() {
        return this.pin;
    }

    public void setPin(String str) {
        this.pin = str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public int getYn() {
        return this.yn;
    }

    public void setYn(int i) {
        this.yn = i;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double d) {
        this.longitude = d;
    }

    public String getCreatePin() {
        return this.createPin;
    }

    public void setCreatePin(String str) {
        this.createPin = str;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double d) {
        this.latitude = d;
    }

    public int getCountyId() {
        return this.countyId;
    }

    public void setCountyId(int i) {
        this.countyId = i;
    }

    public long getLastUsedTime() {
        return this.lastUsedTime;
    }

    public void setLastUsedTime(long j) {
        this.lastUsedTime = j;
    }

    public String getAddressName() {
        return this.addressName;
    }

    public void setAddressName(String str) {
        this.addressName = str;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String str) {
        this.mobile = str;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String str) {
        this.phone = str;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String str) {
        this.email = str;
    }

    public boolean isCanDelivery() {
        return this.canDelivery;
    }

    public void setCanDelivery(boolean z) {
        this.canDelivery = z;
    }

    public boolean isShowTitle() {
        return this.showTitle;
    }

    public void setShowTitle(boolean z) {
        this.showTitle = z;
    }

    public String getAdcode() {
        return this.adcode;
    }

    public void setAdcode(String str) {
        this.adcode = str;
    }

    public String toString() {
        return "MyInfoShippingAddress [createTime=" + this.createTime + ", groupName=" + this.groupName + ", fullAddress=" + this.fullAddress + ", ts=" + this.ts + ", updateTime=" + this.updateTime + ", cityId=" + this.cityId + ", cityName=" + this.cityName + ", addressType=" + this.addressType + ", addressDetail=" + this.addressDetail + ", id=" + this.id + ", poi=" + this.poi + ", countyName=" + this.countyName + ", pin=" + this.pin + ", name=" + this.name + ", yn=" + this.yn + ", longitude=" + this.longitude + ", createPin=" + this.createPin + ", latitude=" + this.latitude + ", countyId=" + this.countyId + ", lastUsedTime=" + this.lastUsedTime + ", addressName=" + this.addressName + ", mobile=" + this.mobile + ", coordType=" + this.coordType + ", tags=" + this.tags + ", from=" + this.from + "]";
    }

    public MyInfoShippingAddress clone() {
        MyInfoShippingAddress myInfoShippingAddress = new MyInfoShippingAddress();
        myInfoShippingAddress.setCreateTime(new String(getCreateTime()));
        myInfoShippingAddress.setGroupName(new String(getGroupName()));
        myInfoShippingAddress.setFullAddress(new String(getFullAddress()));
        myInfoShippingAddress.setTs(new String(getTs()));
        myInfoShippingAddress.setUpdateTime(new String(getUpdateTime()));
        myInfoShippingAddress.setCityId(getCityId());
        myInfoShippingAddress.setCityName(new String(getCityName()));
        myInfoShippingAddress.setAddressType(getAddressType());
        myInfoShippingAddress.setAddressDetail(new String(getAddressDetail()));
        myInfoShippingAddress.setId(getId());
        myInfoShippingAddress.setPoi(new String(getPoi()));
        myInfoShippingAddress.setCountyName(new String(getCountyName()));
        myInfoShippingAddress.setPin(new String(getPin()));
        myInfoShippingAddress.setName(new String(getName()));
        myInfoShippingAddress.setYn(getYn());
        myInfoShippingAddress.setLongitude(getLongitude());
        myInfoShippingAddress.setCreatePin(new String(getCreatePin()));
        myInfoShippingAddress.setLatitude(getLatitude());
        myInfoShippingAddress.setCountyId(getCountyId());
        myInfoShippingAddress.setLastUsedTime(getLastUsedTime());
        myInfoShippingAddress.setAddressName(new String(getAddressName()));
        myInfoShippingAddress.setMobile(new String(getMobile()));
        myInfoShippingAddress.setPhone(new String(getPhone()));
        myInfoShippingAddress.setEmail(new String(getEmail()));
        myInfoShippingAddress.setCoordType(new String(getCoordType()));
        myInfoShippingAddress.setPostCode(new String(getPostCode()));
        myInfoShippingAddress.setQuantityOfGoods(getQuantityOfGoods());
        myInfoShippingAddress.setCanDelivery(isCanDelivery());
        myInfoShippingAddress.setShowTitle(isShowTitle());
        myInfoShippingAddress.setTags(new String(this.tags));
        myInfoShippingAddress.setAdcode(new String(this.adcode));
        return myInfoShippingAddress;
    }
}