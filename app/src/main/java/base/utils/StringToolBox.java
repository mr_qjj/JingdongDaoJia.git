package base.utils;

import java.io.UnsupportedEncodingException;

public class StringToolBox {
    static final byte[] HEX_CHAR_TABLE = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    public static String getHexString(byte[] bArr) {
        byte[] bArr2 = new byte[(bArr.length * 2)];
        int i = 0;
        for (byte b : bArr) {
            byte b2 = (byte)(b & 255);
            int i2 = i + 1;
            byte[] bArr3 = HEX_CHAR_TABLE;
            bArr2[i] = bArr3[b2 >>> 4];
            i = i2 + 1;
            bArr2[i2] = bArr3[b2 & 15];
        }
        try {
            return new String(bArr2, "ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static boolean isChinesePrefix(String str) {
        char charAt;
        if (str == null || str.length() == 0 || 19968 > (charAt = str.charAt(0)) || charAt > 40869) {
            return false;
        }
        return true;
    }
}