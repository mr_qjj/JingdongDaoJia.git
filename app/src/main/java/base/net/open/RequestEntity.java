package base.net.open;

import java.util.HashMap;
import java.util.Map;
import jd.utils.UrlTools;
import org.json.JSONObject;

public class RequestEntity {
//    public CookieListener cookieListener;
    private Map<String, String> headParams = new HashMap();
    public boolean isChangedHostToIp = false;
    public boolean isHandleLogin = true;
    public JSONObject json;
    public String mHost = "";
    public int method = 0;
    private Map<String, String> params = new HashMap();
    public String url;

    public RequestEntity(String str, JSONObject jSONObject) {
        this.url = str;
        this.json = jSONObject;
        if (this.json == null) {
            this.json = new JSONObject();
        }
    }

    public Map<String, String> getHeadParams() {
        return this.headParams;
    }

    public Map<String, String> getParams() {
        return this.params;
    }

    public void putParam(String str, String str2) {
        this.params.put(str, str2);
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.url);
        stringBuffer.append("?");
        for (Map.Entry next : this.params.entrySet()) {
            stringBuffer.append((String) next.getKey());
            stringBuffer.append("=");
            stringBuffer.append((String) next.getValue());
            stringBuffer.append("&");
        }
        stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        return stringBuffer.toString();
    }

    public void refreshBody() {
        putParam(UrlTools.BODY, this.json.toString());
    }
}