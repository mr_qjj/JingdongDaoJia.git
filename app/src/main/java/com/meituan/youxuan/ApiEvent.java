package com.meituan.youxuan;

import android.util.Log;

import com.alibaba.fastjson.JSONObject;
import com.o2o.tools.InitXposedHook;
import com.o2o.tools.MyThreadLocal;
import com.o2o.tools.Utils;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Map;

import de.robv.android.xposed.XposedHelpers;

public class ApiEvent {

    /**
     * 获取当前类对象
     * @return
     */
    public static ApiEvent get(){
        return new ApiEvent();
    }

    /**
     * 获取当前app的分类数据
     * @param youxuanParamMap
     * @param responseMap
     * @param youxuanObject
     * @param clientParam
     */
    public void getCategory(Map<String,String> youxuanParamMap,Map<String,Object> responseMap, Object youxuanObject, JSONObject clientParam){
        try {
            String poiId = clientParam.getString("poiId");
            String cityId = clientParam.getString("cityId");
            if(poiId == null){
                   poiId = youxuanParamMap.get("poi");
            }
            if(cityId == null){
                cityId = youxuanParamMap.get("ci");
            }
            String categoryUrlApi = "https://grocery-app.meituan.com/api/c/poi/" + poiId + "/category/list/v4?";
            categoryUrlApi += "uuid=" + youxuanParamMap.get("uuid") + "&utm_medium=" + youxuanParamMap.get("utm_medium");
            categoryUrlApi += "&brand=" + youxuanParamMap.get("brand") + "&tenantId=" + youxuanParamMap.get("tenantId");
            categoryUrlApi += "&utm_term=" + youxuanParamMap.get("utm_term") + "&device_model=" + youxuanParamMap.get("device_model");
            categoryUrlApi += "&poi=" + poiId + "&type=1";
            categoryUrlApi += "&poiId=" + poiId + "&stockPois=" + youxuanParamMap.get("stockPois");
            categoryUrlApi += "&ci=" + cityId + "&bizId=" + youxuanParamMap.get("bizId");
            categoryUrlApi += "&fp_user_id=" + youxuanParamMap.get("fp_user_id") + "&sysName=" + youxuanParamMap.get("sysName");
            categoryUrlApi += "&sysVerion=" + youxuanParamMap.get("sysVerion") + "&app_version=" + youxuanParamMap.get("app_version") + "&app_tag=" + youxuanParamMap.get("app_tag");
            MyThreadLocal.getInstance().set("");
            java.lang.reflect.Method getResponse = youxuanObject.getClass().getDeclaredMethod("getResponseWithInterceptorChain", InitXposedHook.youxuanRequestObject.getClass());
            getResponse.setAccessible(true);
            //修改头部数据的执行时间
            Utils.modifyHeader(InitXposedHook.youxuanRequestObject, "retrofit_exec_time", String.valueOf(System.currentTimeMillis()));
            //修改请求中的url目标地址
            Utils.modifyValue(InitXposedHook.youxuanRequestObject, "url", categoryUrlApi);
            //设置请求方法 GET
            Field methodField = InitXposedHook.youxuanRequestObject.getClass().getDeclaredField("method");
            methodField.setAccessible(true);
            methodField.set(InitXposedHook.youxuanRequestObject,"GET");
            //修改 请求体
            Utils.modifyValue(InitXposedHook.youxuanRequestObject, "body", null);
            getResponse.invoke(youxuanObject, InitXposedHook.youxuanRequestObject);
            String res = MyThreadLocal.getInstance().get();
            responseMap.put("msg", res);
            responseMap.put("code", 200);
            responseMap.put("cityId",youxuanParamMap.get("ci"));
            responseMap.put("poiId",youxuanParamMap.get("poiId"));
            responseMap.put("optParam", JSONObject.toJSONString(InitXposedHook.youxuanParamMap));
        }catch(Exception e){
            e.printStackTrace();
            responseMap.put("msg",e.getMessage());
            responseMap.put("code",500);
        }
    }

    /**
     * 获取首页分类list
     * @param youxuanParamMap
     * @param responseMap
     * @param youxuanObject
     * @param clientParam
     */
    public void getHomeCategory(Map<String,String> youxuanParamMap,Map<String,Object> responseMap, Object youxuanObject, JSONObject clientParam){
        try {
            String poiId = clientParam.getString("poiId");
            String cityId = clientParam.getString("cityId");
            if(poiId == null){
                poiId = youxuanParamMap.get("poi");
            }
            if(cityId == null){
                cityId = youxuanParamMap.get("ci");
            }

            String categoryUrlApi = "https://grocery-mtapp.meituan.com/api/c/homepage/aggregation/includelocation?";
            categoryUrlApi += "uuid=" + youxuanParamMap.get("uuid") + "&utm_medium=" + youxuanParamMap.get("utm_medium");
            categoryUrlApi += "&brand=" + youxuanParamMap.get("brand") + "&tenantId=" + youxuanParamMap.get("tenantId");
            categoryUrlApi += "&prefetch=" + youxuanParamMap.get("prefetch") + "&spotId=" + youxuanParamMap.get("spotId");
            categoryUrlApi += "&selectedTabTime=" + youxuanParamMap.get("selectedTabTime") + "&app_tag=" + youxuanParamMap.get("app_tag");
            categoryUrlApi += "&strategy=" + youxuanParamMap.get("strategy") + "&mmp=" + youxuanParamMap.get("mmp");
            categoryUrlApi += "&utm_term=" + youxuanParamMap.get("utm_term") + "&appid=" + youxuanParamMap.get("appid");
            categoryUrlApi += "&sysName=" + youxuanParamMap.get("sysName");
            categoryUrlApi += "&poi=" + poiId;
            categoryUrlApi += "&ci=" + cityId + "&sysVerion=" + youxuanParamMap.get("sysVerion");
            categoryUrlApi += "&token=" + youxuanParamMap.get("token") + "&timestamp=" + System.currentTimeMillis();
            categoryUrlApi += "&scene=" + youxuanParamMap.get("scene");
            MyThreadLocal.getInstance().set("");
            java.lang.reflect.Method getResponse = youxuanObject.getClass().getDeclaredMethod("getResponseWithInterceptorChain", InitXposedHook.youxuanRequestObject.getClass());
            getResponse.setAccessible(true);
            //修改头部数据的执行时间
            Utils.modifyHeader(InitXposedHook.youxuanRequestObject, "retrofit_exec_time", String.valueOf(System.currentTimeMillis()));
            //修改请求中的url目标地址
            Utils.modifyValue(InitXposedHook.youxuanRequestObject, "url", categoryUrlApi);
            //设置请求方法 GET
            Field methodField = InitXposedHook.youxuanRequestObject.getClass().getDeclaredField("method");
            methodField.setAccessible(true);
            methodField.set(InitXposedHook.youxuanRequestObject,"GET");
            //修改 请求体
            Utils.modifyValue(InitXposedHook.youxuanRequestObject, "body", null);

            getResponse.invoke(youxuanObject, InitXposedHook.youxuanRequestObject);
            String res = MyThreadLocal.getInstance().get();
            responseMap.put("msg", res);
            responseMap.put("code", 200);
            responseMap.put("cityId",youxuanParamMap.get("ci"));
            responseMap.put("poiId",youxuanParamMap.get("poiId"));
            responseMap.put("optParam", JSONObject.toJSONString(InitXposedHook.youxuanParamMap));
        }catch(Exception e){
            e.printStackTrace();
            responseMap.put("msg",e.getMessage());
            responseMap.put("code",500);
        }
    }
    /**
     * 获取分类商品列表
     * @param youxuanParamMap
     * @param responseMap
     * @param youxuanObject
     * @param clientParam
     */
    public void getItemList(Map<String,String> youxuanParamMap,Map<String,Object> responseMap, Object youxuanObject, JSONObject clientParam) {
        String poiId = clientParam.getString("poiId");
        String cityId = clientParam.getString("cityId");
        String categoryId = clientParam.getString("categoryId");
        String skus = clientParam.getString("skus");
        int offset = 0;
        if(poiId == null){
            poiId = youxuanParamMap.get("poi");
        }
        if(cityId == null){
            cityId = youxuanParamMap.get("ci");
        }
        if (categoryId != null){
            if (skus == null || "".equals(skus)){
                skus = "";
            }else{
                skus =  URLDecoder.decode(skus);
                offset = skus.split(",").length;
            }
            try {
                String itemListUrlApi = "https://grocery-app.meituan.com/api/c/poi/" + poiId + "/sku/list/category/" + categoryId + "/v4?";
                itemListUrlApi += "uuid=" + youxuanParamMap.get("uuid") + "&utm_medium=" + youxuanParamMap.get("utm_medium");
                itemListUrlApi += "&brand=" + youxuanParamMap.get("brand") + "&tenantId=" + youxuanParamMap.get("tenantId");
                itemListUrlApi += "&utm_term=" + youxuanParamMap.get("utm_term") + "&device_model=" + youxuanParamMap.get("device_model");
                itemListUrlApi += "&firstCategoryId=" + categoryId;

                itemListUrlApi += "&poi=" + poiId + "&type=1";
                itemListUrlApi += "&poiId=" + poiId + "&categoryId="+categoryId;
                itemListUrlApi += "&listTag=1&isLastSecondCategory=false&limit=15&offset=" + offset;
                itemListUrlApi += "&excludeSkuIds=" + URLEncoder.encode(skus);

                itemListUrlApi += "&stockPois=" + youxuanParamMap.get("stockPois");
                itemListUrlApi += "&ci=" + cityId + "&bizId=" + youxuanParamMap.get("bizId");
                itemListUrlApi += "&fp_user_id=" + youxuanParamMap.get("fp_user_id") + "&sysName=" + youxuanParamMap.get("sysName");
                itemListUrlApi += "&sysVerion=" + youxuanParamMap.get("sysVerion") + "&app_version=" + youxuanParamMap.get("app_version") + "&app_tag=" + youxuanParamMap.get("app_tag");
                itemListUrlApi += "&optimus_uuid=&optimus_risk_level=71&optimus_code=10";
                MyThreadLocal.getInstance().set("");
                java.lang.reflect.Method getResponse = youxuanObject.getClass().getDeclaredMethod("getResponseWithInterceptorChain", InitXposedHook.youxuanRequestObject.getClass());
                getResponse.setAccessible(true);
                //修改头部数据的执行时间
                Utils.modifyHeader(InitXposedHook.youxuanRequestObject, "retrofit_exec_time", String.valueOf(System.currentTimeMillis()));
                //修改请求中的url目标地址
                Utils.modifyValue(InitXposedHook.youxuanRequestObject, "url", itemListUrlApi);
                //设置请求方法 GET
                Field methodField = InitXposedHook.youxuanRequestObject.getClass().getDeclaredField("method");
                methodField.setAccessible(true);
                methodField.set(InitXposedHook.youxuanRequestObject,"GET");
                //修改 请求体
                Utils.modifyValue(InitXposedHook.youxuanRequestObject, "body", null);

                getResponse.invoke(youxuanObject, InitXposedHook.youxuanRequestObject);
                String res = MyThreadLocal.getInstance().get();
                responseMap.put("msg", res);
                responseMap.put("code", 200);
                responseMap.put("cityId", youxuanParamMap.get("ci"));
                responseMap.put("poiId", youxuanParamMap.get("poiId"));
                responseMap.put("optParam", JSONObject.toJSONString(InitXposedHook.youxuanParamMap));
            }catch(Exception e){
                e.printStackTrace();
                responseMap.put("msg",e.getMessage());
                responseMap.put("code",500);
            }
        }else{
            responseMap.put("msg","未传入分类ID,请传入\"categoryId\"后重新发起请求.");
            responseMap.put("code",500);
        }
    }


    /**
     * 获取主页分类商品列表
     * @param youxuanParamMap
     * @param responseMap
     * @param youxuanObject
     * @param clientParam
     */
    public void getHomeItemList(Map<String,String> youxuanParamMap,Map<String,Object> responseMap, Object youxuanObject, JSONObject clientParam) {
        String poiId = clientParam.getString("poiId");
        String cityId = clientParam.getString("cityId");
        String categoryId = clientParam.getString("categoryId");
        String latitude = clientParam.getString("lat");
        String longitude = clientParam.getString("lon");
        String skus = clientParam.getString("skus");
        int offset = 0;
        if(poiId == null){
            poiId = youxuanParamMap.get("poi");
        }
        if(latitude == null){
            latitude = youxuanParamMap.get("homepageLat");
        }
        if(longitude == null){
            longitude = youxuanParamMap.get("homepageLng");
        }
        if(cityId == null){
            cityId = youxuanParamMap.get("ci");
        }
        if (categoryId != null){
            if (skus == null || "".equals(skus)){
                skus = "";
            }else{
                skus =  URLDecoder.decode(skus);
                offset = skus.split(",").length;
            }
            try {

                String itemListUrlApi = "https://grocery-app.meituan.com/api/c/poi/" + poiId + "/sku/list/category/" + categoryId + "/speedApp/v1?";
                itemListUrlApi += "uuid=" + youxuanParamMap.get("uuid") + "&utm_medium=" + youxuanParamMap.get("utm_medium");
                itemListUrlApi += "&brand=" + youxuanParamMap.get("brand") + "&tenantId=" + youxuanParamMap.get("tenantId");
                itemListUrlApi += "&homepageLng=null&homepageLat=null";
                itemListUrlApi += "&utm_term=" + youxuanParamMap.get("utm_term") + "&device_model=" + youxuanParamMap.get("device_model");
                itemListUrlApi += "&tabType=2&poi=" + poiId + "&stockPois=" + poiId + "&ci=" + cityId + "&bizId=" + youxuanParamMap.get("bizId");
                itemListUrlApi += "&fp_user_id=" + youxuanParamMap.get("fp_user_id") + "&sysName=" + youxuanParamMap.get("sysName");
                itemListUrlApi += "&sysVerion=" + youxuanParamMap.get("sysVerion") + "&app_version=" + youxuanParamMap.get("app_version") + "&app_tag=" + youxuanParamMap.get("app_tag");

                String data = "{\"offset\":0,\"limit\":6,\"categoryId\":\"42516\",\"poiId\":143104549503257,\"tabType\":2,\"excludeSkuIds\":[],\"firstCategoryId\":\"42516\",\"isLastSecondCategory\":false}";

                Class<?> clazz = InitXposedHook.youxuanClassLoader.loadClass("com.sankuai.meituan.retrofit2.RequestBodyBuilder");
                Method resqBodyMethod = clazz.getDeclaredMethod("build",byte[].class,String.class);
                Object requestBody = resqBodyMethod.invoke(null,data.getBytes("UTF-8"),"application/json");


                //修改头部数据的执行时间
                Utils.modifyHeader(InitXposedHook.youxuanRequestObject, "retrofit_exec_time", String.valueOf(System.currentTimeMillis()));
                //修改请求中的url目标地址
                Utils.modifyValue(InitXposedHook.youxuanRequestObject, "url", itemListUrlApi);
                //修改 请求体 当请求为post的时候,必须设置此对象以及内容

                Log.e("EDXposed",InitXposedHook.youxuanRequestBodyObject.toString());
                Utils.modifyValue(InitXposedHook.youxuanRequestObject, "body", requestBody);
                Class<?> cla = InitXposedHook.youxuanRequestObject.getClass();
                Field d = cla.getDeclaredField("body");
                d.setAccessible(true);
                Log.e("EDXposed",d.get(InitXposedHook.youxuanRequestObject).toString());


                //设置请求方法 POST
                Field methodField = InitXposedHook.youxuanRequestObject.getClass().getDeclaredField("method");
                methodField.setAccessible(true);
                methodField.set(InitXposedHook.youxuanRequestObject,"POST");


                MyThreadLocal.getInstance().set("");
                java.lang.reflect.Method getResponse = youxuanObject.getClass().getDeclaredMethod("getResponseWithInterceptorChain", InitXposedHook.youxuanRequestObject.getClass());
                getResponse.setAccessible(true);


                getResponse.invoke(youxuanObject, InitXposedHook.youxuanRequestObject);
                String res = MyThreadLocal.getInstance().get();


                responseMap.put("msg", res);
                responseMap.put("code", 200);
                responseMap.put("cityId", youxuanParamMap.get("ci"));
                responseMap.put("poiId", youxuanParamMap.get("poiId"));
                responseMap.put("optParam", JSONObject.toJSONString(InitXposedHook.youxuanParamMap));
            }catch(Exception e){
                e.printStackTrace();
                responseMap.put("msg",e.getMessage());
                responseMap.put("code",500);
            }
        }else{
            responseMap.put("msg","未传入分类ID,请传入\"categoryId\"后重新发起请求.");
            responseMap.put("code",500);
        }
    }




    /**
     * 获取城市列表
     * @param youxuanParamMap
     * @param responseMap
     * @param youxuanObject
     * @param clientParam
     */
    public void getCityList(Map<String,String> youxuanParamMap,Map<String,Object> responseMap, Object youxuanObject, JSONObject clientParam) {
        try {
            String cityListApiUrl = "https://grocery-app.meituan.com/api/c/grocerylbs/getOpeningCityList?";
            cityListApiUrl += "uuid=" + youxuanParamMap.get("uuid") + "&utm_medium=" + youxuanParamMap.get("utm_medium");
            cityListApiUrl += "&brand=" + youxuanParamMap.get("brand") + "&tenantId=" + youxuanParamMap.get("tenantId");
            cityListApiUrl += "&utm_term=" + youxuanParamMap.get("utm_term") + "&device_model=" + youxuanParamMap.get("device_model");
            cityListApiUrl += "&poi=" + youxuanParamMap.get("poi") + "&stockPois=" + youxuanParamMap.get("stockPois");
            cityListApiUrl += "&ci=" + youxuanParamMap.get("ci") + "&bizId=" + youxuanParamMap.get("bizId");
            cityListApiUrl += "&fp_user_id=" + youxuanParamMap.get("fp_user_id") + "&sysName=" + youxuanParamMap.get("sysName");
            cityListApiUrl += "&sysVerion=" + youxuanParamMap.get("sysVerion") + "&app_version=" + youxuanParamMap.get("app_version") + "&app_tag=" + youxuanParamMap.get("app_tag");
            MyThreadLocal.getInstance().set("");
            java.lang.reflect.Method getResponse = youxuanObject.getClass().getDeclaredMethod("getResponseWithInterceptorChain", InitXposedHook.youxuanRequestObject.getClass());
            getResponse.setAccessible(true);
            //修改头部数据的执行时间
            Utils.modifyHeader(InitXposedHook.youxuanRequestObject, "retrofit_exec_time", String.valueOf(System.currentTimeMillis()));
            //修改请求中的url目标地址
            Utils.modifyValue(InitXposedHook.youxuanRequestObject, "url", cityListApiUrl);
            //设置请求方法 GET
            Field methodField = InitXposedHook.youxuanRequestObject.getClass().getDeclaredField("method");
            methodField.setAccessible(true);
            methodField.set(InitXposedHook.youxuanRequestObject,"GET");

            getResponse.invoke(youxuanObject, InitXposedHook.youxuanRequestObject);
            String res = MyThreadLocal.getInstance().get();
            responseMap.put("msg", res);
            responseMap.put("code", 200);
            responseMap.put("cityId",youxuanParamMap.get("ci"));
            responseMap.put("poiId",youxuanParamMap.get("poiId"));
            responseMap.put("optParam", JSONObject.toJSONString(InitXposedHook.youxuanParamMap));
        }catch(Exception e){
            e.printStackTrace();
            responseMap.put("msg",e.getMessage());
            responseMap.put("code",500);
        }
    }

    /**
     * 搜索团长列表
     * @param youxuanParamMap
     * @param responseMap
     * @param youxuanObject
     * @param clientParam
     */
    public void searchPointList(Map<String,String> youxuanParamMap,Map<String,Object> responseMap, Object youxuanObject, JSONObject clientParam) {
        String cityId = clientParam.getString("cityId");
        String keyWord = clientParam.getString("keyWord");
        if(cityId == null){
            cityId = "-1";
        }
        if(keyWord == null || "".equals(keyWord)){
            responseMap.put("msg", "必传参数keyWord不能为空,请传入[keyWord]参数后重新请求...");
            responseMap.put("code", 500);
        }else {
            try {
                keyWord = URLEncoder.encode(keyWord,"utf-8");
                String cityListApiUrl = "https://grocery-app.meituan.com/api/c/grocerylbs/keyword/searchPoi?";
                cityListApiUrl += "uuid=" + youxuanParamMap.get("uuid") + "&utm_medium=" + youxuanParamMap.get("utm_medium");
                cityListApiUrl += "&brand=" + youxuanParamMap.get("brand") + "&tenantId=" + youxuanParamMap.get("tenantId");
                cityListApiUrl += "&utm_term=" + youxuanParamMap.get("utm_term") + "&device_model=" + youxuanParamMap.get("device_model");
                cityListApiUrl += "&poi=" + youxuanParamMap.get("poi") + "&stockPois=" + youxuanParamMap.get("stockPois");
                cityListApiUrl += "&ci=" + cityId + "&bizId=" + youxuanParamMap.get("bizId");
                cityListApiUrl += "&fp_user_id=" + youxuanParamMap.get("fp_user_id") + "&sysName=" + youxuanParamMap.get("sysName");
                cityListApiUrl += "&sysVerion=" + youxuanParamMap.get("sysVerion") + "&app_version=" + youxuanParamMap.get("app_version");
                cityListApiUrl += "&app_tag=" + youxuanParamMap.get("app_tag");
                cityListApiUrl += "&districtId=-1&districtType=-1&cityId="+cityId+"&keyword="+keyWord;
                MyThreadLocal.getInstance().set("");
                java.lang.reflect.Method getResponse = youxuanObject.getClass().getDeclaredMethod("getResponseWithInterceptorChain", InitXposedHook.youxuanRequestObject.getClass());
                getResponse.setAccessible(true);
                //修改头部数据的执行时间
                Utils.modifyHeader(InitXposedHook.youxuanRequestObject, "retrofit_exec_time", String.valueOf(System.currentTimeMillis()));
                //修改请求中的url目标地址
                Utils.modifyValue(InitXposedHook.youxuanRequestObject, "url", cityListApiUrl);
                //设置请求方法 GET
                Field methodField = InitXposedHook.youxuanRequestObject.getClass().getDeclaredField("method");
                methodField.setAccessible(true);
                methodField.set(InitXposedHook.youxuanRequestObject,"GET");

                getResponse.invoke(youxuanObject, InitXposedHook.youxuanRequestObject);
                String res = MyThreadLocal.getInstance().get();
                responseMap.put("msg", res);
                responseMap.put("code", 200);
                responseMap.put("cityId", youxuanParamMap.get("ci"));
                responseMap.put("poiId", youxuanParamMap.get("poiId"));
                responseMap.put("optParam", JSONObject.toJSONString(InitXposedHook.youxuanParamMap));
            } catch (Exception e) {
                e.printStackTrace();
                responseMap.put("msg", e.getMessage());
                responseMap.put("code", 500);
            }
        }
    }
}
