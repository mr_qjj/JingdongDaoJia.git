package com.meituan.youxuan;

import android.util.Log;

import java.util.Map;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;

public class Dianping {

    public static Object poiDetailServiceImpl;
    public static Object scApiServiceImpl;
    public static Object shopApiServiceImpl;
    public static Object waimaiSearchServiceImpl;
    public static Object sCSearchApiServiceImpl;


    public static Dianping getInstance() {
        return new Dianping();
    }

    /**
     * 获取 com.sankuai.waimai.store.base.net.sg.SCApiService 动态代理实现类
     *
     * @param result
     */
    public void setSCApiService(Object result) {
        scApiServiceImpl = result;
        XposedHelpers.findAndHookMethod(result.getClass(), "getShopMenuV8",
                long.class, Long.class,
                int.class, Long.class,
                String.class, String.class, new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        super.beforeHookedMethod(param);
                        Log.e("EDXposed", "参数1:\t" + param.args[0].toString());
                        Log.e("EDXposed", "参数2:\t" + param.args[1].toString());
                        Log.e("EDXposed", "参数3:\t" + param.args[2].toString());
                        Log.e("EDXposed", "参数4:\t" + param.args[3].toString());
                        Log.e("EDXposed", "参数5:\t" + param.args[4].toString());
                        Log.e("EDXposed", "参数6:\t" + param.args[5].toString());
                    }

                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        super.afterHookedMethod(param);
                    }
                });
        /**
         * 获取店铺评分信息
         */
//        XposedHelpers.findAndHookMethod(result.getClass(), "getComments", long.class,
//                int.class,
//                int.class,
//                int.class,
//                long.class, new XC_MethodHook() {
//                    @Override
//                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
//                        super.beforeHookedMethod(param);
//
//                    }
//                });
    }

    /**
     * 获取 com.sankuai.waimai.business.restaurant.base.repository.net.ShopApiService 动态代理实现类
     *
     * @param result
     */
    public void setShopApiService(Object result) {
        shopApiServiceImpl = result;
//        Method[] mts = result.getClass().getDeclaredMethods();
//        for(Method m: mts){
//            Log.e("EDXposed", m.getName());
//        }
        XposedHelpers.findAndHookMethod(result.getClass(), "getShopMenu",
                String.class,
                long.class,
                Long.class,
                int.class,
                String.class,
                String.class,
                int.class,
                String.class,
                int.class,
                String.class,
                String.class,
                String.class,
                String.class,
                String.class,
                String.class,
                String.class,
                new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        super.beforeHookedMethod(param);
                        Log.e("EDXposed", param.args[0].toString());
                        Log.e("EDXposed", String.valueOf(param.args[1]));
                        Log.e("EDXposed", String.valueOf(param.args[2]));
                        Log.e("EDXposed", String.valueOf(param.args[3]));
                        Log.e("EDXposed", String.valueOf(param.args[4]));
                        Log.e("EDXposed", String.valueOf(param.args[5]));
                        Log.e("EDXposed", String.valueOf(param.args[6]));
                        Log.e("EDXposed", String.valueOf(param.args[7]));
                        Log.e("EDXposed", String.valueOf(param.args[8]));
                        Log.e("EDXposed", String.valueOf(param.args[9]));
                        Log.e("EDXposed", String.valueOf(param.args[10]));
                        Log.e("EDXposed", String.valueOf(param.args[11]));
                        Log.e("EDXposed", String.valueOf(param.args[12]));
                        Log.e("EDXposed", String.valueOf(param.args[13]));
                        Log.e("EDXposed", String.valueOf(param.args[14]));
                        Log.e("EDXposed", String.valueOf(param.args[15]));
                    }

                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        super.afterHookedMethod(param);
                    }
                });
    }

    /**
     * 获取 com.meituan.android.food.retrofit.FoodApiService$PoiDetailService 动态代理实现类
     *
     * @param result
     */
    public void setPoiDetailService(Object result) {
        poiDetailServiceImpl = result;
        XposedHelpers.findAndHookMethod(result.getClass(), "getBasePoiDetail",
                long.class, Map.class,
                new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        super.beforeHookedMethod(param);
                        Log.e("EDXposed", param.args[0].toString());
                        Map<String, Object> map = (Map<String, Object>) param.args[1];
                        for (Map.Entry<String, Object> entry : map.entrySet()) {
                            Log.e("EDXposed", entry.getKey() + "," + entry.getValue().toString());
                        }
                    }

                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        super.afterHookedMethod(param);
                    }
                });
    }

    /**
     * 获取 com.sankuai.waimai.business.search.api.WaimaiSearchService 动态代理实现类
     *
     * @param result
     */
    public void setWaimaiSearchService(Object result) {
        waimaiSearchServiceImpl = result;
        XposedHelpers.findAndHookMethod(result.getClass(), "searchGlobalPage",
                long.class,
                int.class,
                int.class,
                String.class,
                int.class,
                int.class,
                int.class,
                String.class,
                String.class,
                int.class,
                boolean.class,
                String.class,
                String.class,
                Integer.class,
                int.class,
                int.class,
                String.class,
                long.class,
                int.class,
                long.class,
                long.class,
                String.class,
                int.class,
                int.class,
                new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        super.beforeHookedMethod(param);
                        for (Object obj : param.args) {
                            Log.e("EDXposed", obj == null ? "null" : String.valueOf(obj));
                        }
                    }

                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        super.afterHookedMethod(param);
                    }
                });
    }

    /**
     * 搜索实现类
     *
     * @param result
     */
    public void setSCSearchApiService(Object result) {
        sCSearchApiServiceImpl = result;
        Log.e("EDXposed", "______searchGlobalPage");
        XposedHelpers.findAndHookMethod(result.getClass(), "searchGlobalPage",
                long.class,
                int.class,
                String.class,
                int.class,
                String.class,
                int.class,
                int.class,
                int.class,
                String.class,
                String.class,
                long.class,
                boolean.class,
                String.class,
                String.class,
                Integer.class,
                int.class,
                int.class,
                String.class,
                long.class,
                int.class,
                int.class,
                String.class,
                String.class,
                new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        super.beforeHookedMethod(param);
                        for (Object obj : param.args) {
                            Log.e("EDXposed", obj == null ? "null" : String.valueOf(obj));
                        }
                    }

                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        super.afterHookedMethod(param);
                    }
                });
    }
}
