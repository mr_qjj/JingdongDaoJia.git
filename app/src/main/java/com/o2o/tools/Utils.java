package com.o2o.tools;

import android.util.Log;

import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class Utils {

    /**
     * 1.生成的字符串每个位置都有可能是str中的一个字母或数字，需要导入的包是import java.util.Random;
     * @param length
     * @return
     */
    public static String createRandomStr1(int length){
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(str.length());
            stringBuffer.append(str.charAt(number));
        }
        return stringBuffer.toString();
    }

    /**
     * 随机数字
     * @param length
     * @return
     */
    public static String createRandomNum(int length){
        String str = "123456789";
        Random random = new Random();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(str.length());
            stringBuffer.append(str.charAt(number));
        }
        return stringBuffer.toString();
    }

    /**
     * map 转 treeMap
     * @param url
     * @param map
     */
    public static void mapConvertMap(String url, Map<String,String> map){
        try {
            URL u = new URL(url);
            String uri = u.getQuery();
            if(uri != null) {
                String[] keyList = uri.split("&");
                for (String list : keyList) {
                    if (list.split("=").length > 1) {
                        String key = list.split("=")[0];
                        String value = list.split("=")[1];
                        map.put(key, value);
                    }
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 反射修改header头部时间戳
     * @param o
     * @param key
     * @param value
     */
    public static void modifyHeader(Object o,String key, String value) {
        try{
            Field field = o.getClass().getDeclaredField("headers");
            field.setAccessible(true);
            List<?> list = (List<?>)field.get(o);
            for (int i = 0; i< list.size(); i++){
                Class<?> cla = list.get(i).getClass();
                String n =  cla.getMethod("getName").invoke(list.get(i)).toString();
                String v =  cla.getMethod("getValue").invoke(list.get(i)).toString();
                if (key.equals(n)){
                    Field f = cla.getDeclaredField("value");
                    f.setAccessible(true);
                    f.set(list.get(i),value);
                }
            }
        }catch (Exception e) {

        }
    }

    /**
     * 反射修改Request类中的值
     * @param o
     * @param key
     * @param value
     */
    public static void modifyValue(Object o,String key, String value) {
        try{
            Field field = o.getClass().getDeclaredField(key);
            field.setAccessible(true);
            field.set(o,value);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 反射修改Request类中的值
     * @param o
     * @param key
     * @param object
     */
    public static void modifyValue(Object o,String key, Object object) {
        try{
            Field field = o.getClass().getDeclaredField(key);
            field.setAccessible(true);
            field.set(o,object);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
