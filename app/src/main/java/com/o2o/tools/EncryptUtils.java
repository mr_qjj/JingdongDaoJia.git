package com.o2o.tools;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPInputStream;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptUtils {
    public static final String CIPHER_MODE = "AES/CBC/NoPadding";
    public static byte[] KEY;
    public static byte[] IV;

    public static int a(String str) {
        int hashCode = str.hashCode();
        return (hashCode >>> 16) ^ (65535 & hashCode);
    }

    static {
        byte[] bArr = {92, 115, 116, 117, 112, 113, 6, 112, 112, 3, 3, 4, 6, 118, 0, 112};
        byte b = 24;
        for (int i = 0; i < bArr.length; i++) {
            b = (byte) (b ^ (bArr[i] & 255));
            bArr[i] = b;
        }
        KEY = bArr;
        byte[] bArr2 = {0, 118, 122, 10, 3, 116, 124, 10, 5, 117, 6, 5, 3, 4, 2, 37};
        byte b2 = 97;
        for (int length = bArr2.length - 1; length >= 0; length--) {
            b2 = (byte) (b2 ^ (bArr2[length] & 255));
            bArr2[length] = b2;
        }
        IV = bArr2;
    }

    public static byte[] decrypt(byte[] bArr) throws Exception {
        Cipher instance = Cipher.getInstance(CIPHER_MODE);
        instance.init(2, new SecretKeySpec(KEY, CIPHER_MODE.substring(0, 3)), new IvParameterSpec(IV));
        GZIPInputStream gZIPInputStream = new GZIPInputStream(new ByteArrayInputStream(instance.doFinal(bArr)));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        byte[] bArr3 = new byte[4096];
        while (true) {
            int read = gZIPInputStream.read(bArr3);
            if (read > 0) {
                byteArrayOutputStream.write(bArr3, 0, read);
            } else {
                gZIPInputStream.close();
                byteArrayOutputStream.close();
                return byteArrayOutputStream.toByteArray();
            }
        }
    }
}
