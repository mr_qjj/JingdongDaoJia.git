package com.o2o.tools;


public class MyThreadLocal extends ThreadLocal<String> {
    private static volatile MyThreadLocal instance = null;
    public static MyThreadLocal getInstance() {
        if (instance == null) {
            synchronized (MyThreadLocal.class) {
                if (instance == null) {
                    instance = new MyThreadLocal();
                }
            }
        }
        return instance;
    }
}