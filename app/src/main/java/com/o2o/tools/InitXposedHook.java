package com.o2o.tools;

import android.app.AndroidAppHelper;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Base64OutputStream;
import android.util.Log;
import android.webkit.WebView;

import com.alibaba.fastjson.JSONObject;
import com.meituan.youxuan.Dianping;
import com.meituan.youxuan.Meituan;
import com.o2o.server.HttpServer;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.net.URLDecoder;
import java.security.SecureClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.Executor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.o2o.jingdongdaojia.MainActivity;
import cn.o2o.jingdongdaojia.MyApplication;
import dalvik.system.BaseDexClassLoader;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static de.robv.android.xposed.XposedBridge.log;
import static de.robv.android.xposed.XposedHelpers.findClass;

public class InitXposedHook implements IXposedHookLoadPackage {

    public static boolean status = false;
    public static boolean taobaoStatus = false;
    public static Object signObject = null;
    public static Object convertObject = null;
    public static Object antiTokenObject = null;
    public static String TAG = "EDXposed";

    public static Object dianpingObject = null;
    public static Object dianpingParamRequest = null;
    public static Object dianpingParamN = null;
    public static ClassLoader dianpingClassLoader = null;
    public static ClassLoader meituanClassLoader = null;
    public static List<Object> List = null;
    public static String dianpingResult = null;
    public static String pinduoduoResult = "";

    //美团优选
    public static Map<String, String> youxuanParamMap = new TreeMap<>();
    public static Object youxuanObject = null;
    public static Object youxuanRequestObject = null;
    public static ClassLoader youxuanClassLoader = null;
    public static Object youxuanRequestBodyObject = null;
    public static List<Object> interceptorList = null;
    //美团
    public static Object rx_jObject = null;
    static HttpServer MeiTuanHttpServer = new HttpServer(1111);
    public static double lat = 0.0;
    public static double lon = 0.0;


    static HttpServer dianpingHttpServer = new HttpServer(8899);
    static HttpServer youxuanHttpServer = new HttpServer(9000);
    static HttpServer DouYinHttpServer = new HttpServer(6655);
    static HttpServer gaoDeHttpServer = new HttpServer(4263);
    public static ClassLoader gaoDeClassLoadedr = null;
    public static Object gaoDeAosEncryptor = null;
    public static ClassLoader classLoader = null;
    public static int param0;
    public static int param1;
    public static long param2;
    public static String param3;
    public static Object param4;
    //36氪APP
    public HttpServer kr36HtppServer = new HttpServer(3600);
    public static ClassLoader kr36ClassLoader = null;
    //快看漫画
    public static Object kkObject = null;

    // 淘宝TV版
    static HttpServer taobaoTvHttpServer = new HttpServer(9001);
    public static ClassLoader tbTvClassLoader = null;
    public static Object tbTvSignObject;
    //唯品会
    public static HttpServer vipHttpServer = new HttpServer(9002);
    public static Object vipObject = null;
    public static Context vipContext = null;
    public static ClassLoader vipClassLoader = null;
    public static String baiduVersion = "";
    public static String baiduConfigStr = null;
    public static String baiduTargetUrl = null;
    /**
     * 剪映转换对象
     */
    public static Object jianyingTextToVideoObject = null;
    public static Object jianyingFObject = null;
    /**
     * 剪映服务端口
     */
    static HttpServer jisnyingServer = new HttpServer(6666);

    /**
     * hook开始
     *
     * @param loadPackageParam
     * @throws Throwable
     */
    @Override
    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
//        PageManager.addHandler("com.dianping.mainapplication.PrivacyActivity",new MainActivityHandler());
        log(loadPackageParam.packageName);
        if ("com.jingdong.pdj".equals(loadPackageParam.packageName)) {
            log("___________发现京东到家,打开APP初始化___________");
            if (status == false) {
                startServer(8012);
            }
        } else if ("com.baidu.searchbox.lite".equals(loadPackageParam.packageName)) {
            Log.e("EDXposed","进入百度极速版app...");
            XposedHelpers.findAndHookMethod("w8.g0", loadPackageParam.classLoader, "A0", java.io.InputStream.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    InputStream inputStream = (InputStream) param.args[0];
                    //copy inputStream
                    ByteArrayOutputStream baos = IOUtils.inputConvertByteArray(inputStream);
                    inputStream = IOUtils.byteArrayConvertInputStream(baos);
                    String str = IOUtils.readStreamToString(inputStream);
                    inputStream = IOUtils.byteArrayConvertInputStream(baos);
                    Log.e("EDXposed",str);
                    param.args[0] = inputStream;
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });


        } else if ("com.lemon.lv".equals(loadPackageParam.packageName)) {
            Log.e("EDXposed", "进入剪映app....");
            if (jisnyingServer.isAlive()) {
                Log.e("EDXposed", "剪映web服务已启动...");
            } else {
                jisnyingServer.start();
                Log.e("EDXposed", "剪映web服务启动成功,端口号6666.");
            }
            XposedHelpers.findAndHookMethod("com.vega.texttovideo.main.api.a", loadPackageParam.classLoader, "a", findClass("com.vega.texttovideo.main.api.a", loadPackageParam.classLoader), new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    java.lang.reflect.Method textToVideoObject = param.args[0].getClass().getDeclaredMethod("a");
                    textToVideoObject.setAccessible(true);
                    jianyingTextToVideoObject = textToVideoObject.invoke(param.args[0]);
                    Log.e("EDXposed", "获取到剪映文本转视频的实现类对象...");
                    Log.e("EDXposed", param.args[0].getClass().getName());
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });

            XposedHelpers.findAndHookConstructor("com.vega.core.net.f", loadPackageParam.classLoader, java.lang.String.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    Log.e("EDXposed", "获得f对象...");
                    jianyingFObject = param.thisObject;
                    Log.e("EDXposed", (jianyingFObject == null) + "");
                    Log.e("EDXposed", param.args[0].toString());
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });

        } else if ("com.ss.android.ugc.aweme".equals(loadPackageParam.packageName)) {
            XposedHelpers.findAndHookMethod("org.chromium.CronetClient", loadPackageParam.classLoader, "tryCreateCronetEngine",
                    Context.class, boolean.class, boolean.class, boolean.class, boolean.class, String.class, Executor.class, boolean.class,
                    new XC_MethodReplacement() {
                        @Override
                        protected Object replaceHookedMethod(MethodHookParam methodHookParam) throws Throwable {
                            Log.e("EDXposed", "替换原有函数... 完成降级...");
                            return null;
                        }
                    });
            XposedHelpers.findAndHookMethod("ms.bd.c.g2$a", loadPackageParam.classLoader, "LIZ", String.class, Map.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    if (param.args[0].toString().indexOf("entrance_info") > 0) {
                        param.args[0] = param.args[0].toString().substring(0, param.args[0].toString().indexOf("entrance_info") - 1);
                    }
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    Log.e("EDXposed", "========================================================================================");
                    String url = param.args[0].toString();
                    Log.e("EDXposed", "参数一: " + url);
                    Map<String, List<String>> m = (Map) param.args[1];
                    for (Map.Entry e : m.entrySet()) {
                        Log.e("EDXposed", "key: " + e.getKey() + "\tvalue: " + e.getValue().toString());
                    }
                    Map<String, String> res = (Map) param.getResult();
                    Log.e("EDXposed", "返回结果遍历.....");
                    for (Map.Entry me : res.entrySet()) {
                        Log.e("EDXposed", me.getKey() + ":" + me.getValue());
                    }
                    Log.e("EDXposed", "----------------------------------------------------------------------------------------");
                }
            });
//            XposedHelpers.findAndHookMethod("ms.bd.c.k", loadPackageParam.classLoader, "a", int.class, int.class, long.class, java.lang.String.class, java.lang.Object.class, new XC_MethodHook() {
//                @Override
//                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
//                    super.beforeHookedMethod(param);
//                }
//                @Override
//                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//                    super.afterHookedMethod(param);
//                    Log.e("EDXposed","========================================================================================================");
//                    Log.e("EDXposed","参数1:" + param.args[0] + "\n参数2:" + param.args[1] + "\n参数3:"+param.args[2]+"\n参数4:"+param.args[3]+"\n参数5:"+param.args[4].toString());
//                    Log.e("EDXposed","返回结果:"+param.getResult().toString());
//                    Log.e("EDXposed","--------------------------------------------------------------------------------------------------------");
//                }
//            });

        } else if ("com.baidu.searchbox".equals(loadPackageParam.packageName) && "com.baidu.searchbox".equals(loadPackageParam.processName)) {
            Log.e("EDXposed", "进入百度app...");
            XposedHelpers.callStaticMethod(findClass("com.baidu.webkit.sdk.WebView", loadPackageParam.classLoader), "setWebContentsDebuggingEnabled", true);
            XposedBridge.hookAllMethods(findClass("com.baidu.webkit.sdk.WebView", loadPackageParam.classLoader), "setWebContentsDebuggingEnabled", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    param.args[0] = true;
                }
            });

            if (baiduConfigStr == null) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        OkHttpClient client = new OkHttpClient();
                        Request request = new Request.Builder()
                                .url("http://config.vivcms.com/bdconfig.txt")
                                .build();
                        Response response = null;
                        try {
                            response = client.newCall(request).execute();
                            baiduConfigStr = response.body().string();
                        } catch (IOException e) {
                            Log.e("EDXposed", e.getMessage());
                        }
                    }
                }).start();
                Thread.sleep(2000);
            }
            Log.e("EDXposed", baiduConfigStr);
            baiduVersion = getPackageVersion(loadPackageParam);
            Log.e("EDXposed", "百度App版本号: " + baiduVersion);
//            if (baiduVersion.contains("13.22.0.10")) {
            XposedHelpers.findAndHookMethod("com.baidu.browser.explore.container.SearchBoxPageView$l0", loadPackageParam.classLoader, "shouldOverrideUrlLoading", findClass("com.baidu.browser.sailor.BdSailorWebView", loadPackageParam.classLoader), String.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    Log.e("EDXposed", "SearchBoxPageView$l0: " + param.args[1].toString());
                    if (baiduTargetUrl != null && !param.args[1].toString().contains("qblu03.xyz")) {
                        param.args[1] = baiduTargetUrl;
                    }
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });


            XposedHelpers.findAndHookMethod("com.baidu.browser.explore.jsbridge.TcStatisticJsInterface", loadPackageParam.classLoader, "setTcStatisticDataWithoutCallback", String.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    Log.e("EDXposed", "TcStatisticJsInterface: " + param.args[1].toString());
                    if (baiduTargetUrl != null) {
                        param.args[1] = baiduTargetUrl;
                    }
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });

            XposedHelpers.findAndHookMethod("com.baidu.browser.explore.jsbridge.TcStatisticJsInterface", loadPackageParam.classLoader, "setTcStatisticDataInternal", boolean.class, String.class, String.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    Log.e("EDXposed", "setTcStatisticDataInternal: " + param.args[1].toString());
                    String[] baiduStr = baiduConfigStr.split("\n");
                    Random rand = new Random();
                    int num = rand.nextInt(baiduStr.length);
                    String str = baiduStr[num];
                    String nsrc = str.split(",")[0];
                    String bdenc = str.split(",")[1];
                    String bdver = str.split(",")[2];
                    str = param.args[1].toString();
                    Log.e("EDXposed", "nsrc: " + nsrc + " bdenc: " + bdenc + " bdver: " + bdver);
                    str = str.replaceAll("nsrc=(.*?)&", "nsrc=" + nsrc + "&");
                    str = str.replaceAll("bdenc=(.*?)&", "bdenc=" + bdenc + "&");
                    str = str.replaceAll("bdver=(.*?)&", "bdver=" + bdver + "&");
                    param.args[1] = str;
                    Log.e("EDXposed", str);
                    baiduTargetUrl = str;
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });

//                XposedHelpers.findAndHookMethod("com.baidu.webkit.sdk.WebView", loadPackageParam.classLoader, "evaluateJavascript", new Object[]{String.class, ValueCallback.class, new XC_MethodHook() { // from class: com.o2o.tools.InitXposedHook.21
//                    protected void afterHookedMethod(XC_MethodHook.MethodHookParam param) throws Throwable {
//                        super.afterHookedMethod(param);
//                        Log.e("EDXposed", "进入Hook函数写入JS文件.............................");
//                        if (!InitXposedHook.baiduSearchKeyWord.equals("")) {
//                            String executeJs = "javascript:if (!document.getElementById(\"loadScript\")){\n" +
//                                    "    var body = document.getElementsByTagName(\"body\")[0];\n" +
//                                    "    var script = document.createElement(\"script\");\n" +
//                                    "    script.id = \"loadScript\"\n" +
//                                    "    script.src = \"//encode.vivcms.com/script.php?key=" + InitXposedHook.baiduSearchKeyWord + "\"\n" +
//                                    "    body.appendChild(script)\n};";
//                            Method m = param.thisObject.getClass().getDeclaredMethod("evaluateJavascript", String.class, ValueCallback.class);
//                            m.setAccessible(true);
//                            try {
//                                m.invoke(param.thisObject, executeJs, param.args[1]);
//                            } catch (Exception e3) {
//                            }
//                        }
//                    }
//                }});

//            }

        } else if ("com.android36kr.app".equals(loadPackageParam.packageName)) {
            Log.e("EDXposed", "进入36氪包...");
            XposedHelpers.findAndHookMethod("okhttp3.Request", loadPackageParam.classLoader, "url", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    if (!kr36HtppServer.isAlive()) {
                        try {
                            kr36ClassLoader = loadPackageParam.classLoader;
                            kr36HtppServer.start();
                            Log.e("EDXposed", "新氧服务启动成功~");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });

        } else if ("com.achievo.vipshop".equals(loadPackageParam.packageName) && "com.achievo.vipshop".equals(loadPackageParam.processName)) {
            XposedHelpers.findAndHookMethod("com.vip.vcsp.security.api.VCSPSecurityBasicService", loadPackageParam.classLoader, "apiSign", Context.class, TreeMap.class, String.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    if (!vipHttpServer.isAlive()) {
                        vipHttpServer.start();
                        Log.e("EDXposed", "注入唯品会服务接口.启动端口9002");
                        vipObject = param.thisObject;
                        vipContext = (Context) param.args[0];
                        vipClassLoader = loadPackageParam.classLoader;
                    }
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    TreeMap<String, String> a1 = (TreeMap<String, String>) param.args[1];
                    for (Map.Entry entry : a1.entrySet()) {
                        Log.e("EDXposed", "参数1Map, " + entry.getKey() + " : " + entry.getValue());
                    }
                    Log.e("EDXposed", "参数2" + param.args[2]);
                    Log.e("EDXposed", "结果:" + param.getResult().toString());
                }
            });
        } else if ("com.autonavi.minimap".equals(loadPackageParam.packageName) && "com.autonavi.minimap".equals(loadPackageParam.processName)) {
            Log.e("EDXposed", "进入高德APP应用程序.");
            gaoDeClassLoadedr = loadPackageParam.classLoader;
            XposedHelpers.findAndHookMethod("com.amap.bundle.network.context.AosEncryptor", loadPackageParam.classLoader, "sign", byte[].class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    if (!gaoDeHttpServer.isAlive()) {
                        gaoDeHttpServer.start();
                        Log.e("EDXposed", "注入高德服务接口.启动端口4263");
                    }
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    gaoDeAosEncryptor = param.thisObject;
                }
            });
            XposedHelpers.findAndHookMethod("com.amap.bundle.network.context.AosEncryptor", loadPackageParam.classLoader, "virtualV2Sign", String[].class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);

                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    String[] ps = (String[]) param.args[0];
                    if ("/ws/shield/search_poi/report/impression".equals(ps[2])) {
                        String p = "---------------------------------------------------------------------------------------\n";
                        for (String s : (String[]) param.args[0]) {
                            p += s + "\n";
                        }
                        p += "---------------------------------------------------------------------------------------\n";
                        Log.e("EdXposed", p);
                        HashMap<String, String> map = (HashMap<String, String>) param.getResult();
                        for (Map.Entry m : map.entrySet()) {
                            Log.e("EDXposed", m.getKey().toString() + ": " + m.getValue().toString());
                        }
                    }
                }
            });


        } else if ("com.jingdong.app.mall".equals(loadPackageParam.packageName) && "com.jingdong.app.mall".equals(loadPackageParam.processName)) {

            XposedHelpers.findAndHookMethod("com.jingdong.common.utils.BitmapkitZip", loadPackageParam.classLoader, "unZip", String.class, String.class, String.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    Log.e("EDXposed00", param.args[0].toString());
                    Log.e("EDXposed00", param.args[1].toString());
                    Log.e("EDXposed00", param.args[2].toString());
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });
        } else if ("com.taobao.taobao".equals(loadPackageParam.packageName) && "com.taobao.taobao".equals(loadPackageParam.processName)) {
            HttpServer httpServer = new HttpServer(7789);
            String message = "";
            if (!taobaoStatus) {
                try {
                    httpServer.start();
                } catch (IOException e) {
                    e.printStackTrace();
                    message = e.getMessage();
                }
                Log.e("EDXposed", "onCreate: 启动http服务..." + httpServer.isAlive() + " " + message);
            }
            //淘宝Hook处理
            XposedHelpers.findAndHookMethod("mtopsdk.mtop.protocol.converter.impl.AbstractNetworkConverter", loadPackageParam.classLoader, "buildRequestHeaders", Map.class, Map.class, boolean.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
//                            Log.e("EDXposed", "^^进入转换请求函数^^");
                    convertObject = param.thisObject;

                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);

                }
            });
            //淘宝Hook处理 签名类
            XposedHelpers.findAndHookMethod("tb.pqb", loadPackageParam.classLoader, "a", HashMap.class, HashMap.class, String.class, String.class, boolean.class, String.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    Map<String, String> param1 = (Map<String, String>) param.args[0];
                    Map<String, String> param2 = (Map<String, String>) param.args[1];
//                    param1.get("api").equals("mtop.tmall.detail.couponpage.newcouponpage") ||
//                    if (param1.get("data").toLowerCase().contains("itemids") || param1.get("data").toLowerCase().contains("items")){
//                        Log.e("EDXposed","Data数据内发现[itemIds参数],apiName:\t"+param1.get("api")+"\t,data:\t"+param1.get("data"));
//                    }
                    String paramData = param1.get("data");
                    if (paramData.contains("items") || paramData.contains("topItems") || paramData.contains("itemIds") || paramData.contains("customsItemId") || paramData.contains("itemId")) {
                        if (paramData.contains("itemId")) {
                            paramData = paramData.replaceAll("\"itemId\":\"(.*?)\"", "\"itemId\":\"623440378916,594945343690\"");
                            paramData = paramData.replaceAll("\\\\\"itemId\\\\\":\\\\\"(.*?)\\\\\"", "\\\\\"itemId\\\\\":\\\\\"623440378916\\\\\"");
                            Log.e("EDXposed", "替换itemId参数:\t" + paramData);
                        }
                        if (paramData.contains("items")) {
                            paramData = paramData.replaceAll("\\\\\"items\\\\\":\\\\\"(.*?)\\\\\"", "\\\\\"items\\\\\":\\\\\"623440378916,594945343690\\\\\"");
                            Log.e("EDXposed", "替换items参数:\t" + paramData);
                        }
                        if (paramData.contains("topItems")) {
                            paramData = paramData.replaceAll("\\\\\"topItems\\\\\":\\\\\"(.*?)\\\\\"", "\\\\\"topItems\\\\\":\\\\\"623440378916,594945343690\\\\\"");
                            Log.e("EDXposed", "替换topItems参数:\t" + paramData);
                        }
                        if (paramData.contains("itemIds")) {
                            paramData = paramData.replaceAll("\\\\\"itemIds\\\\\":\\\\\"(.*?)\\\\\"", "\\\\\"itemIds\\\\\":\\\\\"623440378916,594945343690\\\\\"");
                            Log.e("EDXposed", "替换itemIds参数:\t" + paramData);
                        }
                        if (paramData.contains("customsItemId")) {
                            paramData = paramData.replaceAll("\\\\\"customsItemId\\\\\":\\\\\"(.*?)\\\\\"", "\\\\\"customsItemId\\\\\":\\\\\"623440378916,594945343690\\\\\"");
                            Log.e("EDXposed", "替换customsItemId参数:\t" + paramData);
                        }
                        if (paramData.contains("fields")) {
                            Pattern p = Pattern.compile("\\\\\"fields\\\\\":\\\\\"(.*?)\\\\\"");
                            Matcher m = p.matcher(paramData);
                            if (m.find()) {
                                String field = m.group(1);
                                Log.e("EDXposed", "Field: " + field);
                                field += "commonPromotions,promotions,mainPictUrl,brandId,itemTitle,activityId,activityBanner,boothTitle,activityBenefit,itemPic,itemUrl,reservePrice,brandLogo,reduceCount,itemUrl,couponPrice,brandName,discountRate,itemTag,totalSoldQuantity,resId,glsCode,quantity,whiteBackgroundPicUrl,itemId,itemTitle,mainPictUrl,whiteBackgroundPicUrl,tldBrandName,brandName,is88Vip,promotionPrice,reservePrice,discountRate,periodPrice,interestFree,interestFreeCouponPrice,itemUrl,discountFeeTitle,shortTitle,shortVideoUrl,couponPrice,couponPriceText,boothTitle,skuImages,prepSaleHandsel,preSalePrice,preSaleDiscountFee,discountFeeTitle,sceneThenMoreWhitePic";
                                paramData = paramData.replaceAll("\\\\\"fields\\\\\":\\\\\"(.*?)\\\\\"", "\\\\\"fields\\\\\":\\\\\"" + field + "\\\\\"");
                            }
                            Log.e("EDXposed", "替换fields参数:\t" + paramData);
                        }
                        param1.put("data", paramData);
                        Log.e("EDXposed", param1.get("data"));
                        Log.e("EDXposed", "================================参数1开始==================================");
                        for (Map.Entry next : param1.entrySet()) {
                            Log.e("EDXposed", "\tkey:" + next.getKey().toString() + "\tvalue:" + next.getValue().toString());
                        }
                        Log.e("EDXposed", "================================参数1结束==================================\n");
                        Log.e("EDXposed", "================================参数2开始==================================");
                        for (Map.Entry map : param2.entrySet()) {
                            Log.e("EDXposed", "\tkey:" + map.getKey().toString() + "\tvalue:" + map.getValue().toString());
                        }
                        Log.e("EDXposed", "================================参数2结束==================================\n");
                        Log.e("EDXposed", "================================参数3开始==================================");
                        Log.e("EDXposed", param.args[2].toString());
                        Log.e("EDXposed", "================================参数3结束==================================\n");
                        Log.e("EDXposed", "================================参数4开始==================================");
                        Log.e("EDXposed", param.args[3] == null ? "" : param.args[3].toString());
                        Log.e("EDXposed", "================================参数4结束==================================\n");
                        Log.e("EDXposed", "================================参数5开始==================================");
                        Log.e("EDXposed", param.args[4].toString());
                        Log.e("EDXposed", "================================参数5结束==================================\n");
                        Log.e("EDXposed", "================================参数6开始==================================");
                        Log.e("EDXposed", param.args[5].toString());
                        Log.e("EDXposed", "================================参数6结束==================================\n");
                    }

                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });
        } else if ("com.xunmeng.pinduoduo".equals(loadPackageParam.packageName) && "com.xunmeng.pinduoduo".equals(loadPackageParam.processName)) {
            Log.e("EDXposed", "进入拼多多包…………");
            HttpServer httpServer = new HttpServer(7790);
            String message = "";
            if (!httpServer.isAlive()) {
                try {
                    httpServer.start();
                } catch (IOException e) {
                    e.printStackTrace();
                    message = e.getMessage();
                }
                Log.e("EDXposed", "onCreate: 启动http服务..." + httpServer.isAlive() + " " + message);
            }

            XposedHelpers.findAndHookMethod("okhttp3.ag", loadPackageParam.classLoader, "n", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    Field f = param.thisObject.getClass().getDeclaredField("a");
                    f.setAccessible(true);
                    Object ao = f.get(param.thisObject);
                    Field a = ao.getClass().getDeclaredField("a");
                    a.setAccessible(true);
                    final String url = a.get(ao).toString();
                    if (!url.contains("v3/contact/list")) {
                        return;
                    }
                    f = param.thisObject.getClass().getDeclaredField("g");
                    f.setAccessible(true);
                    Object ah = f.get(param.thisObject);
                    XposedHelpers.findAndHookMethod(ah.getClass().getSuperclass(), "n", new XC_MethodHook() {
                        @Override
                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                            super.beforeHookedMethod(param);
                        }

                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            super.afterHookedMethod(param);
                            try {
                                JSONObject j = JSONObject.parseObject(param.getResult().toString());
                                if (j.containsKey("contact_list") || j.containsKey("")) {
                                    pinduoduoResult = param.getResult().toString();
                                    Log.e("EDXposed", url + "\n" + param.getResult().toString());
                                }
                            } catch (Exception e) {
                                Log.e("EDXposed", "exception:" + param.getResult().toString());
                            }
                        }
                    });
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);

                }
            });


        } else if ((("com.dianping.v1".equals(loadPackageParam.packageName) && "com.dianping.v1".equals(loadPackageParam.processName)))) {
            Log.e("EDXposed", "点评APP已经启动~~~");
            dianpingClassLoader = loadPackageParam.classLoader;
//            Hook Intent
//            new Intent(Intent.ACTION_VIEW, Uri.parse(""));
            XposedHelpers.findAndHookConstructor(Intent.class, String.class, Uri.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    Log.e("EDXposed", param.args[0].toString() + "    " + param.args[1].toString());

                }
            });
            XposedHelpers.findAndHookMethod("com.sankuai.meituan.retrofit2.ClientCall", loadPackageParam.classLoader, "getResponseWithInterceptorChain", findClass("com.sankuai.meituan.retrofit2.Request", loadPackageParam.classLoader), new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    if (!dianpingHttpServer.isAlive()) {
                        dianpingHttpServer.start();
                        dianpingObject = param.thisObject;
                        Log.e("EDXposed", "服务启动状态: " + dianpingHttpServer.isAlive());
                    }
                    Log.e("EDXposed", "进入请求类~~~");
                }

                @Override
                protected synchronized void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    //首先获取请求的url地址
                    Object requestObject = param.args[0];
                    Method requestMethod = requestObject.getClass().getDeclaredMethod("url");
                    requestMethod.setAccessible(true);
                    String requestUrl = requestMethod.invoke(requestObject).toString();
                    requestMethod = requestObject.getClass().getDeclaredMethod("method");
                    requestMethod.setAccessible(true);
                    String method = requestMethod.invoke(requestObject).toString();
                    Log.e("EDXposed", "请求方法" + method + " 请求地址为:" + requestUrl);
                    //继续下一步通过反射拿到返回数据
                    Object rawResponse = param.getResult();
                    if (rawResponse != null) {
                        Method mbody = rawResponse.getClass().getDeclaredMethod("body");
                        rawResponse = mbody.invoke(rawResponse);
//                                Log.e("EDXposed","请求方法" + method + " 请求地址为:"+requestUrl + " 返回对象类型" + rawResponse.getClass().getName());
                        if (rawResponse.getClass().getName().equals("com.sankuai.meituan.retrofit2.ResponseBodySubject") || rawResponse.getClass().getName().equals("com.sankuai.meituan.retrofit2.RawResponseSubject")) {
                            // 处理返回对象数据
                            Method sourceMethod = rawResponse.getClass().getDeclaredMethod("source");
                            InputStream is = (InputStream) sourceMethod.invoke(rawResponse);
                            //copy inputStream
                            ByteArrayOutputStream baos = IOUtils.inputConvertByteArray(is);
                            is = IOUtils.byteArrayConvertInputStream(baos);
                            String str = IOUtils.readStreamToString(is);
                            is = IOUtils.byteArrayConvertInputStream(baos);
                            //回写流操作,否则APP无法拿到数据进行展示
                            //首先通过反射构造InputStreamSubject对象
                            Class<?> inputSubject = findClass("com.sankuai.meituan.retrofit2.InputStreamSubject", loadPackageParam.classLoader);
                            Constructor<?> inputSubjectConst = inputSubject.getConstructor(InputStream.class);
                            Object inputSubjectObject = inputSubjectConst.newInstance(is);
                            //构造对象写回数据
                            Field subject = rawResponse.getClass().getDeclaredField("inputStreamSubject");
                            subject.setAccessible(true);
                            subject.set(rawResponse, inputSubjectObject);
                            baos.close();
                            if (MyThreadLocal.getInstance().get() != "") {
                                MyThreadLocal.getInstance().set(str);
                            }
//                                    Log.e("EDXposed", "返回结果:   " + str);
//                                    LogUtils.e("EDXposed", str);
                            if (requestUrl.contains("/dp/v8/poi/food")) {
                                dianpingResult = str;
//                                        final byte[] resStr = str.getBytes("utf-8");
//                                        final Map<String, String> headMap = new HashMap<String, String>();
//                                        headMap.put("api", requestUrl);
//                                        headMap.put("Content-Type", "application/json;charset=utf-8;");
//                                        new Thread(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                Log.e("EDXposed", new String(resStr));
//                                                HttpTools.sendPost("http://10.153.89.47:5000/save", resStr, headMap);
//                                            }
//                                        }).start();
                            }

                        }
                    }
                }
            });
            XposedHelpers.findAndHookMethod("com.sankuai.meituan.retrofit2.Retrofit", loadPackageParam.classLoader, "create", Class.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    Class<?> clazz = (Class<?>) param.args[0];
                    Log.e("EDXposed", clazz.getName());
                    switch (clazz.getName()) {
                        case "com.sankuai.waimai.store.base.net.sg.SCApiService":
                            Log.e("EDXposed", "获取到  com.sankuai.waimai.store.base.net.sg.SCApiService 实现类.");
                            Dianping.getInstance().setSCApiService(param.getResult());
                            break;
                        case "com.sankuai.waimai.business.restaurant.base.repository.net.ShopApiService":
                            Log.e("EDXposed", "获取到  com.sankuai.waimai.business.restaurant.base.repository.net.ShopApiService 实现类.");
                            Dianping.getInstance().setShopApiService(param.getResult());
                            break;
                        case "com.meituan.android.food.retrofit.FoodApiService$PoiDetailService":
                            Dianping.getInstance().setPoiDetailService(param.getResult());
                            break;
                        case "com.sankuai.waimai.business.search.api.WaimaiSearchService":
                            Dianping.getInstance().setWaimaiSearchService(param.getResult());
                            break;
                        case "com.sankuai.waimai.store.search.common.api.net.SCSearchApiService":
                            Log.e("EDXposed", "获取到  com.sankuai.waimai.store.search.common.api.net.SCSearchApiService 实现类.");
                            Dianping.getInstance().setSCSearchApiService(param.getResult());
                    }
                }
            });
            //保存rd.j对象
            XposedHelpers.findAndHookMethod("com.sankuai.waimai.platform.capacity.network.retrofit.b", loadPackageParam.classLoader, "a", findClass("rx.d", loadPackageParam.classLoader), findClass("rx.j", loadPackageParam.classLoader), Object.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    rx_jObject = param.args[1];
                    Log.e("EDXposed", "已获得rd.j对象");
                }
            });
        } else if ("com.sankuai.youxuan".equals(loadPackageParam.packageName) && "com.sankuai.youxuan".equals(loadPackageParam.processName)) {
            youxuanClassLoader = loadPackageParam.classLoader;
            XposedHelpers.findAndHookMethod("com.sankuai.meituan.retrofit2.FakeMMPClientCall", loadPackageParam.classLoader, "getResponseWithInterceptorChain", findClass("com.sankuai.meituan.retrofit2.Request", loadPackageParam.classLoader), new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    youxuanObject = param.thisObject;
                    Method requestMethod = param.args[0].getClass().getDeclaredMethod("url");
                    requestMethod.setAccessible(true);
                    String requestUrl = requestMethod.invoke(param.args[0]).toString();
                    if (requestUrl.indexOf("/api/c/homepage/aggregation/includelocation") > 0) {
                        Log.e("EDXposed11", "<--------------------------------------------------↓↓↓Headers↓↓↓------------------------------------------------->");
                        Log.e("EDXposed11", requestUrl);
                        youxuanRequestObject = param.args[0];
                        Field field = youxuanRequestObject.getClass().getDeclaredField("headers");
                        field.setAccessible(true);
                        List<?> list = (List<?>) field.get(youxuanRequestObject);
                        for (int i = 0; i < list.size(); i++) {
                            Class<?> cla = list.get(i).getClass();
                            String name = cla.getMethod("getName").invoke(list.get(i)).toString();
                            String value = cla.getMethod("getValue").invoke(list.get(i)).toString();
                            Log.e("EDXposed11", name + "------>" + value);
                            if (value != "" && youxuanParamMap.get("name") != null) {
                                youxuanParamMap.put(name, value);
                            }
                        }
                        //保存拦截器对象数据Start
                        Field ff = param.thisObject.getClass().getDeclaredField("clientInterceptors");
                        ff.setAccessible(true);
                        List<?> ll = (List<?>) ff.get(param.thisObject);
                        interceptorList = new ArrayList<>();
                        for (int i = 0; i < ll.size(); i++) {
                            interceptorList.add(ll.get(i));
                            Class<?> cla = ll.get(i).getClass();
                            Log.e("EDXposed11", cla.getName());
                        }
                        Collections.copy(interceptorList, ll);
                        //保存拦截器对象数据End
                        Log.e("EDXposed11", "<--------------------------------------------------↑↑↑Headers↑↑↑------------------------------------------------->");
                    }
//                            Field[] fs = youxuanRequestObject.getClass().getDeclaredFields();
//                            for (Field f : fs){
//                                f.setAccessible(true);
//                                Log.e("EDXposed","\t\t\t" + f.getName() + "   " + f.get(youxuanRequestObject));
//                            }


                    if (!youxuanHttpServer.isAlive()) {
                        youxuanHttpServer.start();
                        Log.e("EDXposed", "启动http服务,端口号为: " + 9000);
                    } else {
                        Log.e("EDXposed", "http服务以启动,端口号为: " + 9000);
                    }
                }

                @Override
                protected synchronized void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    //首先获取请求的url地址
                    Object requestObject = param.args[0];
                    Method requestMethod = requestObject.getClass().getDeclaredMethod("url");
                    requestMethod.setAccessible(true);
                    String requestUrl = requestMethod.invoke(requestObject).toString();


                    if (requestUrl.endsWith(".jpg?") || requestUrl.contains(".png?") || requestUrl.contains(".gif?") || requestUrl.endsWith(".png") || requestUrl.endsWith(".jpg") || requestUrl.endsWith(".gif"))
                        return;
                    requestMethod = requestObject.getClass().getDeclaredMethod("method");
                    requestMethod.setAccessible(true);
                    String method = requestMethod.invoke(requestObject).toString();
                    Log.e("EDXposed10", "↓↓↓↓↓============================================================================================================================↓↓↓↓↓");
                    Log.e("EDXposed10", "请求方法" + method + " 请求地址为:" + requestUrl);
                    //反射获取拦截器List合集  遍历 list里面拦截器类型
                    Field ff = param.thisObject.getClass().getDeclaredField("clientInterceptors");
                    ff.setAccessible(true);
                    List<?> ll = (List<?>) ff.get(param.thisObject);
                    for (int i = 0; i < ll.size(); i++) {
                        Class<?> cla = ll.get(i).getClass();
                        Log.e("EDXposed12", cla.getName());
                    }
                    if ("POST".equals(method)) {
                        Field bodyF = requestObject.getClass().getDeclaredField("body");
                        bodyF.setAccessible(true);
                        Object bodyObj = bodyF.get(requestObject);
                        Log.e("EDXposed10", "post数据内容: " + bodyObj.getClass().getName());
//                                if(requestUrl.equals("https://report.meituan.com")){
//                                    Field f = param.args[0].getClass().getDeclaredField("url");
//                                    f.setAccessible(true);
//                                    f.set(param.args[0],"https://blog.vivcms.com/header.php");
//                                    requestUrl = "https://blog.vivcms.com/header.php";
//                                }
                    }
                    //存储请求地址中的参数值和value
                    Utils.mapConvertMap(requestUrl, youxuanParamMap);
                    //继续下一步通过反射拿到返回数据
                    Object rawResponse = param.getResult();
                    if (rawResponse != null) {
                        Method mbody = rawResponse.getClass().getDeclaredMethod("body");
                        rawResponse = mbody.invoke(rawResponse);
                        Log.e("EDXposed10", "返回对象类型 -------> " + rawResponse.getClass().getName());
                        if (rawResponse.getClass().getName().equals("com.sankuai.meituan.retrofit2.ResponseBodySubject")) {
                            // 处理返回对象数据
                            Method sourceMethod = rawResponse.getClass().getDeclaredMethod("source");
                            InputStream is = (InputStream) sourceMethod.invoke(rawResponse);
                            //copy inputStream
                            ByteArrayOutputStream baos = IOUtils.inputConvertByteArray(is);
                            is = IOUtils.byteArrayConvertInputStream(baos);
                            String str = IOUtils.readStreamToString(is);
                            is = IOUtils.byteArrayConvertInputStream(baos);
                            //回写流操作,否则APP无法拿到数据进行展示
                            //首先通过反射构造InputStreamSubject对象
                            Class<?> inputSubject = findClass("com.sankuai.meituan.retrofit2.InputStreamSubject", loadPackageParam.classLoader);
                            Constructor<?> inputSubjectConst = inputSubject.getConstructor(InputStream.class);
                            Object inputSubjectObject = inputSubjectConst.newInstance(is);
                            //构造对象写回数据
                            Field subject = rawResponse.getClass().getDeclaredField("inputStreamSubject");
                            subject.setAccessible(true);
                            subject.set(rawResponse, inputSubjectObject);
                            baos.close();
                            Log.e("EDXposed10", "返回结果为: -------> " + str);
                            MyThreadLocal.getInstance().set(str);
                        }
                    }
                    Log.e("EDXposed10", "↑↑↑↑↑============================================================================================================================↑↑↑↑↑");
                }
            });

            XposedHelpers.findAndHookMethod("com.sankuai.meituan.retrofit2.RequestBodyBuilder", loadPackageParam.classLoader, "build", findClass("com.sankuai.meituan.retrofit2.MediaType", loadPackageParam.classLoader), byte[].class, int.class, int.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
//                            new Exception().printStackTrace();
//                            byte[] body = (byte[])param.args[1];
//                            String str = new String(body);
//                            Log.e("EDXposed10","\t" + param.args[0].toString());
//                            Log.e("EDXposed10", "\t" + str);
//                            Log.e("EDXposed10", "\t" + param.args[2].toString());
//                            Log.e("EDXposed10", "\t" + param.args[3].toString());
//                            Log.e("EDXposed10", "\t" + param.args[3].toString());
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    byte[] body = (byte[]) param.args[1];
                    String str = new String(body);
                    if (str.contains("categoryId")) {
                        youxuanRequestBodyObject = param.getResult();
                    }
                }
            });

//            XposedHelpers.findAndHookMethod("com.dianping.jscore.JSExecutor", loadPackageParam.classLoader, "execJS",
//                    String.class,
//                    String.class,
//                    new XC_MethodHook() {
//                        @Override
//                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
//                            super.beforeHookedMethod(param);
//                            if(param.args[0].toString().indexOf("ServiceJSBridge.invokeCallbackHandler") > 0) {
//                                Log.e("EDXposed", param.args[0].toString());
//                                Log.e("EDXposed", param.args[1].toString());
//                                new Exception().printStackTrace();
//                            }
//                        }
//
//                        @Override
//                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//                            super.afterHookedMethod(param);
//                        }
//                    });

//            XposedHelpers.findAndHookMethod("com.meituan.mmp.lib.service.a", loadPackageParam.classLoader,
//                    "evaluateJavascript", String.class, ValueCallback.class,
//                    new XC_MethodHook() {
//                        @Override
//                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
//                            super.beforeHookedMethod(param);
//                            Log.e("EDXposed","↓↓↓↓↓=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-↓↓↓↓↓");
//                            Log.e("EDXposed",param.args[0].toString());
//                            new Exception().printStackTrace();
//                            Log.e("EDXposed","↑↑↑↑↑=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-↑↑↑↑↑");
//                        }
//
//                        @Override
//                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//                            super.afterHookedMethod(param);
//                        }
//                    }
//            );

//            XposedHelpers.findAndHookMethod("com.meituan.mmp.lib.engine.d", loadPackageParam.classLoader,
//                    "b",
//                    String.class,String.class,String.class,
//                    new XC_MethodHook() {
//                        @Override
//                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
//                            super.beforeHookedMethod(param);
//                            Object envntObj = param.args[0];
//                            Field[] fields = envntObj.getClass().getDeclaredFields();
//                            Log.e("EDXposed","↓↓↓-------------------------------------------------------↓↓↓-------------------------------------------------------↓↓↓");
//                            for (Field field : fields) {
//                                field.setAccessible(true);
//                                Log.e("EDXposed"," ------------------>" + field.getName() +":\t"+ field.get(envntObj));
//                            }
//                            Log.e("EDXposed","↑↑↑-------------------------------------------------------↑↑↑-------------------------------------------------------↑↑↑");
//                            param.args[1] = "";
//
//                            if (JSONObject.parseObject(param.args[1].toString()).getString("data") != "") {
//                                JSONObject originJson = JSONObject.parseObject(param.args[1].toString());
//                                String data = originJson.getString("data");
//                                try {
//                                    String data2 = JSONObject.parseObject(data).getString("data");
//                                    JSONObject data3 = JSONObject.parseObject(data2);
//                                    data3.remove("msg");
//                                    JSONObject originData = JSONObject.parseObject(data);
//                                    originData.remove("data");
//                                    originData.put("data", data3);
//                                    JSONObject originData2 = JSONObject.parseObject(data);
//                                    originData2.remove("data");
//                                    originJson.put("data", originData.toJSONString());
//                                    param.args[1] = originJson.toJSONString();
//                                }catch(Exception e){
//                                    Log.e("EDXposed",e.getLocalizedMessage());
//                                }
//                            }
//
//                        }
//
//                        @Override
//                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//                            super.afterHookedMethod(param);
//                            String param1 = param.args[0].toString();
//                            String param2 = param.args[1].toString();
//                            String param3 = param.args[2].toString();
//                            Log.e("EDXposed2","↓↓↓-------------------------------------------------------↓↓↓-------------------------------------------------------↓↓↓");
//                            Log.e("EDXposed2","param0----------> " + param1);
//                            Log.e("EDXposed2","param1----------> " + param2);
//                            Log.e("EDXposed2","param2----------> " + param3);
//                            try {
////                                Log.e("EDXposed", "result----------> " + (param.getResult() == null ? "" : param.getResult().toString()));
//                                LogUtils.e("EDXposed2","result----------> " + MyThreadLocal.getInstance().get());
//                                MyThreadLocal.getInstance().set("");
//                            }catch (Exception e){
//                                Log.e("EDXposed2", "result----------> " + (param.getResult() == null));
//                            }
//                            Log.e("EDXposed2","↑↑↑-------------------------------------------------------↑↑↑-------------------------------------------------------↑↑↑");
//                        }
//                    });

        } else if ("com.sankuai.meituan.takeoutnew".equals(loadPackageParam.packageName) && "com.sankuai.meituan.takeoutnew".equals(loadPackageParam.processName)) {
            meituanClassLoader = loadPackageParam.classLoader;
            XposedHelpers.findAndHookMethod("com.sankuai.meituan.retrofit2.ClientCall", loadPackageParam.classLoader, "getResponseWithInterceptorChain", findClass("com.sankuai.meituan.retrofit2.Request", loadPackageParam.classLoader), new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    if (!MeiTuanHttpServer.isAlive()) {
                        MeiTuanHttpServer.start();
                        Log.e("EDXposed", "启动http服务,端口号为: " + 1111);
                    }
                    Object requestObject = param.args[0];
                    Method requestMethod = requestObject.getClass().getDeclaredMethod("url");
                    requestMethod.setAccessible(true);
                    String requestUrl = requestMethod.invoke(requestObject).toString();
                    if (requestUrl.indexOf("/meishi/poi/v1/poi/combinedinfos") > 0) {
                        URL u = new URL(requestUrl);
                        requestUrl = u.getProtocol() + "://apimeishi.meituan.com" + u.getPath() + "?" + u.getQuery();
                        Field f = requestObject.getClass().getDeclaredField("url");
                        f.setAccessible(true);
                        f.set(requestObject, requestUrl);
                        param.args[0] = requestObject;

                    }
                }

                @Override
                protected synchronized void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    //首先获取请求的url地址
                    Object requestObject = param.args[0];
                    Method requestMethod = requestObject.getClass().getDeclaredMethod("url");
                    requestMethod.setAccessible(true);
                    String requestUrl = requestMethod.invoke(requestObject).toString();
                    requestMethod = requestObject.getClass().getDeclaredMethod("method");
                    requestMethod.setAccessible(true);
                    String method = requestMethod.invoke(requestObject).toString();
//                            Log.e("EDXposed","请求方法" + method + " 请求地址为:"+requestUrl);
                    //继续下一步通过反射拿到返回数据
                    Object rawResponse = param.getResult();
                    if (rawResponse != null) {
                        Method mbody = rawResponse.getClass().getDeclaredMethod("body");
                        rawResponse = mbody.invoke(rawResponse);
                        Log.e("EDXposed", "请求方法" + method + " 请求地址为:" + requestUrl + " 返回对象类型" + rawResponse.getClass().getName());
                        if (rawResponse.getClass().getName().equals("com.sankuai.meituan.retrofit2.ResponseBodySubject") || rawResponse.getClass().getName().equals("com.sankuai.meituan.retrofit2.RawResponseSubject")) {
                            // 处理返回对象数据
                            Method sourceMethod = rawResponse.getClass().getDeclaredMethod("source");
                            InputStream is = (InputStream) sourceMethod.invoke(rawResponse);
                            //copy inputStream
                            ByteArrayOutputStream baos = IOUtils.inputConvertByteArray(is);
                            is = IOUtils.byteArrayConvertInputStream(baos);
                            String str = IOUtils.readStreamToString(is);
                            is = IOUtils.byteArrayConvertInputStream(baos);
                            //回写流操作,否则APP无法拿到数据进行展示
                            //首先通过反射构造InputStreamSubject对象
                            Class<?> inputSubject = findClass("com.sankuai.meituan.retrofit2.InputStreamSubject", loadPackageParam.classLoader);
                            Constructor<?> inputSubjectConst = inputSubject.getConstructor(InputStream.class);
                            Object inputSubjectObject = inputSubjectConst.newInstance(is);
                            //构造对象写回数据
                            Field subject = rawResponse.getClass().getDeclaredField("inputStreamSubject");
                            subject.setAccessible(true);
                            subject.set(rawResponse, inputSubjectObject);
                            baos.close();
                            if (MyThreadLocal.getInstance().get() != "") {
                                MyThreadLocal.getInstance().set(str);
                            }
//                                    LogUtils.e("EDXposed", requestUrl);
//                                    LogUtils.e("EDXposed", str);
                            if (requestUrl.contains("mtapi/v6/channel/feeds/list")) {
                                final byte[] resStr = str.getBytes("utf-8");
//                                        final Map<String, String> headMap = new HashMap<String, String>();
//                                        headMap.put("api", requestUrl);
//                                        headMap.put("Content-Type", "application/json;charset=utf-8;");
//                                        new Thread(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                Log.e("EDXposed", new String(resStr));
//                                                HttpTools.sendPost("http://49.232.3.169:5000/save", resStr, headMap);
//                                            }
//                                        }).start();
                            } else {
                                final byte[] resStr = str.getBytes("utf-8");
                                MyThreadLocal.getInstance().set(new String(resStr));
                            }
                        } else if (rawResponse.getClass().getName().equals("com.sankuai.waimai.platform.net.callfactory.b$1")) {

                            if (requestUrl.contains("api/v6/channel/feeds/list") || requestUrl.contains("api/v6/channel/feeds/tabs") || requestUrl.contains("/v8/poi/food")) {
                                // 处理返回对象数据
                                Method sourceMethod = rawResponse.getClass().getDeclaredMethod("source");
                                InputStream is = (InputStream) sourceMethod.invoke(rawResponse);
                                //copy inputStream
                                ByteArrayOutputStream baos = IOUtils.inputConvertByteArray(is);
                                is = IOUtils.byteArrayConvertInputStream(baos);
                                String str = IOUtils.readStreamToString(is);
//                                        Log.e("EDXposed", requestUrl);
//                                        LogUtils.e("EDXposed", str);
                                final byte[] resStr = str.getBytes("utf-8");
                                MyThreadLocal.getInstance().set(new String(resStr));
//                                        final Map<String, String> headMap = new HashMap<String, String>();
//                                        headMap.put("api", requestUrl);
//                                        headMap.put("device", "device2");
//                                        headMap.put("Content-Type", "application/json;charset=utf-8;");
//                                        new Thread(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                Log.e("EDXposed", new String(resStr));
//                                                HttpTools.sendPost("http://49.232.3.169:5000/save", resStr, headMap);
//                                            }
//                                        }).start();
                            }
                        }
                    }
                }
            });


            XposedHelpers.findAndHookMethod("com.sankuai.meituan.retrofit2.Retrofit", loadPackageParam.classLoader, "create", Class.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    Class<?> clazz = (Class<?>) param.args[0];
                    Log.e("EDXposed", clazz.getName());
                    switch (clazz.getName()) {
                        case "com.sankuai.waimai.store.base.net.sg.SCApiService":
                            Meituan.getInstance().setSCApiService(param.getResult());
                            break;
                        case "com.sankuai.waimai.business.restaurant.base.repository.net.ShopApiService":
                            Log.e("EDXposed", "获取到  com.sankuai.waimai.business.restaurant.base.repository.net.ShopApiService 实现类.");
                            Meituan.getInstance().setShopApiService(param.getResult());
                            break;
                        case "com.meituan.android.food.retrofit.FoodApiService$PoiDetailService":
                            Meituan.getInstance().setPoiDetailService(param.getResult());
                            break;
                        case "com.sankuai.waimai.business.search.api.WaimaiSearchService":
                            Meituan.getInstance().setWaimaiSearchService(param.getResult());
                            break;
                        case "com.sankuai.waimai.store.search.common.api.net.SCSearchApiService":
                            Log.e("EDXposed", "获取到  com.sankuai.waimai.store.search.common.api.net.SCSearchApiService 实现类.");
                            Meituan.getInstance().setSCSearchApiService(param.getResult());
                    }
                }
            });
            //保存rd.j对象
            XposedHelpers.findAndHookMethod("com.sankuai.waimai.platform.capacity.network.retrofit.b", loadPackageParam.classLoader, "a", findClass("rx.d", loadPackageParam.classLoader), findClass("rx.j", loadPackageParam.classLoader), Object.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    rx_jObject = param.args[1];
                    Log.e("EDXposed", "已获得rd.j对象");
                }
            });
        } else if (loadPackageParam.packageName.equals("com.ss.android.ugc.aweme") && loadPackageParam.processName.equals("com.ss.android.ugc.aweme")) {
            classLoader = loadPackageParam.classLoader;
            if (!DouYinHttpServer.isAlive()) {
                DouYinHttpServer.start();
                Log.e("EDXposed", "启动http服务,端口号为: " + 6655);
            } else {
                Log.e("EDXposed", "http服务以启动,端口号为: " + 6655);
            }
            XposedHelpers.findAndHookMethod("ms.bd.c.h", loadPackageParam.classLoader, "a", int.class, int.class, long.class, String.class, Object.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    if (param.args[3].toString().contains("v1/user/profile/other/")) {
                        param0 = Integer.valueOf(param.args[0].toString());
                        Log.e("EDXposed", "param1:\t" + (param0));
                        param1 = Integer.valueOf(param.args[1].toString());
                        Log.e("EDXposed", "param1:\t" + (param1));
                        param2 = Long.valueOf(param.args[2].toString());
                        Log.e("EDXposed", "param2:\t" + (param2));
                        param3 = param.args[3].toString();
                        Log.e("EDXposed", "param3:\t" + (param3));
                        param4 = param.args[4];
                        Log.e("EDXposed", "param4:\t" + JSONObject.toJSONString(param4));
                        Log.e("EDXposed", "==============================================================================================================================");
                    }
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });
        } else if (loadPackageParam.packageName.equals("com.kuaikan.comic") && loadPackageParam.processName.equals("com.kuaikan.comic")) {
            XposedHelpers.findAndHookMethod("com.kuaikan.comic.manager.KKSignManager", loadPackageParam.classLoader, "a", long.class, long.class, SortedMap.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    if (!youxuanHttpServer.isAlive()) {
                        youxuanHttpServer.start();
                        Log.e("EDXposed", "启动http服务,端口号为: " + 9000);
                    } else {
                        Log.e("EDXposed", "http服务以启动,端口号为: " + 9000);
                    }
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    kkObject = param.thisObject;
                    Log.e("EDXposed", "==============================================================================================================================================================================================");
                    SortedMap<String, String> sortedMap = (SortedMap<String, String>) param.args[2];
                    Log.e("EDXposed", param.args[0].toString());
                    Log.e("EDXposed", param.args[1].toString());
                    SortedMap<String, String> t = new TreeMap<String, String>();
                    for (Map.Entry<String, String> entry : sortedMap.entrySet()) {
                        if (!(entry == null || entry.getKey() == null || entry.getValue() == null)) {
                            Log.e("EDXposed", entry.getKey() + ":" + entry.getValue());
                            t.put(entry.getKey(), entry.getValue());
                        }
                    }
                    Log.e("EDXposed", param.getResult().toString());
                    Log.e("EDXposed", "==============================================================================================================================================================================================");
                }
            });
        } else if ("com.manmanbuy.bijia".equals(loadPackageParam.packageName) && "com.manmanbuy.bijia".equals(loadPackageParam.processName)) {
            XposedHelpers.findAndHookMethod("com.stub.StubApp", loadPackageParam.classLoader, "attachBaseContext", Context.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);

                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    Log.e("EDXposed", "进入慢慢买app...");
//                               获取壳的ClassLoader
                    Context context = (Context) param.args[0];
                    final ClassLoader classLoader = context.getClassLoader();

                    XposedHelpers.findAndHookMethod(classLoader.loadClass("com.maochunjie.mencryptsign.RNReactNativeMencryptSignModule"), "getToken", String.class, new XC_MethodHook() {
                        @Override
                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                            super.beforeHookedMethod(param);
                            Log.e("EDXposed", "入参到Navite层值为: " + param.args[0].toString());
                            XposedHelpers.findAndHookMethod("com.maochunjie.mencryptsign.MD5Util", classLoader, "getMD5String", String.class, new XC_MethodHook() {
                                @Override
                                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                                    super.beforeHookedMethod(param);
                                    Log.e("EDXposed", "入参到java MD5加密函数,  参数为:   " + param.args[0].toString());
                                }
                            });
                        }
                    });
                }
            });
        } else if ("rca.rc.tvtaobao".equals(loadPackageParam.packageName) && "rca.rc.tvtaobao".equals(loadPackageParam.processName)) {
            tbTvClassLoader = loadPackageParam.classLoader;
            XposedHelpers.findAndHookMethod("mtopsdk.security.InnerSignImpl", tbTvClassLoader, "getMtopApiSign", HashMap.class, String.class, String.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                    if (!taobaoTvHttpServer.isAlive()) {
                        taobaoTvHttpServer.start();
                        Log.e("EDXposed", "启动http服务,端口号为: " + 9001);
                    } else {
                        Log.e("EDXposed", "http服务以启动,端口号为: " + 9001);
                    }
                    tbTvSignObject = param.thisObject;
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });
        } else {
        }
        //其他app使用else if判断其包名
    }


    private static String getPackageVersion(XC_LoadPackage.LoadPackageParam lpparam) {
        try {
            Class<?> parserCls = findClass("android.content.pm.PackageParser", lpparam.classLoader);
            Object parser = parserCls.newInstance();
            File apkPath = new File(lpparam.appInfo.sourceDir);
            Object pkg = XposedHelpers.callMethod(parser, "parsePackage", new Object[]{apkPath, 0});
            String versionName = (String) XposedHelpers.getObjectField(pkg, "mVersionName");
            int versionCode = XposedHelpers.getIntField(pkg, "mVersionCode");
            return String.format("%s (%d)", versionName, Integer.valueOf(versionCode));
        } catch (Throwable th) {
            return "(unknown)";
        }
    }

    public static Map<String, String> randomBrand() {
        Map<String, String> map = new HashMap<String, String>();
        String Huawei[] = {"nova 8 SE", "nova 7", "nova 6", "nova 5z", "nova 5i Pro", "nova 5i", "nova 5 Pro", "nova 5", "nova 4e", "nova 4", "nova 3i", "nova 3", "P40 Pro+", "P40", "P30 Pro", "P30", "P20 Pro", "P20", "P10 Plus", "P9", "P8", "P8 Max", "Mate 40 RS", "Mate 40 Pro+", "Mate 40 Pro", "Mate 40", "Mate Xs", "Mate 30 RS", "Mate 30E Pro", "Mate 30 Pro", "Mate 30", "Mate 20 RS"};
        String Xiaomi[] = {"MIX", "MIX 2", "MIX 2S", "MIX 3", "Max 2", "Max 3", "Redmi 7", "Redmi 7A", "Redmi 8", "Redmi 8A", "Redmi 9", "Redmi 9A", "Redmi Note 7", "Redmi Note 7 Pro", "Redmi Note 8", "Redmi Note 8T", "Redmi Note 8 Pro", "Redmi Note 9", "Redmi K20", "Redmi K20 Pro", "Redmi K30", "Redmi K30 Pro"};
        Random random = new Random();
        int type = random.nextInt(2);
        switch (type) {
            case 0:
                String model = Huawei[random.nextInt(Huawei.length)];
                map.put("brand", "Huawei");
                map.put("model", model);
                map.put("build_manu", "Huawei");
                map.put("build_brand", "Huawei");
                map.put("hd_brand", "Huawei");
                break;
            case 1:
                model = Xiaomi[random.nextInt(Xiaomi.length)];
                map.put("brand", "Xiaomi");
                map.put("model", model);
                map.put("build_manu", "Xiaomi");
                map.put("build_brand", "Xiaomi");
                map.put("hd_brand", "Xiaomi");
                break;
            default:
                break;
        }
        return map;
    }

    /**
     * 随机软件版本号
     *
     * @return
     */
    public static String randomVersion() {
        String[] appVersionArray = new String[13];
        appVersionArray[0] = "10.41.16";
        appVersionArray[1] = "10.41.15";
        appVersionArray[2] = "10.41.12";
        appVersionArray[3] = "10.40.4";
        appVersionArray[4] = "10.39.13";
        appVersionArray[5] = "10.38.13";
        appVersionArray[6] = "10.38.4";
        appVersionArray[7] = "10.37.13";
        appVersionArray[8] = "10.37.3";
        appVersionArray[9] = "10.36.12";
        appVersionArray[10] = "10.36.3";
        appVersionArray[11] = "10.35.12";
        appVersionArray[12] = "10.35.2";
        int index = new Random().nextInt(appVersionArray.length);
        return appVersionArray[index];
    }

    /**
     * 随机固定长度字符串
     *
     * @param length
     * @return
     */
    public static String randomStr(int length) {
        String[] strs = {"a", "b", "c", "d", "e", "f", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            String temStr = strs[new Random().nextInt(strs.length)];
            sb.append(temStr);
        }
        return sb.toString();
    }

    /**
     * 随机mac地址
     *
     * @return
     */
    public static String randomMac() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            sb.append(randomStr(2) + ":");
        }
        return sb.substring(0, sb.toString().length() - 1);
    }

    public void startServer(int prot) {
        HttpServer httpServer = new HttpServer(prot);
        String message = "";
        if (!httpServer.isAlive()) {
            try {
                httpServer.start();
            } catch (IOException e) {
                e.printStackTrace();
                message = e.getMessage();
            }
            System.out.println(Build.VERSION.RELEASE);
            status = httpServer.isAlive();
            Log.d(getClass().getName(), "onCreate: 启动http服务..." + httpServer.isAlive());
//            TextView textView =(TextView) MainActivity.findViewById(R.id.text1);
//
//            textView.setText("启动端口号"+prot+"的http服务:" + (httpServer.isAlive()==true?"启动成功.":"启动失败("+message+")"));
            log("启动端口号" + prot + "的http服务:" + (httpServer.isAlive() == true ? "启动成功." : "启动失败(" + message + ")"));
        }
    }

}
