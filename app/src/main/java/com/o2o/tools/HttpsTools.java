package com.o2o.tools;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

/**
 * Created by Administrator on 2018/3/26.
 *
 * @author QuJianJun
 */
public class HttpsTools {
	/**
	 * 获取网站返回内容
	 * @param
	 * @return
	 */
	public static String Getcode(String requsturl,String requestMethod,String encode,String referer,String outputStr, Map<String,String> headers){
        StringBuffer buffer = new StringBuffer();
        if (null==requestMethod || requestMethod.equals("GET")) {
        	requestMethod="GET";
		}
        if(encode==null){
        	encode="UTF-8";
        }
		 try {  
	            // 创建SSLContext对象，并使用我们指定的信任管理器初始化  
	            TrustManager[] tm = { new MyX509TrustManager() };  
	            SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");  
	            sslContext.init(null, tm, new java.security.SecureRandom());  
	            // 从上述SSLContext对象中得到SSLSocketFactory对象  
	            SSLSocketFactory ssf = sslContext.getSocketFactory();  
	  
	            URL url = new URL(requsturl);  
	            HttpsURLConnection httpUrlConn = (HttpsURLConnection) url.openConnection();  
	            httpUrlConn.setSSLSocketFactory(ssf);
				for (Map.Entry map:headers.entrySet()){
					 httpUrlConn.setRequestProperty(map.getKey().toString(),map.getValue().toString());
				}
	            httpUrlConn.setDoOutput(true);  
	            httpUrlConn.setDoInput(true);  
	            httpUrlConn.setUseCaches(false);  
	            // 设置请求方式（GET/POST）  
	            httpUrlConn.setRequestMethod(requestMethod);  
	  
	            if ("GET".equalsIgnoreCase(requestMethod)) {
					httpUrlConn.connect();
				}
	            // 当有数据需要提交时  
	            if (null != outputStr) {  
	                OutputStream outputStream = httpUrlConn.getOutputStream();
	                // 注意编码格式，防止中文乱码  
	                outputStream.write(outputStr.getBytes(encode));  
	                outputStream.close();  
	            }  
	  
	            // 将返回的输入流转换成字符串  
	            InputStream inputStream = httpUrlConn.getInputStream();  
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, encode);  
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

	            String str = null;

	            while ((str = bufferedReader.readLine()) != null) {  
	                buffer.append(str+"\n");  
	            }  
	            bufferedReader.close();  
	            inputStreamReader.close();  
	            // 释放资源  
	            inputStream.close();  
	            inputStream = null;  
	            httpUrlConn.disconnect();  
	  
	        } catch (Exception e) {  
	            e.printStackTrace();
	        }  
	        return buffer.toString();  
  
	}
	
}
