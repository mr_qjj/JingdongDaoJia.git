package com.o2o.tools;

import android.util.Log;

public class LogUtils {
    /**
     * 分割打印字符串
     *
     * @param tag
     * @param str
     */
    public static void e(String tag, String str) {
        if (str.length() > 4000) {
            for (int i = 0; i < str.length(); i += 3000) {
                if (i + 4000 < str.length()) {
                    Log.e(tag, str.substring(i, i + 3000));
                } else {
                    Log.e(tag, str.substring(i, str.length()));
                }
            }
        } else
            Log.e(tag, str);
    }
}
