package com.o2o.tools;

import android.util.Log;

import com.alibaba.fastjson.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/3/26.
 *
 * @author QuJianJun
 */
public class HttpTools {
	/**
	 * 返回响应头信息
	 * @param url
	 * @return
	 */
	public static String GetHttpHeader(String url,String referer){
			String result = "";
			try {
				URL u = new URL(url);
				HttpURLConnection conn = (HttpURLConnection)u.openConnection();
				conn.setRequestMethod("GET"); 
				conn.addRequestProperty("Referer", referer);
				conn.setRequestProperty("User-Agent", "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4");
				//必须设置false，否则会自动redirect到重定向后的地址 
	            conn.setInstanceFollowRedirects(false);
	            conn.connect();
	            if (conn.getResponseCode() == 302 || conn.getResponseCode() == 301) { 
	                //如果会重定向，保存302重定向地址，以及Cookies,然后重新发送请求(模拟请求)
	                result = conn.getHeaderField("Location"); 
	            //    Map<String, List<String>> map = conn.getHeaderFields();
	        //        for (String key : map.keySet()) {
	        //        	System.out.println(key+"    ----->"+ map.get(key));
	        //        }
	        //        GetHttpHeader(result,"");
	            }
	            
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
	}
	
	/**
	 * 获取网站返回内容
	 * @param url
	 * @return
	 */
	public static byte[] Getcode(String url,String encode,Map<String,String> headers){
		if(encode==""){encode="UTF-8";}
		byte[] result;
		String temp="";
		try {
			URL u = new URL(url);
			HttpURLConnection conn = (HttpURLConnection)u.openConnection();
			// 设置通用的请求属性
			for (Map.Entry map:headers.entrySet()){
				conn.setRequestProperty(map.getKey().toString(),map.getValue().toString());
			}
			//连接远程服务器超时设置(毫秒)
			conn.setConnectTimeout(20000);
			//读取内容超时设置
			conn.setReadTimeout(20000);
			Map<String, List<String>> map = conn.getHeaderFields();
//			for (Map.Entry m : map.entrySet()){
//				System.out.println(m.getKey()+":"+m.getValue());
//			}
			return readBytes(conn.getInputStream());
		} catch (Exception e) {
			System.out.println("***************************************************************异常地址:"+url);
			e.printStackTrace();
		}
		
		return null;
	}

	 /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url
     *            发送请求的 URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static byte[] sendPost(String url, byte[] param, Map<String,String> headers) {
		OutputStream out = null;
        BufferedReader in = null;
        byte[] result;
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            HttpURLConnection conn = (HttpURLConnection)realUrl.openConnection();
            // 设置通用的请求属性
			for (Map.Entry map:headers.entrySet()){
				conn.setRequestProperty(map.getKey().toString(),map.getValue().toString());
			}
			conn.setConnectTimeout(3000);
			conn.setReadTimeout(3000);
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            //post数据
            if (param != null){
                OutputStream outputStream = conn.getOutputStream();
                // 注意编码格式，防止中文乱码
                outputStream.write(param);
                outputStream.close();
            }
            // 获取URLConnection对象对应的输出流
			InputStream inStream = conn.getInputStream();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			byte[] buffer = new byte[4096];
			int len = 0;
			while( (len=inStream.read(buffer)) != -1 ){
				outStream.write(buffer, 0, len);
			}
			inStream.close();
			result =  outStream.toByteArray();
			return result;
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！"+"***************************异常地址:"+url+"异常参数:"+param);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
		return null;
    }


	/**
	 * 读取http流中的byte数组数据
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	private static byte[] readBytes(InputStream inputStream) throws IOException {
		Object[] objArr = {inputStream};
		BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
		byte[] bArr = new byte[1024];
		while (true) {
			int read = bufferedInputStream.read(bArr);
			if (read != -1) {
				byteArrayOutputStream.write(bArr, 0, read);
			} else {
				bufferedInputStream.close();
				return byteArrayOutputStream.toByteArray();
			}
		}
	}

    /**
     * 获取网站返回内容
     * @param url
     * @return
     */
    public static String Getcode(String url,String encode,String referer,String cookie){
        if(encode==""){encode="UTF-8";}
        String result = "";
        String temp="";
        try {
            URL u = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)u.openConnection();
            conn.setRequestProperty("Cookie", cookie);
            conn.setRequestProperty("Referer", referer);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1");
            //连接远程服务器超时设置(毫秒)
            conn.setConnectTimeout(20000);
            //读取内容超时设置
            conn.setReadTimeout(20000);
            InputStreamReader isr = new InputStreamReader(conn.getInputStream(),encode);
            BufferedReader br = new BufferedReader(isr);
            while((temp = br.readLine()) != null){
                result +=temp+"\n";
            }
            if (isr!=null) {
                isr.close();
            }
        } catch (Exception e) {
            Log.e("EDXposed", "***********************异常地址:" + url);
            Map<String,Object> map = new HashMap<>();
            map.put("code",500);
            map.put("msg", "地址请求异常,无法打开或者内部报错..." + e.getMessage());
            map.put("url",url);
            e.printStackTrace();
            return JSONObject.toJSONString(map);
        }
        return result;
    }
}
