package com.o2o.server;


import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;

import com.jingdong.common.utils.BitmapkitUtils;
import com.meituan.youxuan.ApiEvent;
import com.meituan.youxuan.Meituan;
import com.o2o.tools.*;
import com.zhihu.android.cloudid.CloudIDHelper;


import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import cn.o2o.jingdongdaojia.MainActivity;
import de.robv.android.xposed.XposedHelpers;
import fi.iki.elonen.NanoHTTPD;
import jd.net.z;
import oO.oz.o0s.o.AppApiCommonParameter;

public class HttpServer extends NanoHTTPD {
    public static final String model = "AES/CBC/NoPadding";
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final byte[] key;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public static final byte[] iv;

    static {
        byte[] bArr = {92, 115, 116, 117, 112, 113, 6, 112, 112, 3, 3, 4, 6, 118, 0, 112};
        byte b2 = 24;
        for (int i = 0; i < bArr.length; i++) {
            b2 = (byte) (b2 ^ (bArr[i] & 255));
            bArr[i] = b2;
        }
        key = bArr;
        byte[] bArr2 = {0, 118, 122, 10, 3, 116, 124, 10, 5, 117, 6, 5, 3, 4, 2, 37};
        byte b3 = 97;
        for (int length = bArr2.length - 1; length >= 0; length--) {
            b3 = (byte) (b3 ^ (bArr2[length] & 255));
            bArr2[length] = b3;
        }
        iv = bArr2;
        System.out.println("Key:" + new String(key));
        System.out.println("IV:" + new String(iv));
    }

    public HttpServer(int port) {
        super(port);
    }

    public HttpServer(String hostname, int port) {
        super(hostname, port);
    }

    //开放http服务请求到这里

    @Override
    public synchronized Response serve(IHTTPSession session) {
        Method method = session.getMethod();
        Map<String, String> header = session.getHeaders();

        Map<String, String> parameters = session.getParms();
        InputStream in = HttpServer.class.getResourceAsStream(session.getUri());
        String uri = session.getUri().substring(1);
        StringBuffer sb = new StringBuffer();
        Map<String, Object> responseMap = new HashMap<String, Object>();
        if (Method.GET.equals(method)) {
            //get方式
            String queryParams = session.getQueryParameterString();
            sb.append("<center><H1>WelCome to My Mini WebServer.</H1></center>");
            return newFixedLengthResponse(Response.Status.OK, "text/html; charset=UTF-8;", sb.toString());
        } else if (Method.POST.equals(method)) {
            //post方式
            Map<String, String> files = new HashMap<String, String>();
            try {
                session.parseBody(files);
                String data = files.get("postData");
                //解析数据
                if ("getUrl".equals(parameters.get("type"))) {
                    //处理数据参数加密前的操作
                    String source = new String(Base64.decode(data.getBytes("utf-8")));
                    String url = JSONObject.parseObject(source).get("url").toString();
                    String result = z.returnResult(url);
                    sb.append(result);
                    return newFixedLengthResponse(Response.Status.OK, "Application/json; charset=UTF-8", sb.toString());
                }else if("jianying".equals(parameters.get("type"))){
                    if ("create".equals(parameters.get("event"))){
                        synchronized (this) {
                            JSONObject j = JSONObject.parseObject(data);
                            String article_title = j.getString("article_title");
                            String article_content = j.getString("article_content");
                            Map<String, Object> map = new HashMap<>();
                            map.put("article_title", article_title);
                            map.put("article_content", article_content);
                            map.put("mode", 0);
                            map.put("only_tts", false);
                            map.put("only_gif", false);
                            map.put("recommend_template", true);
                            map.put("is_tail_enable", false);
                            String createTaskStr = JSONObject.toJSONString(map);
                            Constructor<?> m = InitXposedHook.jianyingFObject.getClass().getConstructor(String.class);
                            m.setAccessible(true);
                            InitXposedHook.jianyingFObject = m.newInstance(createTaskStr);


                            java.lang.reflect.Method submitMethod = InitXposedHook.jianyingTextToVideoObject.getClass().getDeclaredMethod("submitGenerateVideoTask", InitXposedHook.jianyingFObject.getClass());
                            submitMethod.setAccessible(true);
                            Object callObj = submitMethod.invoke(InitXposedHook.jianyingTextToVideoObject, InitXposedHook.jianyingFObject);
                            Object execObj = callObj.getClass().getMethod("execute").invoke(callObj);
                            Object result = execObj.getClass().getMethod("body").invoke(execObj);
                            Log.e("EDXposed", JSONObject.toJSONString(result));
                            return newFixedLengthResponse(Response.Status.OK, "Application/json; charset=UTF-8", JSONObject.toJSONString(result));
                        }
                    }else if ("query".equals(parameters.get("event"))){
                        synchronized (this) {
                            JSONObject j = JSONObject.parseObject(data);
                            String event_id = j.getString("event_id");
                            Map<String, Object> map = new HashMap<>();
                            map.put("event_id", event_id);
                            String createTaskStr = JSONObject.toJSONString(map);
                            Constructor<?> m = InitXposedHook.jianyingFObject.getClass().getConstructor(String.class);
                            m.setAccessible(true);
                            InitXposedHook.jianyingFObject = m.newInstance(createTaskStr);


                            java.lang.reflect.Method submitMethod = InitXposedHook.jianyingTextToVideoObject.getClass().getDeclaredMethod("queryGenerateVideoTask", InitXposedHook.jianyingFObject.getClass());
                            submitMethod.setAccessible(true);
                            Object callObj = submitMethod.invoke(InitXposedHook.jianyingTextToVideoObject, InitXposedHook.jianyingFObject);
                            Object execObj = callObj.getClass().getMethod("execute").invoke(callObj);
                            Object result = execObj.getClass().getMethod("body").invoke(execObj);
                            return newFixedLengthResponse(Response.Status.OK, "Application/json; charset=UTF-8", JSONObject.toJSONString(result));
                        }
                    }
                } else if ("36kr".equals(parameters.get("type"))) {
                    Class<?> sigClass = InitXposedHook.kr36ClassLoader.loadClass("com.android36kr.app.utils.EncryptTools");
                    java.lang.reflect.Method getSign = sigClass.getMethod("getSign", String.class);
                    String res = getSign.invoke(null, data).toString();
                    responseMap.put("data", res);
                    responseMap.put("code", 200);
                    return newFixedLengthResponse(Response.Status.OK, "Application/json; charset=UTF-8", JSONObject.toJSONString(responseMap));
                } else if ("weipinhui".equals(parameters.get("type"))) {
                    TreeMap<String, String> paramMap = JSONObject.parseObject(data, TreeMap.class);
                    Class<?> secClass = InitXposedHook.vipClassLoader.loadClass("com.vip.vcsp.security.api.VCSPSecurityBasicService");
                    java.lang.reflect.Method apiSign = secClass.getDeclaredMethod("apiSign", Context.class, TreeMap.class, String.class);
                    String result = apiSign.invoke(null, null, paramMap, null).toString();
                    responseMap.put("code", 200);
                    responseMap.put("msg", result);
                } else if ("gaode".equals(parameters.get("type"))) {
                    String type = JSONObject.parseObject(data).getString("type");
                    String d = JSONObject.parseObject(data).getString("data");
                    if ("encrypt".equals(type)) {
                        String res = XposedHelpers.callStaticMethod(XposedHelpers.findClass("com.autonavi.jni.server.aos.ServerkeyNative", InitXposedHook.gaoDeClassLoadedr), "amapEncode", d).toString();
                        responseMap.put("code", 200);
                        responseMap.put("msg", res);
                    } else if ("decrypt".equals(type)) {
                        String res = XposedHelpers.callStaticMethod(XposedHelpers.findClass("com.autonavi.jni.server.aos.ServerkeyNative", InitXposedHook.gaoDeClassLoadedr), "amapDecode", d).toString();
                        responseMap.put("code", 200);
                        responseMap.put("msg", res);
                    } else if ("header".equals(type)) {
                        String t = String.valueOf(System.currentTimeMillis() / 1000);
                        java.lang.reflect.Method m = InitXposedHook.gaoDeAosEncryptor.getClass().getDeclaredMethod("virtualV2Sign", String[].class);
                        String[] params = new String[]{d, t, "/ws/shield/search_poi/report/impression", "false"};
                        HashMap<String, String> result = (HashMap<String, String>) m.invoke(InitXposedHook.gaoDeAosEncryptor, (Object) params);
                        result.put("x-t", t);
                        result.put("x-appkey", "21792629");
                        result.put("Content-Type", "application/x-www-form-urlencoded");
                        result.put("x-pv", "6.3");
                        result.put("X-Requested-With", "X-Requested-With");
                        responseMap.put("code", 200);
                        responseMap.put("msg", JSONObject.toJSONString(result));
                    } else {
                        responseMap.put("code", 403);
                        responseMap.put("msg", "未知的方法名称.");
                    }
                } else if ("zhihu".equals(parameters.get("type"))) {
                    String time = parameters.get("t");
                    String udid = null;
                    try {
                        udid = parameters.get("udid");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String res = CloudIDHelper.getInstance().encrypt("2", udid, null, data, "1355", time,
                            "dd49a835-56e7-4a0f-95b5-efd51ea5397f");
                    responseMap.put("code", 200);
                    responseMap.put("data", res);
                } else if ("taobao".equals(parameters.get("type"))) {
                    String keyWord = parameters.get("keyWord");
                    String deviceId = Utils.createRandomStr1(15) + "-" + Utils.createRandomStr1(17) + "_" + Utils.createRandomStr1(10);
                    String utdid = Utils.createRandomStr1(24);
                    String time = String.valueOf(System.currentTimeMillis() / 1000);
                    HashMap<String, String> paramMap = new HashMap<>();
                    paramMap.put("data", "{\"entityId\":\"1220\",\"env\":\"1\",\"params\":\"{\\\"context\\\":null,\\\"query\\\":\\\"" + keyWord + "\\\"}\",\"bizVersion\":\"3\"}");
                    paramMap.put("deviceId", deviceId);
                    paramMap.put("sid", null);
                    paramMap.put("uid", null);
                    paramMap.put("x-features", "27");
                    paramMap.put("appKey", "21646297");
                    paramMap.put("api", "mtop.cogman.execute.nologin");
                    paramMap.put("utdid", utdid);
                    paramMap.put("ttid", "1568707461534@taobao_android_9.0.0");
                    paramMap.put("t", time);
                    paramMap.put("v", "1.0");
                    HashMap<String, String> global = new HashMap<>();
                    global.put("pageId", "http://h5.m.taobao.com/tnode/index.htm");
                    global.put("pageName", "com.taobao.android.layoutmanager.container.CommonContainerActivity");
                    java.lang.reflect.Method signMethod = InitXposedHook.signObject.getClass().getDeclaredMethod("a", HashMap.class, HashMap.class, String.class, String.class, boolean.class);
                    signMethod.setAccessible(true);
                    Object res = signMethod.invoke(InitXposedHook.signObject, paramMap, global, "21646297", null, false);
                    //转换最终map集合,
                    paramMap.putAll((HashMap<String, String>) res);
                    paramMap.putAll(global);
                    paramMap.put("nq", "4G");
                    paramMap.put("pv", "6.3");
                    paramMap.put("x-app-conf-v", "0");
                    paramMap.put("umt", paramMap.get("x-umt"));
                    paramMap.remove("x-umt");
                    paramMap.put("sign", paramMap.get("x-sign"));
                    paramMap.remove("x-sign");
                    paramMap.put("api", "mtop.taobao.maserati.personal.works");
                    paramMap.put("f-refer", "mtop");
                    paramMap.put("netType", "LTE");
                    paramMap.put("x-app-ver", "9.0.0");
                    paramMap.put("x-c-traceid ", Utils.createRandomStr1(24) + "16115961475070056127355");
                    paramMap.put("user-agent", "MTOPSDK/3.1.1.7 (Android;10;Xiaomi;M2003J15SC)");
                    paramMap.put("x-page-url", "http://h5.m.taobao.com/tnode/index.htm");
                    paramMap.put("x-page-name", "com.taobao.android.layoutmanager.container.CommonContainerActivity");
                    global.clear();
                    global.put("x-region-channel", "CN");
                    global.put("x-bx-version", "6.4.11");
                    java.lang.reflect.Method m = InitXposedHook.convertObject.getClass().getMethod("buildRequestHeaders", Map.class, Map.class, boolean.class);
                    Object resObject = m.invoke(InitXposedHook.convertObject, paramMap, global, false);
                    Map<String, String> resMap = (Map<String, String>) resObject;
                    resMap.put("getUrl", "https://guide-acs.m.taobao.com/gw/mtop.cogman.execute.nologin/1.0/?data=" + URLEncoder.encode(paramMap.get("data")));
                    return newFixedLengthResponse(Response.Status.OK, "Application/json; charset=UTF-8", JSONObject.toJSONString(resObject));
                } else if ("pinduoduo".equals(parameters.get("type"))) {
                    String url = parameters.get("url");
                    if (InitXposedHook.antiTokenObject != null) {
                        java.lang.reflect.Method m = InitXposedHook.antiTokenObject.getClass().getMethod("a", String.class, boolean.class);
                        Object resObj = m.invoke(InitXposedHook.antiTokenObject, url, true);
                        HashMap<String, String> hashMap = (HashMap<String, String>) resObj;
                        hashMap.put("Cookie", "acid=9e3eab37f7c0fd810130c404da18d16f; JSESSIONID=9CA429A447F55931027AB5809A08B355; api_uid=CksH12AffllFogBQNa/uAg==");
                        hashMap.put("p-mediainfo", "player=1.0.3&rtc=1.0.0");
                        hashMap.put("accept", "application/json, text/plain, */*");
                        hashMap.put("etag", "wKRFikOx");
                        hashMap.put("accesstoken", "WAQ74ITJJ6LICS26WPS2GAQIRAS3MH4SV7Q3X45XWP7ZJ3M2TLGA1115318");
                        hashMap.put("referer", "https://m.pinduoduo.net/brand_activity_subsidy.html?_pdd_fs=1&_pdd_tc=ffffff&_pdd_sbs=1&access_from=home&index_banner_type=0");
                        hashMap.put("p-mode", "1");
                        hashMap.put("p-appname", "pinduoduo");
                        hashMap.put("x-pdd-info", "bold_free%3Dfalse%26bold_product%3D");
                        hashMap.put("p-proc", "main");
                        hashMap.put("x-b3-ptracer", "httpCall461a2ee61d1d47ce8e6744a5");
                        hashMap.put("user-agent", "android Mozilla/5.0 (Linux; Android 10; Redmi Note 8 Build/QKQ1.200114.002; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/83.0.4103.101 Mobile Safari/537.36  phh_android_version/5.50.1 phh_android_build/3b6f9af09fc4d141dc42ced98246d5231069f200 phh_android_channel/hw pversion/0");
                        hashMap.put("pdd-config", "V4:001.055001");
                        hashMap.put("multi-set", "0,0");
                        hashMap.put("Content-Type", "application/json;charset=UTF-8");
                        return newFixedLengthResponse(Response.Status.OK, "Application/json; charset=UTF-8", JSONObject.toJSONString(hashMap));
                    } else {
                        Map<String, String> map = new HashMap<>();
                        map.put("code", "500");
                        map.put("msg", "内部错误,对象类获取失败...");
                        return newFixedLengthResponse(Response.Status.OK, "Application/json; charset=UTF-8", JSONObject.toJSONString(map));
                    }
                } else if ("dianping".equals(parameters.get("type"))) {
                    String url = parameters.get("url");
                    Log.e("EDXposed", url);
                    if (InitXposedHook.dianpingObject != null) {
                        Class<?> clazz = XposedHelpers.findClass("com.dianping.archive.DPObject", InitXposedHook.dianpingClassLoader);
                        Constructor<?> constructor = XposedHelpers.findConstructorExact(clazz, byte[].class, int.class, int.class);
                        constructor.setAccessible(true);
                        /**
                         * 先修改复制的g字段对象后写入当前类的g字段覆盖在调用当前类的a函数
                         */
                        try {
                            MyThreadLocal.getInstance().set("getHttpResult");
                            //写入url
                            Field urlField = InitXposedHook.dianpingParamRequest.getClass().getDeclaredField("f");
                            urlField.setAccessible(true);
                            urlField.set(InitXposedHook.dianpingParamRequest, url);


                            //写入v的值
//                                Field methodField = InitXposedHook.dianpingParamRequest.getClass().getDeclaredField("u");
//                                methodField.setAccessible(true);
//                                methodField.set(InitXposedHook.dianpingParamRequest, "GET: " + url);
                            //写入m的值
                            Field mField = InitXposedHook.dianpingParamRequest.getClass().getDeclaredField("v");
                            mField.setAccessible(true);
                            mField.set(InitXposedHook.dianpingParamRequest, "GET: " + url.replace("https://", "http://"));
                            java.lang.reflect.Method execMethod = InitXposedHook.dianpingObject.getClass().getDeclaredMethod("execSync",
                                    XposedHelpers.findClass("com.dianping.nvnetwork.Request", InitXposedHook.dianpingClassLoader));
                            Object responseObj = execMethod.invoke(InitXposedHook.dianpingObject, InitXposedHook.dianpingParamRequest);
                            Field[] fields = responseObj.getClass().getDeclaredFields();
                            int responseCode = 0;
                            String errorMsg = "";
                            for (Field field : fields) {
                                field.setAccessible(true);
                                Log.e("EDXposed", "字段名称: " + field.getName() + " \t 值内容: " + field.get(responseObj));
                                Field i = responseObj.getClass().getDeclaredField("i");
                                i.setAccessible(true);
                                //响应状态码
                                if (field.getName().equals("h")) {
                                    responseCode = (int) field.get(responseObj);
                                } else if (field.getName().equals("g")) {
                                    errorMsg = field.get(responseObj) == null ? "" : field.get(responseObj).toString();
                                }
                            }
                            //响应数据
                            Field responseField = responseObj.getClass().getDeclaredField("m");
                            responseField.setAccessible(true);
                            byte[] resBytes = (byte[]) responseField.get(responseObj);
                            if (resBytes != null && responseCode == 200) {
                                byte[] resultBytes = EncryptUtils.decrypt(resBytes);
                                String sourceData = android.util.Base64.encodeToString(resBytes, 0);
                                if (url.contains("searchshop.api")) {
                                    Object instance = constructor.newInstance(resultBytes, 0, resultBytes.length);
                                    Class<?> SearchShopApiResult = XposedHelpers.findClass("com.dianping.model.SearchShopApiResult", InitXposedHook.dianpingClassLoader);
                                    Object ba = XposedHelpers.getStaticObjectField(SearchShopApiResult, "aY");
                                    Object object = XposedHelpers.callMethod(instance, "a", ba);
                                    String searchShopResult = (String) XposedHelpers.callMethod(object, "toJson");
                                    responseMap.put("data", searchShopResult);
                                    responseMap.put("code", responseCode);
                                    responseMap.put("sourceData", sourceData);
                                } else if (url.contains("getfeedcontentlist.bin")) {
                                    //全部评论
                                    Object instance = constructor.newInstance(resultBytes, 0, resultBytes.length);
                                    Class<?> ReviewFeedList = XposedHelpers.findClass("com.dianping.model.ReviewFeedList", InitXposedHook.dianpingClassLoader);
                                    Object y = XposedHelpers.getStaticObjectField(ReviewFeedList, "y");
                                    Object object = XposedHelpers.callMethod(instance, "a", y);
                                    String feedContentList = (String) XposedHelpers.callMethod(object, "toJson");
                                    responseMap.put("data", feedContentList);
                                    responseMap.put("code", responseCode);
//                                            responseMap.put("sourceData", sourceData);
                                } else if (url.contains("shop.bin")) {
                                    //获取店铺详情
                                    sourceData = android.util.Base64.encodeToString(resBytes, 0);
                                    Object instance = constructor.newInstance(resultBytes, 0, resultBytes.length);
                                    Class<?> Shop = XposedHelpers.findClass("com.dianping.model.Shop", InitXposedHook.dianpingClassLoader);
                                    Object ey = XposedHelpers.getStaticObjectField(Shop, "ey");
                                    Object object = XposedHelpers.callMethod(instance, "a", ey);
                                    String detailResult = (String) XposedHelpers.callMethod(object, "toJson");
                                    responseMap.put("data", detailResult);
                                    responseMap.put("code", responseCode);
                                    responseMap.put("sourceData", sourceData);
                                } else {
                                    responseMap.put("code", 500);
                                    responseMap.put("data", "未知接口名称...");
                                }
                            } else {
                                responseMap.put("code", responseCode);
                                responseMap.put("data", errorMsg);
                            }
                        } catch (Exception e) {
                            Log.e("EDXposed", e.getLocalizedMessage());
                        }
                    }
                } else if ("dianpingInfo".equals(parameters.get("type"))) {
                    Log.e("EDXposed", InitXposedHook.dianpingResult);
                    if (InitXposedHook.dianpingResult != null) {
                        responseMap.put("data", InitXposedHook.dianpingResult);
                        responseMap.put("code", 200);
                        InitXposedHook.dianpingResult = null;
                    } else {
                        responseMap.put("data", "未获取到数据...");
                        responseMap.put("code", 500);
                    }
                } else if ("decodeShopInfo".equals(parameters.get("type"))) {
                    Class<?> clazz = XposedHelpers.findClass("com.dianping.archive.DPObject", InitXposedHook.dianpingClassLoader);
                    Constructor<?> constructor = XposedHelpers.findConstructorExact(clazz, byte[].class, int.class, int.class);
                    constructor.setAccessible(true);
                    Log.e("EDXposed", data);
                    byte[] resBytes = Base64.decode(data);
                    resBytes = decode(resBytes);
                    //获取店铺详情
                    Object instance = constructor.newInstance(resBytes, 0, resBytes.length);
                    Class<?> Shop = XposedHelpers.findClass("com.dianping.model.Shop", InitXposedHook.dianpingClassLoader);
                    Object ey = XposedHelpers.getStaticObjectField(Shop, "ey");
                    Object object = XposedHelpers.callMethod(instance, "a", ey);
                    String detailResult = (String) XposedHelpers.callMethod(object, "toJson");
                    responseMap.put("data", detailResult);
                    responseMap.put("code", 200);
                } else if ("jingdong".equals(parameters.get("type"))) {
                    String body = parameters.get("body");
                    String uuid = parameters.get("uuid");
                    String funcID = parameters.get("funcid");
                    String res = BitmapkitUtils.getSignFromJni(MainActivity.context, funcID, body, uuid, "android", "9.2.0");
                    responseMap.put("status", 200);
                    responseMap.put("msg", res);
                } else if ("douyin".equals(parameters.get("type"))) {
                    //get方式
                    int p0 = 50331649;
                    int p1 = 0;
                    long p2 = InitXposedHook.param2;
                    String p3 = "https://api5-core-c-hl.amemv.com/aweme/v1/user/profile/other/?sec_user_id=MS4wLjABAAAAqszdrdTPFmmpQXXvMMxrx2Dmj-4sdMQYOIcduwNAfjM&address_book_access=2&from=0&publish_video_strategy_type=2&user_avatar_shrink=188_188&user_cover_shrink=750_422&manifest_version_code=140001&_rticket=1620736738479&app_type=normal&iid=1742080172372237&channel=bf_1007913_1128_44_10&device_type=M2010J19SC&language=zh&cpu_support64=true&host_abi=armeabi-v7a&resolution=1080*2132&openudid=dd167e43c1b719d1&update_version_code=14009900&cdid=62fbca1a-474f-41ee-a351-3870a00bef85&appTheme=dark&os_api=29&dpi=440&oaid=74d3eea28e169f23&ac=wifi&device_id=844869058955485&os_version=10&version_code=140000&app_name=aweme&version_name=14.0.0&device_brand=Redmi&ssmix=a&device_platform=android&aid=1128&ts=1620736737";
                    String[] p4 = {"x-tt-trace-id", "00-5b70062f0d30067682408dd512ad0468-5b70062f0d300676-01", "cookie", "odin_tt=04235c43165f01526b54b5c61e28bde983c97c7fb2d8d0aca5e8224c8610851dbb1bf6894459207fb547d5dca2759dcbfa679ecdea7d704f564bf4a0661b65fa", "sdk-version", "2", "tc_2021_now_client", "0", "x-ss-dp", "1128", "accept-encoding", "gzip, deflate, br", "passport-sdk-version", "18", "x-ss-req-ticket", "1620736738480", "user-agent", "com.ss.android.ugc.aweme/140001 (Linux; U; Android 10; zh_CN; M2010J19SC; Build/QKQ1.200830.002; Cronet/TTNetVersion:c82dee74 2020-11-24 QuicVersion:47946d2a 2020-10-14)"};
                    Class<?> hClass = XposedHelpers.findClass("ms.bd.c.h", InitXposedHook.classLoader);
                    try {
                        java.lang.reflect.Method m = hClass.getDeclaredMethod("a", int.class, int.class, long.class, String.class, Object.class);
                        Object s = m.invoke(null, p0, p1, p2, p3, p4);
                        responseMap.put("code", 200);
                        responseMap.put("msg", JSONObject.toJSONString(s));
                    } catch (Exception e) {
                        e.printStackTrace();
                        responseMap.put("msg", e.getMessage());
                        responseMap.put("code", "500");
                    }
                } else if ("xinyang".equals(parameters.get("type"))) {
                    HashMap<String, String> t;
                    t = JSONObject.parseObject(data, HashMap.class);
                    Log.e("EDXposed", JSONObject.toJSONString(t));
                    AppApiCommonParameter appApiCommonParameter = new AppApiCommonParameter();
                    TreeMap<String, String> treeMap = appApiCommonParameter.getCommonParasm(t, "com.youxiang.soyoungapp");
                    responseMap.put("msg", JSONObject.toJSONString(treeMap));
                    responseMap.put("code", 200);
                } else if ("youxuan".equals(parameters.get("type"))) {
                    JSONObject jsonObject = JSONObject.parseObject(data);
                    String event = jsonObject.getString("event");
                    Map<String, String> youxuanParamMap = InitXposedHook.youxuanParamMap;
                    Object youxuanObject = InitXposedHook.youxuanObject;
                    //恢复拦截器数据 start
                    List<Object> interceptor = new ArrayList<>();
                    for (int i = 0; i < InitXposedHook.interceptorList.size(); i++) {
                        interceptor.add(InitXposedHook.interceptorList.get(i));
                    }
                    Collections.copy(interceptor, InitXposedHook.interceptorList);
                    Field interField = youxuanObject.getClass().getDeclaredField("clientInterceptors");
                    interField.setAccessible(true);
                    interField.set(youxuanObject, interceptor);
                    //恢复拦截器数据 end
                    switch (event) {
                        case "categoryList":
                            ApiEvent.get().getCategory(youxuanParamMap, responseMap, youxuanObject, jsonObject);
                            break;
                        case "homeCategoryList":
                            ApiEvent.get().getHomeCategory(youxuanParamMap, responseMap, youxuanObject, jsonObject);
                            break;
                        case "itemList":
                            ApiEvent.get().getItemList(youxuanParamMap, responseMap, youxuanObject, jsonObject);
                            break;
                        case "homeItemList":
                            ApiEvent.get().getHomeItemList(youxuanParamMap, responseMap, youxuanObject, jsonObject);
                            break;
                        case "cityList":
                            ApiEvent.get().getCityList(youxuanParamMap, responseMap, youxuanObject, jsonObject);
                            break;
                        case "searchPoi":
                            ApiEvent.get().searchPointList(youxuanParamMap, responseMap, youxuanObject, jsonObject);
                            break;
                        default:
                            responseMap.put("status", 500);
                            responseMap.put("msg", "未知的请求方法.");
                            break;
                    }
                } else if ("kuaikan".equals(parameters.get("type"))) {
                    JSONObject jsonObject = JSONObject.parseObject(data);
                    long cTime = jsonObject.getLongValue("cTime");
                    long sTime = jsonObject.getLongValue("sTime");
                    String topipc_id = jsonObject.getString("tid");
                    Object kkobject = InitXposedHook.kkObject;
                    java.lang.reflect.Method m = kkobject.getClass().getDeclaredMethod("a", long.class, long.class, SortedMap.class);
                    m.setAccessible(true);
                    SortedMap<String, String> sortedMap = new TreeMap<String, String>();
                    sortedMap.put("converter", "false");
                    sortedMap.put("gender", "1");
                    sortedMap.put("successive", "1");
                    sortedMap.put("topic_id", topipc_id);
                    String res = m.invoke(kkobject, sTime, cTime, sortedMap).toString();
                    responseMap.put("msg", res);
                    responseMap.put("code", "200");
                } else if ("meituan".equals(parameters.get("type"))) {
                    MyThreadLocal.getInstance().set("food");
                    if ("food".equals(parameters.get("method"))) {
                        JSONObject jsonObject = JSONObject.parseObject(data);
                        long poiId = jsonObject.getLong("poiId");
                        try{
                            java.lang.reflect.Method m = Meituan.shopApiServiceImpl.getClass().getDeclaredMethod("getShopMenu", String.class,
                                    long.class,
                                    Long.class,
                                    int.class,
                                    String.class,
                                    String.class,
                                    int.class,
                                    String.class,
                                    int.class,
                                    String.class,
                                    String.class,
                                    String.class,
                                    String.class,
                                    String.class,
                                    String.class,
                                    String.class);
                            Object res = m.invoke(Meituan.shopApiServiceImpl, "", poiId, null, 0, "", "", 0, "", 0, "[\"7.36_decision_info\",\"746\"]", "", "", "", "0", "0", "{\"seckill\":0}");
                            //获取参数2 回调函数, 返回的数据会在 rx_jObject 下的onNext的回调函数中传入
                            Object rx_jObject = InitXposedHook.rx_jObject;
                            Field d = res.getClass().getDeclaredField("a");
                            d.setAccessible(true);
                            Object a = d.get(res);
                            java.lang.reflect.Method m1 = a.getClass().getDeclaredMethod("call", XposedHelpers.findClass("rx.j", InitXposedHook.meituanClassLoader));
                            m1.setAccessible(true);
                            m1.invoke(a, rx_jObject);
                            String str = MyThreadLocal.getInstance().get();
                            responseMap.put("status", 200);
                            responseMap.put("msg", str);
                        }catch (Exception e){
                            responseMap.put("status", 500);
                            responseMap.put("msg", e.getLocalizedMessage());
                        }
                    } else if ("detail".equals(parameters.get("method"))) {
                        JSONObject jsonObject = JSONObject.parseObject(data);
                        long poiId = jsonObject.getLong("poiId");
                        String searchWord = jsonObject.getString("searchWord");
                        java.lang.reflect.Method m = Meituan.poiDetailServiceImpl.getClass().getDeclaredMethod("getBasePoiDetail", long.class, Map.class);
                        Map<String, Object> map = new HashMap<>();
                        map.put("methods", "baseV3,deallistV2,dynamicBusinessV2,rotateBroadcastV2,payInfoV1");
                        map.put("searchWord", searchWord);
                        map.put("lng", "116.46079329021309");
                        map.put("lat", "39.91289388996128");
                        Object res = m.invoke(Meituan.poiDetailServiceImpl, poiId, map);
                        java.lang.reflect.Method ex = res.getClass().getDeclaredMethod("execute");
                        ex.setAccessible(true);
                        ex.invoke(res);
                        String str = MyThreadLocal.getInstance().get();
                        responseMap.put("status", 200);
                        responseMap.put("msg", str);
                    } else if ("searchGlobal".equals(parameters.get("method"))) {
                        /**
                         * @Field("entrance_id") long j 0
                         * @Field("category_type") int i 0
                         * @Field("sub_category_type") int i2 0
                         * @Field("keyword") String str 麦当劳
                         * @Field("query_type") int i3 0
                         * @Field("page_index") int i4 0
                         * @Field("page_size") int i5 10
                         * @Field("activity_filter_codes") String str2 ""
                         * @Field("slider_select_data") String str3 ""
                         * @Field("sort_type") int i6 0
                         * @Field("is_fix_keyword") boolean z false
                         * @Field("search_global_id") String str4 577607715112881910
                         * @Field("filter_mapping") String str5 null
                         * @Field("show_mode") Integer num null
                         * @Field("search_source") int i7 0
                         * @Field("product_card_page_index") int i8 0
                         * @Field("product_tag_id") String str6 ""
                         * @Field("search_cursor") long j2 0
                         * @Field("search_page_type") int i9 0
                         * @Field("search_longitude") long j3 0
                         * @Field("search_latitude") long j4 0
                         * @Field("word_source") String str7 ""
                         * @Field("gaoda_id") int i10 0
                         * @Field("weien_id") int i11); 0
                         */

                        JSONObject jsonObject = JSONObject.parseObject(data);
                        String keyWord = jsonObject.getString("keyWord");
                        long lat = jsonObject.getLong("lat");
                        long lon = jsonObject.getLong("lon");
                        InitXposedHook.lon = jsonObject.getDouble("lon");
                        InitXposedHook.lat = jsonObject.getDouble("lat");
                        java.lang.reflect.Method m = Meituan.waimaiSearchServiceImpl.getClass().getDeclaredMethod("searchGlobalPage",
                                long.class,
                                int.class,
                                int.class,
                                String.class,
                                int.class,
                                int.class,
                                int.class,
                                String.class,
                                String.class,
                                int.class,
                                boolean.class,
                                String.class,
                                String.class,
                                Integer.class,
                                int.class,
                                int.class,
                                String.class,
                                long.class,
                                int.class,
                                long.class,
                                long.class,
                                String.class,
                                int.class,
                                int.class);
                        Object res = m.invoke(Meituan.waimaiSearchServiceImpl,
                                0, 0, 0, keyWord, 0, 0, 10, "", "", 0, false, "577607715112881910", null, null, 0, 0, "", 0, 0, lon, lat, "", 0, 0);
                        //获取参数2 回调函数, 返回的数据会在 rx_jObject 下的onNext的回调函数中传入
                        Object rx_jObject = InitXposedHook.rx_jObject;
                        Field d = res.getClass().getDeclaredField("a");
                        d.setAccessible(true);
                        Object a = d.get(res);
                        java.lang.reflect.Method m1 = a.getClass().getDeclaredMethod("call", XposedHelpers.findClass("rx.j", InitXposedHook.meituanClassLoader));
                        m1.setAccessible(true);
                        m1.invoke(a, rx_jObject);
                        String str = MyThreadLocal.getInstance().get();
                        responseMap.put("status", 200);
                        responseMap.put("msg", str);
                    } else if ("poiComment".equals(parameters.get("method"))) {
                        JSONObject jsonObject = JSONObject.parseObject(data);
                        long poiId = jsonObject.getLong("poiId");
                        java.lang.reflect.Method m = Meituan.shopApiServiceImpl.getClass().getDeclaredMethod("getComments", long.class,
                                int.class,
                                int.class,
                                int.class,
                                long.class);
                        Object res = m.invoke(Meituan.shopApiServiceImpl, poiId, 0, 20, 0, 0);
                        //获取参数2 回调函数, 返回的数据会在 rx_jObject 下的onNext的回调函数中传入
                        Object rx_jObject = InitXposedHook.rx_jObject;
                        Field d = res.getClass().getDeclaredField("a");
                        d.setAccessible(true);
                        Object a = d.get(res);
                        java.lang.reflect.Method m1 = a.getClass().getDeclaredMethod("call", XposedHelpers.findClass("rx.j", InitXposedHook.meituanClassLoader));
                        m1.setAccessible(true);
                        m1.invoke(a, rx_jObject);
                        String str = MyThreadLocal.getInstance().get();
                        responseMap.put("status", 200);
                        responseMap.put("msg", str);
                    } else if ("shopMenu".equals(parameters.get("method"))) {
                        JSONObject jsonObject = JSONObject.parseObject(data);
                        long poiId = jsonObject.getLong("poiId");
                        /**
                         * @Field("wm_poi_id") long j      参数1:1053887408869739
                         * @Field("product_spu_id") Long l      参数2:null
                         * @Field("page_index") int i      参数3:0
                         * @Field("tag_id") Long l2      参数4:-1
                         * @Field("extra") String str      参数5:{"source_id":"2"}
                         */
                        try {
                            java.lang.reflect.Method m = Meituan.scApiServiceImpl.getClass().getDeclaredMethod("getShopMenu",
                                    long.class,
                                    Long.class,
                                    int.class,
                                    Long.class,
                                    String.class);
                            Object res = m.invoke(Meituan.scApiServiceImpl,
                                    Long.valueOf(poiId), null, 0, -1L,
                                    "{\"source_id\":\"1\"}");
                            //获取参数2 回调函数, 返回的数据会在 rx_jObject 下的onNext的回调函数中传入
                            Object rx_jObject = InitXposedHook.rx_jObject;
                            Field d = res.getClass().getDeclaredField("a");
                            d.setAccessible(true);
                            Object a = d.get(res);
                            java.lang.reflect.Method m1 = a.getClass().getDeclaredMethod("call", XposedHelpers.findClass("rx.j", InitXposedHook.meituanClassLoader));
                            m1.setAccessible(true);
                            m1.invoke(a, rx_jObject);
                            Log.e("EDXposed", "反射调用函数返回值为:" + rx_jObject.getClass().getName());
                            Log.e("EDXposed", "反射调用函数返回值为:" + rx_jObject.getClass().getMethods().length);
                            String str = MyThreadLocal.getInstance().get();
                            Log.e("EDXposed", "hook返回结果:   " + str);
                            responseMap.put("code", 200);
                            responseMap.put("msg", str);
                        } catch (Exception e) {
                            Log.e("EDXposed", e.getLocalizedMessage());
                            e.printStackTrace();
                        }
                    } else if ("getProducts".equals(parameters.get("method"))) {
                        String poiId = parameters.get("poiId");
                        String tagCode = parameters.get("tagCode");
                        String pageNum = parameters.get("pageNum");
                        String tagType = parameters.get("tagType");
                        /**
                         * @Field("page_index") int i                 参数1:0
                         * @Field("spu_tag_id") String str            参数2:act_17_17
                         * @Field("wm_poi_id") String str2            参数3:1053887408935275
                         * @Field("tag_type") int i2                  参数4:2
                         * @Field("sort_type") int i3                 参数5:1
                         * @Field("is_support_smooth_render") int i4  参数6:1
                         * @Field("product_spu_id") long j            参数7:-1
                         * @Field("brand_ids") String str3            参数8:null
                         * @Field("extra") String str4                参数9:
                         * @Field("smooth_render_type") String str5   参数10:0
                         */
                        try {

                            java.lang.reflect.Method m = Meituan.scApiServiceImpl.getClass().getDeclaredMethod("getProducts",
                                    int.class,
                                    String.class,
                                    String.class,
                                    int.class,
                                    int.class,
                                    int.class,
                                    long.class,
                                    String.class,
                                    String.class,
                                    String.class);
                            Object res = m.invoke(Meituan.scApiServiceImpl,
                                    Integer.valueOf(pageNum) - 1, tagCode, poiId, Integer.valueOf(tagType),
                                    1, 1, -1L, null, "", "0");
                            //获取参数2 回调函数, 返回的数据会在 rx_jObject 下的onNext的回调函数中传入
                            Object rx_jObject = InitXposedHook.rx_jObject;
                            Field d = res.getClass().getDeclaredField("a");
                            d.setAccessible(true);
                            Object a = d.get(res);
                            java.lang.reflect.Method m1 = a.getClass().getDeclaredMethod("call", XposedHelpers.findClass("rx.j", InitXposedHook.meituanClassLoader));
                            m1.setAccessible(true);
                            m1.invoke(a, rx_jObject);
                            Log.e("EDXposed", "反射调用函数返回值为:" + rx_jObject.getClass().getName());
                            Log.e("EDXposed", "反射调用函数返回值为:" + rx_jObject.getClass().getMethods().length);
                            String str = MyThreadLocal.getInstance().get();
                            return newFixedLengthResponse(Response.Status.OK, "applicaltion/json; charset=UTF-8", str);
                        } catch (Exception e) {
                            Log.e("EDXposed", e.getLocalizedMessage());
                            e.printStackTrace();
                        }
                    } else if ("getProductsByIds".equals(parameters.get("method"))) {
                        String poiId = parameters.get("poiId");
                        String tagCode = parameters.get("tagCode");
                        String tagType = parameters.get("tagType");
                        String spuIds = parameters.get("spuIds");
                        String traceId = parameters.get("traceId");
                        /**
                         * @Field("wm_poi_id") String strd             参数1: poiId
                         * @Field("spu_tag_id") String str2d           参数2: spu_tag_id
                         * @Field("tag_type") int id                   参数3: tag_type
                         * @Field("spuIds") String str3                参数4: spuIds
                         * @Field("trace_id") String str4              参数5: trace_id
                         */
                        try {
                            java.lang.reflect.Method m = Meituan.scApiServiceImpl.getClass().getDeclaredMethod("getProductsByIds",
                                    String.class,
                                    String.class,
                                    int.class,
                                    String.class,
                                    String.class);
                            Object res = m.invoke(Meituan.scApiServiceImpl, poiId, tagCode, Integer.valueOf(tagType), spuIds, traceId);
                            //获取参数2 回调函数, 返回的数据会在 rx_jObject 下的onNext的回调函数中传入
                            Object rx_jObject = InitXposedHook.rx_jObject;
                            Field d = res.getClass().getDeclaredField("a");
                            d.setAccessible(true);
                            Object a = d.get(res);
                            java.lang.reflect.Method m1 = a.getClass().getDeclaredMethod("call", XposedHelpers.findClass("rx.j", InitXposedHook.meituanClassLoader));
                            m1.setAccessible(true);
                            m1.invoke(a, rx_jObject);
                            Log.e("EDXposed", "反射调用函数返回值为:" + rx_jObject.getClass().getName());
                            Log.e("EDXposed", "反射调用函数返回值为:" + rx_jObject.getClass().getMethods().length);
                            String str = MyThreadLocal.getInstance().get();
                            return newFixedLengthResponse(Response.Status.OK, "applicaltion/json; charset=UTF-8", str);
                        } catch (Exception e) {
                            Log.e("EDXposed", e.getLocalizedMessage());
                            e.printStackTrace();
                        }
                    } else {
                        responseMap.put("code", 500);
                        responseMap.put("msg", "{美团}未知的请求方法.");
                    }
                } else if ("saveContact".equals(parameters.get("type"))) {
                    JSONArray jsonArray = JSONObject.parseObject(data).getJSONArray("phone");
                    String type = JSONObject.parseObject(data).getString("type");
                    if (type.equals("append")) {
                        //新增追加联系人不移除之前的联系人
                    } else if (type.equals("add")) {
                        //移除之前的联系人
                        MainActivity.moveContact();
                    }
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JSONObject j = jsonArray.getJSONObject(i);
                        String name = j.getString("name");
                        String num = j.getString("num");
                        MainActivity.addContact(name, num);
                    }
                    responseMap.put("code", 200);
                    responseMap.put("msg", "操作成功.");
                } else if ("duoduoContact".equals(parameters.get("type"))) {
                    String result = InitXposedHook.pinduoduoResult;
                    int i = 0;
                    while (result.equals("")) {
                        i += 1;
                        result = InitXposedHook.pinduoduoResult;
                        Thread.sleep(500);
                        if (i >= 10) {
                            break;
                        }
                    }
                    InitXposedHook.pinduoduoResult = "";
                    responseMap.put("code", 200);
                    responseMap.put("msg", result);
                } else {
                    responseMap.put("code", 500);
                    responseMap.put("msg", "未知的请求方法.");
                }
            } catch (Exception e) {
                responseMap.put("code", 500);
                responseMap.put("msg", e.getMessage());
                e.printStackTrace();
            }
        }
        return newFixedLengthResponse(Response.Status.OK, "applicaltion/json; charset=UTF-8", JSONObject.toJSONString(responseMap));

    }


    //    大众点评解密算法
    public static byte[] decode(byte[] bArr) throws Exception {
        byte[] bArr2 = null;

        if (bArr.length % 16 != 0) {
            System.out.println("长度不正确");
            return bArr;
        }
        Cipher instance = Cipher.getInstance(model);
        instance.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, model.substring(0, 3)), new IvParameterSpec(iv));
        GZIPInputStream gZIPInputStream = new GZIPInputStream(new ByteArrayInputStream(instance.doFinal(bArr)));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        byte[] bArr3 = new byte[4096];
        while (true) {
            int read = gZIPInputStream.read(bArr3);
            if (read > 0) {
                byteArrayOutputStream.write(bArr3, 0, read);
            } else {
                gZIPInputStream.close();
                byteArrayOutputStream.close();
                return byteArrayOutputStream.toByteArray();
            }
        }
    }
}
