package com.o2o.hook.utils;

public class Telephony implements VAHook {

    @Override
    public void inject(ClassLoader classLoader) {

        /*
            // 手机卡国家
    public String simCountryIso = "cn";
    // 国家iso代码
    public String networkCountryIso = "cn";
    // 运营商名字
    public String simOperatorName = "中国联通";
    // ====================================================^
    // 运营商
    public String simOperator = "46001";
    // 返回系统版本
    public String deviceSoftwareVersion = "100";
    // IMSI
    public String subscriberId = "460017932859596";
    // 手机号码
    public String line1Number = "13117511178";
    // 手机卡序列号
    public String simSerialNumber = "89860179328595969501";
    // 网络运营商类型
    public String networkOperator = "46001";
    // 网络运营商名字
    public String networkOperatorName = "89860179328595969501";
    // 网络类型
    public int networkType = 6;
    public int phoneType = 5;
    public int simState = 10;
    public String networkInfoType = "";





        HookHelpers.hook(NetworkInfo.class, "getType", FakeInfo.getInstance().networkInfoType);

        HookHelpers.hook("android.bluetooth.BluetoothAdapter", classLoader, "getAddress", FakeInfo.getInstance().fingerprint.bluetoothMac);
        HookHelpers.hook("android.bluetooth.BluetoothDevice", classLoader, "getAddress", FakeInfo.getInstance().fingerprint.bluetoothMac);

        if(Build.VERSION.SDK_INT < 22){
            HookHelpers.hook("com.android.internal.telephony.gsm.GSMPhone", classLoader, "getDeviceId", FakeInfo.getInstance().fingerprint.IMEI);
            HookHelpers.hook("com.android.internal.telephony.PhoneProxy", classLoader, "getDeviceId", FakeInfo.getInstance().fingerprint.IMEI);
        }
        else if(Build.VERSION.SDK_INT == 22){
            HookHelpers.hook("android.telephony.TelephonyManager", classLoader, "getDeviceId", FakeInfo.getInstance().fingerprint.IMEI);
            HookHelpers.hook("com.android.internal.telephony.PhoneSubInfo", classLoader, "getDeviceId", FakeInfo.getInstance().fingerprint.IMEI);
        }
        else if(Build.VERSION.SDK_INT == 23){
            HookHelpers.hook("android.telephony.TelephonyManager", classLoader, "getDeviceId", FakeInfo.getInstance().fingerprint.IMEI);
            HookHelpers.hook("com.android.internal.telephony.PhoneSubInfo", classLoader, "getDeviceId", String.class, FakeInfo.getInstance().fingerprint.IMEI);
        }
        else if(Build.VERSION.SDK_INT >= 24){
            HookHelpers.hook("android.telephony.TelephonyManager", classLoader, "getDeviceId", FakeInfo.getInstance().fingerprint.IMEI);
            HookHelpers.hook("com.android.internal.telephony.PhoneSubInfoController", classLoader, "getDeviceIdForPhone", int.class, String.class, FakeInfo.getInstance().fingerprint.IMEI);
            HookHelpers.hook("com.android.internal.telephony.PhoneSubInfoController", classLoader, "getImeiForSubscriber", int.class, String.class, FakeInfo.getInstance().fingerprint.IMEI);
        }

        HookHelpers.hook(TelephonyManager.class, "getDeviceSoftwareVersion", FakeInfo.getInstance().deviceSoftwareVersion);
        HookHelpers.hook(TelephonyManager.class, "getSubscriberId", FakeInfo.getInstance().subscriberId);
        HookHelpers.hook(TelephonyManager.class, "getLine1Number", FakeInfo.getInstance().line1Number);
        HookHelpers.hook(TelephonyManager.class, "getSimSerialNumber", FakeInfo.getInstance().simSerialNumber);
        HookHelpers.hook(TelephonyManager.class, "getNetworkOperator", FakeInfo.getInstance().networkOperator);
        HookHelpers.hook(TelephonyManager.class, "getNetworkOperatorName", FakeInfo.getInstance().networkOperatorName);
        HookHelpers.hook(TelephonyManager.class, "getSimOperator", FakeInfo.getInstance().simOperator);
        HookHelpers.hook(TelephonyManager.class, "getSimOperatorName", FakeInfo.getInstance().simOperatorName);
        HookHelpers.hook(TelephonyManager.class, "getNetworkCountryIso", FakeInfo.getInstance().networkCountryIso);
        HookHelpers.hook(TelephonyManager.class, "getSimCountryIso", FakeInfo.getInstance().simCountryIso);
        HookHelpers.hook(TelephonyManager.class, "getNetworkType", FakeInfo.getInstance().networkType);
        HookHelpers.hook(TelephonyManager.class, "getPhoneType", FakeInfo.getInstance().phoneType);
        HookHelpers.hook(TelephonyManager.class, "getSimState", FakeInfo.getInstance().simState);
         */
    }
}
