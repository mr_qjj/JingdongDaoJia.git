package com.o2o.hook.utils;

import android.util.Log;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;

public class PropertyHook implements VAHook {
    private static final String TAG = "X-LOG/PropertyHook";

    @Override
    public void inject(ClassLoader classLoader) {
        XposedHelpers.findAndHookMethod("android.os.SystemProperties", classLoader, "get", String.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                String name = (String) param.args[0];
                String result = (String) param.getResult();
                String replace = FakeInfo.getInstance().queryProperty(name);
                if (replace == null || replace.isEmpty()) {
                    return;
                }
                Log.d(TAG, "SystemProperties.get(\"" + name + "\")" + " replace: " + result + " -> " + replace);
                param.setResult(replace);
            }
        });

        XposedHelpers.findAndHookMethod("android.os.SystemProperties", classLoader, "get", String.class, String.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                String name = (String) param.args[0];
                String result = (String) param.getResult();
                String replace = FakeInfo.getInstance().queryProperty(name);
                if (replace == null || replace.isEmpty()) {
                    return;
                }
                Log.d(TAG, "SystemProperties.get(\"" + name + "\")" + " replace: " + result + " -> " + replace);
                param.setResult(replace);
            }
        });
    }
}
