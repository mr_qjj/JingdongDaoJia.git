package com.o2o.hook.utils;

public class Fingerprint {
    // im
    public String IMEI;
    public String IMSI;
    // ANDROID ID
    public String androidId;
    // WIFI MAC
    public String wifiMac;
    // 路由器 MAC
    public String routerMac;
    // 蓝牙 MAC
    public String bluetoothMac;
    // 序列号
    public String serial;
    //
    public String iccId;
    // wifi 名字
    public String wifiName;
    // ua
    public String userAgent;
    // 本地
    public String localIP;
}
