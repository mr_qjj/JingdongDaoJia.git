package com.o2o.hook.utils;

import com.google.gson.annotations.SerializedName;

public class SensorData {
    @SerializedName("name")
    public String mName;
    @SerializedName("vendor")
    public String mVendor;
    @SerializedName("version")
    public int     mVersion;
    @SerializedName("type")
    public int     mType;
    @SerializedName("maxRange")
    public float   mMaxRange;
    @SerializedName("resolution")
    public float   mResolution;
    @SerializedName("power")
    public float   mPower;
    @SerializedName("minDelay")
    public int     mMinDelay;
    @SerializedName("stringType")
    public String mStringType;
    @SerializedName("maxDelay")
    public int     mMaxDelay;
}
