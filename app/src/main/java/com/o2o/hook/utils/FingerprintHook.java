package com.o2o.hook.utils;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;


import java.util.Iterator;
import java.util.List;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;

public class FingerprintHook implements VAHook {
    private static final String TAG = "EDXposed";
    private static final String DEFAULT_MAC_ADDRESS = "02:00:00:00:00:00";

    private void hookIMEI(ClassLoader classLoader) {
        XC_MethodHook setIMEICallback = new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                if (param.getResult() == null) {
                    return;
                }
                param.setResult(FakeInfo.getInstance().getFingerprint().IMEI);
            }
        };
        try {
            if (Build.VERSION.SDK_INT < 22) {
                XposedHelpers.findAndHookMethod("com.android.internal.telephony.gsm.GSMPhone", classLoader, "getDeviceId", setIMEICallback);
                XposedHelpers.findAndHookMethod("com.android.internal.telephony.PhoneSubInfo", classLoader, "getDeviceId", setIMEICallback);
                XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getDeviceId", setIMEICallback);
                XposedHelpers.findAndHookMethod("com.android.internal.telephony.PhoneProxy", classLoader, "getDeviceId", setIMEICallback);
            } else if (Build.VERSION.SDK_INT == 22) {
                XposedHelpers.findAndHookMethod("com.android.internal.telephony.PhoneSubInfo", classLoader, "getDeviceId", setIMEICallback);
                XposedHelpers.findAndHookMethod("com.android.internal.telephony.PhoneProxy", classLoader, "getDeviceId", setIMEICallback);
                XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getDeviceId", setIMEICallback);
            } else if (Build.VERSION.SDK_INT >= 23) {
                XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getDeviceId", setIMEICallback);
                XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getImei", setIMEICallback);
                XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getDeviceId", int.class, setIMEICallback);
                XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getImei", int.class, setIMEICallback);
            }
        } catch (Exception e) {
            Log.e(TAG, "hookIMEI: ", e);
        }
    }


    private void hookUSBDebugMode(ClassLoader classLoader) {
        try {
            XposedHelpers.findAndHookMethod("android.provider.Settings.Global", classLoader, "getInt", ContentResolver.class, String.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {

                    if (param.args[1].equals("adb_enabled")) {
                        param.setResult(0);
                    }
                }
            });
            Log.d(TAG, "hide USBDebugging: hook Settings.Global.getInt method");
        } catch (Exception v3_1) {
            Log.d(TAG, "error hooking USB Debug Mode: Settings.Global.getInt");
        }

        try {
            XposedHelpers.findAndHookMethod("android.provider.Settings.Secure", classLoader, "getInt", ContentResolver.class, String.class, int.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) {
                    if (param.args[1].equals("adb_enabled")) {
                        param.setResult(0);
                    }
                }
            });
            Log.d(TAG, "hide USBDebugging: hook Settings.Secure.getInt method");
        } catch (Exception v1) {
            Log.d(TAG, "error hooking USB Debug Mode: Settings.Secure.getInt");
        }
    }

    private void hookWifi(ClassLoader classLoader) {
        // 禁止 VPN 检测
        XposedHelpers.findAndHookMethod("android.net.ConnectivityManager", classLoader, "getNetworkInfo", int.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                int networkType = (int) param.args[0];
                if (networkType == ConnectivityManager.TYPE_VPN) {
                    NetworkInfo networkInfo = (NetworkInfo) param.getResult();
                    if (networkInfo != null) {
                        Reflect.on(param.getResult()).set("mState", NetworkInfo.State.DISCONNECTED);
                    }
                }
            }
        });

        XposedHelpers.findAndHookMethod(
                "java.net.NetworkInterface",
                classLoader,
                "getName",
                new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        super.afterHookedMethod(param);
                        String name = (String) param.getResult();
                        if (name != null && (name.equals("tun0") || name.equals("ppp0"))) {
                            param.setResult("rm0");
                        }
                    }
                }
        );

        XC_MethodHook propertyCallback = new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                String key = (String) param.args[0];
                if (key == null) {
                    return;
                }
                // 禁止代理检测
                if (key.equals("http.proxyHost") || key.equals("http.proxyPort")) {
                    param.setResult(null);
                }
                // 修改UserAgent
                if (key.equals("http.agent")) {
                    param.setResult(FakeInfo.getInstance().getFingerprint().userAgent);
                }
            }
        };
        XposedHelpers.findAndHookMethod("java.lang.System", classLoader, "getProperty", String.class, propertyCallback);
        XposedHelpers.findAndHookMethod("java.lang.System", classLoader, "getProperty", String.class, String.class, propertyCallback);


        final XC_MethodHook setUserAgent = new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                param.setResult(FakeInfo.getInstance().getFingerprint().userAgent);
            }
        };

        XposedHelpers.findAndHookMethod(WebSettings.class, "getDefaultUserAgent", Context.class, setUserAgent);
        XposedBridge.hookAllMethods(WebView.class, "getSettings", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                XposedHelpers.findAndHookMethod(param.getResult().getClass(), "getUserAgentString", setUserAgent);
            }
        });
    }

    @Override
    public void inject(ClassLoader classLoader) {
        // 修改 DeviceId
        hookIMEI(classLoader);

        // 修改 SubscriberId
        XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getSubscriberId", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                if (param.getResult() != null) {
                    Log.d(TAG, "TelephonyManager.getSubscriberId" + " replace: " + param.getResult() + " -> " + FakeInfo.getInstance().getFingerprint().IMSI);
                    param.setResult(FakeInfo.getInstance().getFingerprint().IMEI);
                }
            }
        });

        // meid
        XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getMeid", int.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                if (param.getResult() != null) {
                    Log.d(TAG, "TelephonyManager.getMeid" + " replace: " + param.getResult() + " -> " + FakeInfo.getInstance().getFingerprint().IMEI);
                    param.setResult(FakeInfo.getInstance().getFingerprint().IMEI);
                }
            }
        });

        // 修改 AndroidId
        XC_MethodHook fakeGetString = new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) {
                if (param.args[1].equals(Settings.Secure.ANDROID_ID)) {
                    Log.d(TAG, String.format("getAndroidId replace %s -> %s", param.getResult(), FakeInfo.getInstance().getFingerprint().androidId));
                    param.setResult(FakeInfo.getInstance().getFingerprint().androidId);
                }
            }
        };
        XposedHelpers.findAndHookMethod("android.provider.Settings.Secure", classLoader, "getString", ContentResolver.class, String.class, fakeGetString);
        XposedHelpers.findAndHookMethod("android.provider.Settings.System", classLoader, "getString", ContentResolver.class, String.class, fakeGetString);
        XposedHelpers.findAndHookMethod("android.provider.Settings.Global", classLoader, "getString", ContentResolver.class, String.class, fakeGetString);

        // 修改 WIFI
        XposedHelpers.findAndHookMethod("android.net.wifi.WifiManager", classLoader, "getConnectionInfo", new XC_MethodHook() {
            @SuppressLint("HardwareIds")
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                WifiInfo wifiInfo = (WifiInfo) param.getResult();
                if (TextUtils.equals(wifiInfo.getSSID(), "<unknown ssid>")) {
                    return;
                }
                if (Reflect.on(wifiInfo).get("mWifiSsid") != null) {
                    Object wifiSsid = Reflect.on("android.net.wifi.WifiSsid").callBest("createFromAsciiEncoded", FakeInfo.getInstance().getFingerprint().wifiName).get();
                    Reflect.on(wifiInfo).set("mWifiSsid", wifiSsid);
                }
                Reflect.on(wifiInfo).call("setBSSID", FakeInfo.getInstance().getFingerprint().routerMac);
                if (!wifiInfo.getMacAddress().equals(DEFAULT_MAC_ADDRESS)) {
                    Log.d(TAG, "getConnectionInfo replace mMacAddress " + wifiInfo.getMacAddress() + " -> " + FakeInfo.getInstance().getFingerprint().wifiMac);
                    Reflect.on(wifiInfo).call("setMacAddress", FakeInfo.getInstance().getFingerprint().wifiMac);
                }
            }
        });

        XposedHelpers.findAndHookMethod("android.net.wifi.WifiInfo", classLoader, "getBSSID", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                Log.d(TAG, "WifiInfo.getBSSID orig:" + param.getResult() + " now:" + FakeInfo.getInstance().getFingerprint().routerMac);
                param.setResult(FakeInfo.getInstance().getFingerprint().routerMac);
            }
        });

        XposedHelpers.findAndHookMethod("android.net.wifi.WifiInfo", classLoader, "getSSID", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                String ssid = (String)param.getResult();
                if (ssid.equals(FakeInfo.getInstance().getFingerprint().wifiName)) {
                    return;
                }
                Log.d(TAG, "WifiInfo.getSSID orig:" + param.getResult() + " now:" + FakeInfo.getInstance().getFingerprint().wifiName);
                param.setResult(FakeInfo.getInstance().getFingerprint().wifiName);
            }
        });

        XposedHelpers.findAndHookMethod("android.net.wifi.WifiInfo", classLoader, "getMacAddress", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                WifiInfo wifiInfo = (WifiInfo) param.thisObject;
                String wifiName = wifiInfo.getSSID();
                String macAddress = (String) param.getResult();
                if (macAddress == null || macAddress.equals(DEFAULT_MAC_ADDRESS)) {
                    return;
                }
                Log.d(TAG, "WifiInfo.getMacAddress wifi name=" + wifiName + " orig:" + param.getResult() + " now:" + FakeInfo.getInstance().getFingerprint().wifiMac);
                param.setResult(FakeInfo.getInstance().getFingerprint().wifiMac);
            }
        });

        if (Build.VERSION.SDK_INT >= 24) {
            XposedHelpers.findAndHookMethod("android.app.admin.DevicePolicyManager", classLoader, "getWifiMacAddress", ComponentName.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    Log.d(TAG, "DevicePolicyManager.getWifiMacAddress orig:" + param.getResult() + " now:" + FakeInfo.getInstance().getFingerprint().wifiMac);
                    param.setResult(FakeInfo.getInstance().getFingerprint().wifiMac);
                }
            });
        }

        // 修改 WIFI MAC
        XposedHelpers.findAndHookMethod(
                "java.net.NetworkInterface",
                classLoader,
                "getHardwareAddress",
                new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        super.afterHookedMethod(param);
                        byte[] hardwareAddress = (byte[]) param.getResult();
                        if (hardwareAddress == null) {
                            return;
                        }
                        String name = Reflect.on(param.thisObject).call("getName").get();
                        if (name.equals("wlan0")) {
                            byte[] replace = HexUtil.hexStringToByteArray(FakeInfo.getInstance().getFingerprint().wifiMac.replace(":", ""));
                            Log.d(TAG, "getHardwareAddress replace " + HexUtil.byteArrayToHexString(hardwareAddress) + " -> " + HexUtil.byteArrayToHexString(replace));
                            param.setResult(replace);
                        } else {
                            Log.d(TAG, "name=" + name + " getHardwareAddress=" + HexUtil.byteArrayToHexString(hardwareAddress));
                        }
                    }
                }
        );

        XposedHelpers.findAndHookMethod("android.bluetooth.BluetoothAdapter", classLoader, "getAddress", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                param.setResult(FakeInfo.getInstance().getFingerprint().bluetoothMac);
            }
        });

//        XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getSimState", new XC_MethodHook() {
//            @Override
//            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//            }
//        });

//        XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getSimCountryIso", new XC_MethodHook() {
//            @Override
//            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//            }
//        });

//        XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getSimOperatorName", new XC_MethodHook() {
//            @Override
//            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//            }
//        });

//        XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getSimOperator", new XC_MethodHook() {
//            @Override
//            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//            }
//        });

        XposedHelpers.findAndHookMethod("java.net.Inet4Address", classLoader, "getHostAddress", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                param.setResult(FakeInfo.getInstance().getFingerprint().localIP);
            }
        });

        hookWifi(classLoader);
        hookPackage(classLoader);
    }


    private void hookPackage(ClassLoader classLoader) {
        try {
            Log.d(TAG, "start hook PackageManager");
            // android.content.pm.PackageManager.getInstalledApplications
            XposedHelpers.findAndHookMethod("android.app.ApplicationPackageManager", classLoader, "getInstalledApplications", int.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    Log.d(TAG, "getInstalledApplications");
                    List<ApplicationInfo> applicationInfoList = (List<ApplicationInfo>) param.getResult();
                    if (applicationInfoList != null) {
                        Iterator<ApplicationInfo> iterator = applicationInfoList.iterator();
                        while (iterator.hasNext()) {
                            ApplicationInfo applicationInfo = iterator.next();
                            String packageName = Reflect.on(applicationInfo).get("packageName");
                            if (packageName.startsWith("com.reacg") ||
                                    packageName.startsWith("com.example") ||
                                    packageName.startsWith("io.busniess.va") ||
                                    packageName.contains("xposed")) {
                                iterator.remove();
                                Log.d(TAG, "getInstalledApplications hide " + packageName);
                            }
                        }
                    }
                    param.setResult(applicationInfoList);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "hookPackage: ", e);
        }
    }
}
