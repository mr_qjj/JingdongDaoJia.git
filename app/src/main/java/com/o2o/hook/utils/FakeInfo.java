package com.o2o.hook.utils;

import android.annotation.SuppressLint;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class FakeInfo {
    private static final String TAG = "X-LOG/FakeInfo";
    volatile private static FakeInfo mInstance;
    private final Random random = new Random();
    public InfoData infoData = new InfoData();
    private Fingerprint fingerprint = new Fingerprint();

    private FakeInfo() {
        init();
    }

    public static FakeInfo getInstance() {
        if (mInstance == null) {
            synchronized (FakeInfo.class) {
                if (mInstance == null) {
                    mInstance = new FakeInfo();
                }
            }
        }
        return mInstance;
    }

    public Fingerprint getFingerprint() {
        return fingerprint;
    }

    private static String generateDeviceId() {
        return generate10(15);
    }

    private static String generate10(int length) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

    private static String generateHex(int length) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int nextInt = random.nextInt(16);
            if (nextInt < 10) {
                sb.append(nextInt);
            } else {
                sb.append((char) ((nextInt - 10) + 'a'));
            }
        }
        return sb.toString();
    }

    private static String generateMac() {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        int next = 1;
        int cur = 0;
        while (cur < 12) {
            int val = random.nextInt(16);
            if (val < 10) {
                sb.append(val);
            } else {
                sb.append((char) (val + 87));
            }
            if (cur == next && cur != 11) {
                sb.append(":");
                next += 2;
            }
            cur++;
        }
        return sb.toString();
    }

    @SuppressLint("HardwareIds")
    private static String generateSerial() {
        Log.d(TAG, "generateSerial original SERIAL: " + Build.SERIAL);
        String serial;
        if (Build.SERIAL == null || Build.SERIAL.length() <= 0 || Build.SERIAL.equals("unknown")) {
            serial = "0123456789ABCDEF";
        } else {
            serial = Build.SERIAL;
        }
        List<Character> list = new ArrayList<>();
        for (char c : serial.toCharArray()) {
            list.add(c);
        }
        Collections.shuffle(list);
        StringBuilder sb = new StringBuilder();
        for (Character c : list) {
            sb.append(c.charValue());
        }
        return sb.toString();
    }

    private String randomString(int length, boolean lowEnglish, boolean upperEnglish, boolean number) {
        String baseString = "";
        if (lowEnglish) baseString += "abcdefghijklmnopqrstuvwxyz";
        if (upperEnglish) baseString += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if (number) baseString += "0123456789";

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(baseString.charAt(random.nextInt(baseString.length())));
        }
        return sb.toString();
    }

    private int randomInt(int min, int max) {
        if (min == max) return min;
        return random.nextInt(max) + min;
    }

    private String randomWifiInfoSSID() {
        String[] strings = new String[]{"TP-", "FAST_", "Tenda_", "TP-LINK_", "MERCURY_"};
        return strings[randomInt(0, strings.length - 1)] + randomString(randomInt(5, 8), false, true, true);
    }

    private void init() {
        fingerprint.IMEI = generateDeviceId();
        fingerprint.IMSI = generateDeviceId();
        fingerprint.androidId = generateHex(16);
        fingerprint.routerMac = generateMac();
        fingerprint.wifiMac = generateMac();
        fingerprint.bluetoothMac = generateMac();
        fingerprint.serial = generateSerial();
        fingerprint.iccId = generate10(20);
        fingerprint.wifiName = randomWifiInfoSSID();
        fingerprint.localIP = "192.168.1." + randomInt(2, 255);
    }

    public void setDeviceInfo(InfoData info) {
        infoData = info;
        infoData.fingerprint = fingerprint;
        String VERSION_RELEASE = queryProperty("ro.build.version.release", Build.VERSION.RELEASE);
        String MODEL = queryProperty("ro.product.model", Build.MODEL);
        String ID = queryProperty("ro.build.id", Build.ID);
        fingerprint.userAgent = String.format("Dalvik/2.1.0 (Linux; U; Android %s; %s Build/%s)", VERSION_RELEASE, MODEL, ID);
    }

    public void readInfo(String packageName) {
        try {
            @SuppressLint("SdCardPath")
            String configDirPath = "/data/data/" + packageName + "/moe";
            @SuppressLint("SdCardPath")
            String configFilePath = "/data/data/" + packageName + "/moe/config.json";
            if (new File(configFilePath).exists()) {
                Log.d(TAG, "配置文件已存在，直接加载...");
                infoData = new Gson().fromJson(new FileReader(configFilePath), InfoData.class);
                Log.d(TAG, "设备指纹: " + new Gson().toJson(infoData.fingerprint));
            } else {
                Log.d(TAG, "配置文件不存在");
                boolean createDirResult = new File(configDirPath).mkdirs();
                if (!createDirResult) {
                    Log.e(TAG, "创建目录失败");
                    return;
                }
                Log.d(TAG, "创建配置文件");
                String json = new Gson().toJson(infoData);
                FileOutputStream fileOutputStream = new FileOutputStream(new File(configFilePath));
                fileOutputStream.write(json.getBytes());
                fileOutputStream.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "readInfo", e);
        }
    }

    @SuppressLint("HardwareIds")
    public void loadProperty() {
        try {
            Log.d(TAG, "loadProperty Set  android.os.Build");
            Reflect.on(Build.class).set("SERIAL", queryProperty("ro.serialno", getFingerprint().serial));
            Reflect.on(Build.class).set("TAGS", queryProperty("ro.build.tags", Build.TAGS));
            Reflect.on(Build.class).set("USER", queryProperty("ro.build.user", Build.USER));
            Reflect.on(Build.class).set("TYPE", queryProperty("ro.build.type", Build.TYPE));
            Reflect.on(Build.class).set("ID", queryProperty("ro.build.id", Build.ID));
            Reflect.on(Build.class).set("HOST", queryProperty("ro.build.host", Build.HOST));
            Reflect.on(Build.class).set("DISPLAY", queryProperty("ro.build.display.id", Build.DISPLAY));
            Reflect.on(Build.class).set("FINGERPRINT", queryProperty("ro.build.fingerprint", Build.FINGERPRINT));
            Reflect.on(Build.class).set("PRODUCT", queryProperty("ro.product.name", Build.PRODUCT));
            Reflect.on(Build.class).set("DEVICE", queryProperty("ro.product.device", Build.DEVICE));
            Reflect.on(Build.class).set("BOARD", queryProperty("ro.product.board", Build.BOARD));
            Reflect.on(Build.class).set("MANUFACTURER", queryProperty("ro.product.manufacturer", Build.MANUFACTURER));
            Reflect.on(Build.class).set("BRAND", queryProperty("ro.product.brand", Build.BRAND));
            Reflect.on(Build.class).set("MODEL", queryProperty("ro.product.model", Build.MODEL));
            Reflect.on(Build.class).set("BOOTLOADER", queryProperty("ro.bootloader", Build.BOOTLOADER));
            Reflect.on(Build.class).set("HARDWARE", queryProperty("ro.hardware", Build.HARDWARE));

            long time = Long.parseLong(queryProperty("ro.build.date.utc", "0")) * 1000;
            if (time == 0) {
                time = Build.TIME;
            }
            Reflect.on(Build.class).set("TIME", time);

            Reflect.on(Build.VERSION.class).set("INCREMENTAL", queryProperty("ro.build.version.incremental", Build.VERSION.INCREMENTAL));
            Reflect.on(Build.VERSION.class).set("RELEASE", queryProperty("ro.build.version.release", Build.VERSION.RELEASE));
            Reflect.on(Build.VERSION.class).set("CODENAME", queryProperty("ro.build.version.codename", Build.VERSION.CODENAME));
            Reflect.on(Build.VERSION.class).set("SDK_INT", Integer.parseInt(queryProperty("ro.build.version.sdk", String.valueOf(Build.VERSION.SDK_INT))));
        } catch (Exception e) {
            Log.e(TAG, "loadProperty: ", e);
        }
    }

    public String queryProperty(String name) {
        return queryProperty(name, null);
    }

    public String queryProperty(String name, String defaultValue) {
        String value = defaultValue;
        if (infoData.properties.containsKey(name)) {
            value = infoData.properties.get(name);
        }
        if (name.startsWith("debug.sqlite") || name.startsWith("debug.force_rtl")) {
            return value;
        }
        Log.d(TAG, "queryProperty: " + name + "=" + value);
        return value;
    }
}
