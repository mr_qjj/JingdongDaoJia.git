package com.o2o.hook.utils;

import android.app.ActivityManager;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;

public class StorageHook implements VAHook {

    private static final String TAG = "X-LOG/StorageHook";

    boolean setAvailableBlocksLong(StatFs statFs, long value) {
        Object mStat = Reflect.on(statFs).get("mStat");
        Reflect.on(mStat).set("f_bavail", value);
        return statFs.getAvailableBlocksLong() == value;
    }

    boolean setBlockCountLong(StatFs statFs, long value) {
        Object mStat = Reflect.on(statFs).get("mStat");
        Reflect.on(mStat).set("f_blocks", value);
        return statFs.getBlockCountLong() == value;
    }

    boolean setBlockSizeLong(StatFs statFs, long value) {
        Object mStat = Reflect.on(statFs).get("mStat");
        Reflect.on(mStat).set("f_frsize", value);
        return statFs.getBlockSizeLong() == value;
    }

    @Override
    public void inject(ClassLoader classLoader) {

        XposedHelpers.findAndHookConstructor(StatFs.class, String.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);

                try {
                    if (param.getThrowable() != null) {
                        return;
                    }

                    String path = (String) param.args[0];
                    StatFs statFs = (StatFs) param.thisObject;

                    // 都需要修改 BlockSize
                    String sdPath = Environment.getExternalStorageDirectory().getPath();
                    String systemPath = Environment.getDataDirectory().getPath();
                    Log.d(TAG, "StatFs=" + path);

//                    // totalSystem
//                    if (!setBlockSizeLong(statFs, 0L)) {
//                        Log.d(TAG, "setBlockSizeLong failed!");
//                    }
//                    // 修改 totalSD
//                    if (!setBlockCountLong(statFs, 0L)) {
//                        Log.d(TAG, "setBlockCountLong failed!");
//                    }
//                    // 修改 availableSystem
//                    if (!setAvailableBlocksLong(statFs, 0L)) {
//                        Log.d(TAG, "setAvailableBlocksLong failed!");
//                    }
//                    if (path.equals(systemPath)) {
//                    } else if (path.equals(sdPath)) {
//                    }
                } catch (Exception e) {
                    Log.e(TAG, "Set Stat", e);
                }
            }
        });

        // android.app.ActivityManager.getMemoryInfo(android.app.ActivityManager$MemoryInfo) : void
        XposedHelpers.findAndHookMethod("android.app.ActivityManager", classLoader, "getMemoryInfo", "android.app.ActivityManager$MemoryInfo", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                ActivityManager.MemoryInfo memoryInfo = (ActivityManager.MemoryInfo) param.args[0];
                Log.d(TAG, "availableMemory=" + memoryInfo.availMem);
            }
        });
    }
}
