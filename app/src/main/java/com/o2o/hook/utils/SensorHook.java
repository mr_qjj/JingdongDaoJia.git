package com.o2o.hook.utils;

import android.hardware.Sensor;


import java.util.List;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;

public class SensorHook implements VAHook {
    private static final String TAG = "X-LOG/SensorHook";

    @Override
    public void inject(ClassLoader classLoader) {
        XposedHelpers.findAndHookMethod("android.hardware.SensorManager", classLoader, "getDefaultSensor", int.class, new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        Sensor sensor = (Sensor) param.getResult();
                        if (sensor == null) {
                            return;
                        }
                        int type = sensor.getType();
                        if (FakeInfo.getInstance().infoData.sensors.containsKey(type)) {
                            SensorData sensorData = FakeInfo.getInstance().infoData.sensors.get(type);
                            if (sensorData == null) {
                                return;
                            }
                            Reflect.on(sensor)
                                    .set("mType", sensorData.mType)
                                    .set("mName", sensorData.mName)
                                    .set("mVendor", sensorData.mVendor)
                                    .set("mVersion", sensorData.mVersion)
                                    .set("mMaxRange", sensorData.mMaxRange)
                                    .set("mResolution", sensorData.mResolution)
                                    .set("mPower", sensorData.mPower)
                                    .set("mMinDelay", sensorData.mMinDelay)
                                    .set("mMaxDelay", sensorData.mMaxDelay)
                                    .set("mStringType", sensorData.mStringType);
                        }
                    }
                }
        );

        XposedHelpers.findAndHookMethod("android.hardware.SensorManager", classLoader, "getSensorList", int.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                List<Sensor> sensorList = (List<Sensor>) param.getResult();
                if (sensorList == null) {
                    return;
                }
                for (Sensor sensor : sensorList) {
                    int type = sensor.getType();
                    if (FakeInfo.getInstance().infoData.sensors.containsKey(type)) {
                        SensorData sensorData = FakeInfo.getInstance().infoData.sensors.get(type);
                        if (sensorData == null) {
                            return;
                        }
                        Reflect.on(sensor)
                                .set("mType", sensorData.mType)
                                .set("mName", sensorData.mName)
                                .set("mVendor", sensorData.mVendor)
                                .set("mVersion", sensorData.mVersion)
                                .set("mMaxRange", sensorData.mMaxRange)
                                .set("mResolution", sensorData.mResolution)
                                .set("mPower", sensorData.mPower)
                                .set("mMinDelay", sensorData.mMinDelay)
                                .set("mMaxDelay", sensorData.mMaxDelay)
                                .set("mStringType", sensorData.mStringType);
                    }
                }
            }
        });


//        XposedHelpers.findAndHookMethod("android.app.ContextImpl", classLoader, "getSystemService", String.class, new XC_MethodHook() {
//            @Override
//            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//                super.afterHookedMethod(param);
//                Object result = param.getResult();
//                if (result == null) {
//                    return;
//                }
//                if (param.args[0].equals(Context.SENSOR_SERVICE)) {
//                    Log.d(TAG, "HOOK SENSOR " + result.getClass().getName());
//                    XposedHelpers.findAndHookMethod(param.getResult().getClass().getName(), classLoader, "getDefaultSensor", int.class, new XC_MethodHook() {
//                        @Override
//                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//                            super.afterHookedMethod(param);
//                        }
//                    });
//                }
//            }
//        });
    }
}
