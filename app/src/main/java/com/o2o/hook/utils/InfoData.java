package com.o2o.hook.utils;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;
import java.util.Map;

public class InfoData {
    @SerializedName("sensor")
    public Map<Integer, SensorData> sensors = new LinkedHashMap<>();

    @SerializedName("memInfo")
    public String memInfo;

    @SerializedName("cpuInfo")
    public String cpuInfo;

    public Fingerprint fingerprint = new Fingerprint();

    @SerializedName("properties")
    public Map<String, String> properties = new LinkedHashMap<>();
}
