package com.o2o.hook.utils;

import android.app.Application;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;
import com.o2o.tools.HttpTools;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Properties;

public class MoeMain {
    private static final String TAG = "EDXposed";


    synchronized public static void start(String packageName, String processName, Application application) {
        Log.e(TAG,"进入java层 修改机型数据开始~~~~~~" + Build.VERSION.SDK_INT);
        if (packageName.equals("com.sankuai.meituan") || packageName.equals("com.dianping.v1")) {
            // 初始化配置文件
            try {
                Log.e(TAG, "执行反射,修改安卓层面设备信息....");
                final File deviceInfo = new File(application.getFilesDir(), "device_info.json");
                if (!deviceInfo.exists()) {
                    Log.i(TAG, "设备信息不存在，正在创建");
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String data = HttpTools.Getcode("http://api.reacg.com:1080/device_info/" + Build.VERSION.SDK_INT, "UTF-8","","");
                                if (new JSONObject(data).optInt("code", 0) != 0) {
                                    Log.e(TAG, "创建设备信息失败:" + data);
                                    return;
                                }
                                FileOutputStream outputStream = new FileOutputStream(deviceInfo);
                                outputStream.write(data.getBytes());
                                outputStream.close();
                                Log.i(TAG, "创建设备信息完成:" + data);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e(TAG,"Error Execute "+e.getLocalizedMessage());
                            }
                        }
                    });
                    thread.start();
                    thread.join();
                    Log.i(TAG, "等待线程完成");
                    if (!deviceInfo.exists()) {
                        Log.e(TAG, "检查设备信息文件失败");
                    }
                }

                InputStream inputStream = new FileInputStream(deviceInfo);
                OutputStream outputStream = new ByteArrayOutputStream();
                int len;
                byte[] buffer = new byte[4096];
                while ((len = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, len);
                    outputStream.flush();
                }
                outputStream.close();
                InfoData infoData = new Gson().fromJson(outputStream.toString(), InfoData.class);
                FakeInfo.getInstance().setDeviceInfo(infoData);

                /////////////////////////////////////////////////////////////////////////////
                // 写 build.prop 文件 为so hook服务
                Properties properties = new Properties();
                for (Map.Entry<String, String> entry : FakeInfo.getInstance().infoData.properties.entrySet()) {
                    properties.setProperty(entry.getKey(), entry.getValue());
                }
                if (properties.size() > 0) {
                    File file = new File(application.getFilesDir(), "build.prop");
                    if (!file.exists()) {
                        properties.store(new FileOutputStream(file), null);
                        Log.d(TAG, "创建 build.prop 文件");
                    }
                }
                // 结束
                /////////////////////////////////////////////////////////////////////////////


                /////////////////////////////////////////////////////////////////////////////
                // 写入重定向数据
//                String cpuInfo = FakeInfo.getInstance().infoData.cpuInfo;
//                String memInfo = FakeInfo.getInstance().infoData.memInfo;
//                File forgeCpuinfoFile = new File(application.getFilesDir(), "cpuinfo");
//                if (!forgeCpuinfoFile.exists()) {
//                    FileUtils.writeToFile(cpuInfo.getBytes(), forgeCpuinfoFile);
//                    Log.d(TAG, "start nativeIORedirect /proc/cpuinfo");
//                }
//                File forgeMeminfoFile = new File(application.getFilesDir(), "meminfo");
//                if (!forgeMeminfoFile.exists()) {
//                    FileUtils.writeToFile(memInfo.getBytes(), forgeMeminfoFile);
//                    Log.d(TAG, "start nativeIORedirect /proc/meminfo");
//                }
                // 使重定向数据生效
//                NativeEngine.nativeIORedirect("/proc/cpuinfo", forgeCpuinfoFile.getPath());
//                NativeEngine.nativeIORedirect("/proc/meminfo", forgeMeminfoFile.getPath());
                // 结束重定向
                /////////////////////////////////////////////////////////////////////////////

                // 启用Java HOOK模块
                FakeInfo.getInstance().readInfo(packageName);
                FakeInfo.getInstance().loadProperty();

                VAHook[] vaHooks = new VAHook[]{
                        new FingerprintHook(),
                        new PropertyHook(),

                        // 修改传感器
                        new SensorHook(),

                        // 减少特征
                        new Cleanup(),

                        // 修改屏幕相关
                        new ScreenHook(),
                        new StorageHook(),
                };
                for (VAHook vaHook : vaHooks) {
                    try {
                        vaHook.inject(application.getClassLoader());
                    } catch (Exception e) {
                        Log.e(TAG, "Error: " + vaHook.getClass().getName(), e);
                    }
                }
                // 启用 SO HOOK 模块
//            NativeEngine.nativeStartHook();
            } catch (Exception e) {
                Log.e(TAG, "ERROR", e);
            }
        }else{
            return;
        }
    }
}
