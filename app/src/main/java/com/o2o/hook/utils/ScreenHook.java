package com.o2o.hook.utils;

import android.util.DisplayMetrics;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;

public class ScreenHook implements VAHook {
    @Override
    public void inject(ClassLoader classLoader) {
        // context.getResources().getDisplayMetrics().densityDpi);
        XposedHelpers.findAndHookMethod("android.content.res.Resources", classLoader, "getDisplayMetrics", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                DisplayMetrics dm = (DisplayMetrics) param.getResult();

                // 修改密度
//                dm.densityDpi = 440;
                // 修改分辨率
//                dm.widthPixels = 1080;
//                dm.heightPixels = 2120;
//                param.setResult(dm);
            }
        });
    }
}
