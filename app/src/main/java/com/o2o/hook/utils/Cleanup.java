package com.o2o.hook.utils;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;

// 减少风控特征

public class Cleanup implements VAHook {
    private static final String TAG = "X-LOG/Cleanup";
    @Override
    public void inject(ClassLoader classLoader) {
        // 不允许扫描附近WIFI
        XposedHelpers.findAndHookMethod("android.net.wifi.WifiManager", classLoader, "getScanResults", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                param.setResult(null);
            }
        });

        XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", classLoader, "getSimSerialNumber", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                param.setResult(null);
            }
        });
    }
}
