package com.o2o.hook.utils;

public interface VAHook {
    static final String TAG = "X-LOG/VAHook";
    void inject(ClassLoader classLoader);
}
