package com.jingdong.common.utils;

import android.app.Application;
import android.content.Context;

public class BitmapkitUtils {
    public static Application a = getApplicationByReflection();

    public static native String a(String... strArr);

    public static native byte[] encodeJni(byte[] bArr, boolean z);

    public static native String getSignFromJni(Context context, String str, String str2, String str3, String str4, String str5);

    public static native String getstring(String str);

    static {
        System.loadLibrary("jdbitmapkit");
    }

    private static Application getApplicationByReflection() {
        try {
            return (Application) Class.forName("android.app.ActivityThread").getMethod("currentApplication", new Class[0]).invoke(null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}