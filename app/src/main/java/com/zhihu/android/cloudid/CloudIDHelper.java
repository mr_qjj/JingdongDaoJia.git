package com.zhihu.android.cloudid;

public class CloudIDHelper {
    private static CloudIDHelper mCloudIDHelper;

    static {
        System.loadLibrary("encrypt");
    }
    public native String encrypt(String str, String str2, String str3, String str4, String str5, String str6, String str7);

    public native String encryptWithoutSecurekey(String str);

    private CloudIDHelper() {
    }

    public static CloudIDHelper getInstance() {
        if (mCloudIDHelper == null) {
            synchronized (CloudIDHelper.class) {
                if (mCloudIDHelper == null) {
                    mCloudIDHelper = new CloudIDHelper();
                }
            }
        }
        return mCloudIDHelper;
    }

}
