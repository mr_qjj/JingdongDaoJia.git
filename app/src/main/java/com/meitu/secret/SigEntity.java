package com.meitu.secret;

import android.content.Context;
import android.util.Log;

/**
 * 美图秀秀Sig签名处理
 */
public class SigEntity {
    private static final String SO_NAME = "release_sig";
    public String finalString;
    public String sig;
    public String sigTime;
    public String sigVersion;

    static {
        System.loadLibrary(SO_NAME);
    }
    public SigEntity(String str, String str2, String str3) {
        this.sigTime = str;
        this.sigVersion = str2;
        this.sig = str3;
    }

    public SigEntity(String str, String str2, String str3, String str4) {
        this.sigTime = str;
        this.sigVersion = str2;
        this.sig = str3;
        this.finalString = str4;
    }



    public static native SigEntity nativeGeneratorSig(String str, byte[][] bArr, String str2, Object obj);

    public static native SigEntity nativeGeneratorSigFinal(String str, byte[][] bArr, String str2, Object obj);

    public static native SigEntity nativeGeneratorSigOld(String str, byte[][] bArr, String str2);


    public static SigEntity generatorSig(String str, String[] strArr, String str2, Object obj) throws Exception {
        if (str == null || strArr == null || str2 == null || obj == null) {
            throw new Exception("path or params[] or appId or mContext must not be null.");
        } else if (obj instanceof Context) {
            byte[][] bArr = new byte[strArr.length][];
            for (int i = 0; i < strArr.length; i++) {
                if (strArr[i] == null) {
                    Log.e("SigEntity", str + " params[" + i + "] is null, encryption result by server maybe failed.");
                    strArr[i] = "";
                }
                bArr[i] = strArr[i].getBytes();
            }
            try {
                return nativeGeneratorSig(str, bArr, str2, obj);
            } catch (UnsatisfiedLinkError unused) {
                return nativeGeneratorSig(str, bArr, str2, obj);
            }
        } else {
            throw new Exception("mContext must be Context!");
        }
    }

    public static SigEntity generatorSig(String str, String[] strArr, String str2) throws Exception {
        if (str == null || strArr == null || str2 == null) {
            throw new Exception("path or params[] or appId must not be null.");
        }
        byte[][] bArr = new byte[strArr.length][];
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] == null) {
                Log.e("SigEntity", str + " params[" + i + "] is null, encryption result by server maybe failed.");
                strArr[i] = "";
            }
            bArr[i] = strArr[i].getBytes();
        }
        return nativeGeneratorSigOld(str, bArr, str2);
    }

    public static SigEntity generatorSigWithFinal(String str, String[] strArr, String str2, Object obj) throws Exception {
        if (str == null || strArr == null || str2 == null) {
            throw new Exception("path or params[] or appId must not be null.");
        }
        byte[][] bArr = new byte[strArr.length][];
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] == null) {
                Log.e("SigEntity", str + " params[" + i + "] is null, encryption result by server maybe failed.");
                strArr[i] = "";
            }
            bArr[i] = strArr[i].getBytes();
        }
        return nativeGeneratorSigFinal(str, bArr, str2, obj);
    }


}
