package cn.o2o.jingdongdaojia;

import android.Manifest;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.jingdong.common.utils.BitmapkitUtils;
import com.o2o.server.HttpServer;

import java.io.IOException;

import jd.net.z;

public class MainActivity extends AppCompatActivity {
    public static Context context = null;
    int port = 7788;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.activity_main);
//        String encryptStr = "{\"addrFilter\":\"1\",\"addressId\":\"0\",\"articleEssay\":\"1\",\"deviceidTail\":\"46\",\"exposedCount\":\"0\",\"gcLat\":\"39.912546\",\"gcLng\":\"116.460766\",\"imagesize\":{\"gridImg\":\"531x531\",\"listImg\":\"358x358\",\"longImg\":\"531x680\"},\"insertArticle\":\"1\",\"insertScene\":\"1\",\"insertedCount\":\"0\",\"isCorrect\":\"1\",\"keyword\":\"美妆\",\"latitude\":\"39.912546\",\"longitude\":\"116.460766\",\"newMiddleTag\":\"1\",\"newVersion\":\"3\",\"oneBoxMod\":\"1\",\"orignalSearch\":\"1\",\"orignalSelect\":\"1\",\"page\":\"2\",\"pageEntrance\":\"1\",\"pagesize\":\"10\",\"pvid\":\"\",\"searchVersionCode\":\"9170\",\"secondInsedCount\":\"0\",\"showShopTab\":\"yes\",\"showStoreTab\":\"1\",\"stock\":\"1\"}";
//        Log.e("EDXposed", BitmapkitUtils.getSignFromJni(MainActivity.this.getBaseContext(), "search", encryptStr, "869878047711287-98f62144e157", "android", "9.2.0"));
        HttpServer httpServer = new HttpServer("0.0.0.0",port);
        String message = "";
//        addContact("罐罐","18813054845");
//        moveContact();
        if (!httpServer.isAlive()) {
            try {
                httpServer.start();
            } catch (IOException e) {
                e.printStackTrace();
                message = e.getMessage();
            }
            System.out.println(Build.VERSION.RELEASE);
            Log.d(getClass().getName(), "onCreate: 启动http服务..." + httpServer.isAlive());
            TextView textView =(TextView) findViewById(R.id.text1);
            textView.setText("启动端口号"+port+"的http服务:" + (httpServer.isAlive()==true?"启动成功.":"启动失败("+message+")"));
        }
    }

    /**
     * 添加一条联系人
     * @param name
     * @param phoneNumber
     */
    public static void addContact(String name, String phoneNumber) {
        // 创建一个空的ContentValues
        ContentValues values = new ContentValues();

        // 向RawContacts.CONTENT_URI空值插入，
        // 先获取Android系统返回的rawContactId
        // 后面要基于此id插入值
        Uri rawContactUri = context.getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, values);
        long rawContactId = ContentUris.parseId(rawContactUri);
        values.clear();

        values.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
        // 内容类型
        values.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
        // 联系人名字
        values.put(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, name);
        // 向联系人URI添加联系人名字
        context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, values);
        values.clear();

        values.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
        values.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
        // 联系人的电话号码
        values.put(ContactsContract.CommonDataKinds.Phone.NUMBER, phoneNumber);
        // 电话类型
        values.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
        // 向联系人电话号码URI添加电话号码
        context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, values);
        values.clear();

        values.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
        values.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE);
        // 联系人的Email地址
//        values.put(Email.DATA, "zhangphil@xxx.com");
        // 电子邮件的类型
//        values.put(Email.TYPE, Email.TYPE_WORK);
        // 向联系人Email URI添加Email数据
        context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, values);
//        Toast.makeText(context, "联系人数据添加成功", Toast.LENGTH_SHORT).show();
    }

    /**
     * 删除所有联系人
     */
    public static void moveContact(){
        Cursor contactsCur = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        while(contactsCur.moveToNext()){
            //获取ID
            String rawId = contactsCur.getString(contactsCur.getColumnIndex(ContactsContract.Contacts._ID));
            //删除
            String where = ContactsContract.Data._ID  + " =?";
            String[] whereparams = new String[]{rawId};
            context.getContentResolver().delete(ContactsContract.RawContacts.CONTENT_URI, where, whereparams);
        }
    }
}
