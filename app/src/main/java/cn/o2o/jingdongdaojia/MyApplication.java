package cn.o2o.jingdongdaojia;
import android.app.Application;
import android.content.Context;
import android.util.Log;
import com.o2o.tools.IOUtils;

import java.net.SocketAddress;


public class MyApplication extends Application {
    private static Context context;
    public static String baiduConfigStr = "";
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        try {
            baiduConfigStr = getBaiduConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Context getContext(){
        return context;
    }

    public static String getBaiduConfig() throws Exception{
        String res = IOUtils.readStreamToString(context.getAssets().open("bdconfig.txt"));
        return res;
    }
}
