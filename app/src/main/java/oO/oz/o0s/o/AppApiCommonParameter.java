package oO.oz.o0s.o;

import android.app.Application;
import android.os.Build;
import android.text.TextUtils;

import com.o2o.tools.Utils;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.TreeMap;
import oO.oz.o0s.o.O0;

public class AppApiCommonParameter extends INetWorkCommonParasm {
    private final String uuid;

    public AppApiCommonParameter() {
        this.uuid = URLMd5.md5_32(com.o2o.tools.Utils.createRandomStr1(16)).substring(8,24);
    }

    private String createRequestId(long j, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(j + "");
        return MD5.md5_32(sb.toString());
    }

    private String paramsMd5(String str) {
        return MD5.md5_32("_sydd=" + "f3b6789eee74269d45c8f4d5d91c2d3b7c3ea5ff" + "_" + "9.0.1" + "&" + str);
    }

    private String paramsNewSign(String str) {
        try {
            return URLEncoder.encode(new O0("").basesec_EncDataToBase64(str, 2));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String paramsSign(String str) {
        System.currentTimeMillis();
        String md5_32 = URLMd5.md5_32("_sydd=" + "$2y$10$DcL7iAJaJj.4CzTYOdPQT.R1Rzm4LPEUYJBFusGPD7zzsXk2HA4UK" + "_" + "8.24.2" + "&" + str);
        return md5_32;
    }

    @Override // com.soyoung.library_network.base_api.INetWorkCommonParasm
    public TreeMap<String, String> getCommonParasm(HashMap<String, String> hashMap, String str) {
        return getCommonParasm(hashMap, str, true);
    }

    @Override // com.soyoung.library_network.base_api.INetWorkCommonParasm
    public TreeMap<String, String> getCommonParasm(HashMap<String, String> hashMap, String str, boolean z) {
        TreeMap<String, String> treeMap = new TreeMap<>();
        if (hashMap != null) {
            treeMap.putAll(hashMap);
        }

        if (TextUtils.isEmpty(treeMap.get("uid"))) {
            treeMap.put("uid", "0");
        }
        if (TextUtils.isEmpty(treeMap.get("lat"))) {
            treeMap.put("lat", "0");
        }
        if (TextUtils.isEmpty(treeMap.get("lng"))) {
            treeMap.put("lng", "0");
        }
        if (TextUtils.isEmpty(treeMap.get("cityId"))) {
            treeMap.put("cityId", "1");
        }
        treeMap.put("lver", "8.24.2");
        treeMap.put("pinyin", "soyoung");
        treeMap.put("sys", "2");
        treeMap.put("sdk_version", String.valueOf(Build.VERSION.SDK_INT));
        treeMap.put("device_os_version", String.valueOf(Build.VERSION.RELEASE));
        treeMap.put("device_model", Build.BRAND + "-" + Build.MODEL);
        treeMap.put("app_id", "2");
        treeMap.put("is_tf", "0");
        if (TextUtils.isEmpty(treeMap.get("page_display_unique_id"))) {
            treeMap.put("page_display_unique_id", MD5.md5_32(Utils.createRandomStr1(32)));
        }
        if (TextUtils.isEmpty(treeMap.get("device_id"))) {
            treeMap.put("device_id", MD5.md5_32(Utils.createRandomStr1(32)));
        }
        if (TextUtils.isEmpty(treeMap.get("xy_device_token"))) {
            treeMap.put("xy_device_token", "67" + MD5.md5_32(Utils.createRandomStr1(34)));
        }
        if (TextUtils.isEmpty(treeMap.get("ab_id"))) {
            treeMap.put("ab_id", MD5.md5_32(Utils.createRandomStr1(32).toUpperCase()).toUpperCase());
        }
        if (TextUtils.isEmpty(treeMap.get("uuid"))) {
            treeMap.put("uuid", this.uuid);
        }
        long currentTimeMillis = System.currentTimeMillis();
        treeMap.put("_time", (currentTimeMillis / 1000) + "");
        treeMap.put("request_id", createRequestId(currentTimeMillis, str));
        treeMap.put("vistor_uid", "0");
        treeMap.put("ab_id", MD5.md5_32(Utils.createRandomStr1(32).toUpperCase()).toUpperCase());
        if (z) {
            treeMap.put("xy_sign", paramsNewSign(paramsMd5(INetWorkCommonParasm.getParamsString(treeMap))));
            treeMap.put("_sign", paramsSign(INetWorkCommonParasm.getParamsString(treeMap)));
        }
        return treeMap;
    }
}