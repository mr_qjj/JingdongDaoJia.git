package oO.oz.o0s.o;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public abstract class INetWorkCommonParasm {
    @NonNull
    public static String getParamsString(Map<String, String> map) {
        if (map == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> next = it.next();
            String key = next.getKey();
            String value = next.getValue();
            if (!TextUtils.isEmpty(value)) {
                if ("adinfo".equalsIgnoreCase(key)) {
                    value = URLDecoder.decode(value);
                }
                sb.append("&");
                sb.append(key);
                sb.append("=");
                sb.append(value);
            } else {
                it.remove();
                map.remove(key);
            }
        }
        return sb.substring(1);
    }

    public abstract TreeMap<String, String> getCommonParasm(HashMap<String, String> hashMap, String str);

    public abstract TreeMap<String, String> getCommonParasm(HashMap<String, String> hashMap, String str, boolean z);
}