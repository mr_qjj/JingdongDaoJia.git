package oO.oz.o0s.o;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class URLMd5 {
    private static final String[] hexDigits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    public static String byteArrayToHexString(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            stringBuffer.append(byteToHexString(b));
        }
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:0:0x0000 */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: byte */
    /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r3v5, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    private static String byteToHexString(byte b) {
        if (b < 0) {
            b += 256;
        }
        int i = (b == 1 ? 1 : 0) / 16;
        return hexDigits[i] + hexDigits[b % 16];
    }

    public static String md5_16(String str) {
        String str2 = null;
        try {
            String str3 = new String(str);
            try {
                return byteArrayToHexString(MessageDigest.getInstance("MD5").digest(str3.getBytes()));
            } catch (Exception unused) {
                str2 = str3;
            }
        } catch (Exception unused2) {
            return str2;
        }
        return "";
    }

    public static String md5_32(String str) {
        String str2 = new String();
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer("");
            for (byte b : digest) {
                int i = b;
                if (b < 0) {
                    i = b + 256;
                }
                if (i < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toHexString(i == 1 ? 1 : 0));
            }
            str2 = stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return BCrypt.hashpw(str2, BCrypt.gensalt(5));
    }
}