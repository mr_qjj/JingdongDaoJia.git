package oO.oz.o0s.o;

import android.util.Base64;

public class O0 {
    private static final int ADD_LEN = 12;
    private static final int CTX_LEN = 16;
    public static final int DATA_TYPE_BANK_NUMBER = 3;
    public static final int DATA_TYPE_ID_NUMBER = 2;
    public static final int DATA_TYPE_LOG = 1;
    public static final int DATA_TYPE_PHONE_NUMBER = 4;
    public static final int DATA_TYPE_USER_NAME = 5;
    private static final int HEAD_LEN = 7;
    private static final int V1_1 = 1;
    protected static final char[] a = "0123456789ABCDEF".toCharArray();
    private byte[] ctx;

    static {
        System.loadLibrary("syoput");
    }

    public O0(String str) throws NullPointerException, Exception {
        this.ctx = null;
        this.ctx = new byte[16];
        if (str != null) {
            byte[] bArr = new byte[(str.length() + 7)];
            setVersion(bArr, 1);
            setCallNum(bArr, 0);
            try {
                setData(bArr, str.getBytes());
            } catch (Exception e) {
                e.printStackTrace();
            }
            int o0o = o0o(this.ctx, bArr, new byte[1], new int[1]);
            if (o0o != 0) {
                throw new Exception("" + o0o);
            }
            return;
        }
        throw new NullPointerException("license path null");
    }

    public static String ByteToBase64(byte[] bArr) throws Exception {
        return Base64.encodeToString(bArr, 2);
    }

    public static String ByteToHex(byte[] bArr) throws Exception {
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            int i2 = bArr[i] & 255;
            int i3 = i * 2;
            char[] cArr2 = a;
            cArr[i3] = cArr2[i2 >>> 4];
            cArr[i3 + 1] = cArr2[i2 & 15];
        }
        return new String(cArr);
    }

    private static native int o0o(byte[] bArr, byte[] bArr2, byte[] bArr3, int[] iArr);

    private static void setCallNum(byte[] bArr, int i) {
        bArr[1] = (byte) (i & 255);
    }

    private static void setData(byte[] bArr, byte[] bArr2) throws Exception {
        System.arraycopy(bArr2, 0, bArr, 7, bArr2.length);
        int length = bArr2.length;
        if (length <= 2147483635) {
            bArr[3] = (byte) ((length >> 24) & 255);
            bArr[4] = (byte) ((length >> 16) & 255);
            bArr[5] = (byte) ((length >> 8) & 255);
            bArr[6] = (byte) (length & 255);
            return;
        }
        throw new Exception("data too long");
    }

    private static void setOpt(byte[] bArr, byte b) {
        bArr[2] = b;
    }

    private static void setVersion(byte[] bArr, int i) {
        bArr[0] = (byte) (i & 255);
    }

    public String basesec_EncData(String str, int i) throws Exception {
        return basesec_EncDataToHex(str, i);
    }

    public String basesec_EncDataToBase64(String str, int i) throws Exception {
        return ByteToBase64(basesec_EncDataToBinary(str, i));
    }

    public byte[] basesec_EncDataToBinary(String str, int i) throws Exception {
        if (this.ctx == null) {
            throw new NullPointerException("not init yet");
        } else if (str == null || "".equals(str)) {
            throw new NullPointerException("input data is null or zero length");
        } else {
            try {
                byte[] bytes = str.getBytes("UTF-8");
                byte[] bArr = new byte[(bytes.length + 7)];
                int length = bytes.length + 12;
                byte[] bArr2 = new byte[length];
                int[] iArr = {bytes.length + 12};
                setVersion(bArr, 1);
                setCallNum(bArr, 1);
                setOpt(bArr, (byte) (i & 255));
                setData(bArr, bytes);
                int o0o = o0o(this.ctx, bArr, bArr2, iArr);
                if (o0o != 0 || length < iArr[0]) {
                    throw new Exception(Integer.toBinaryString(-o0o));
                }
                byte[] bArr3 = new byte[iArr[0]];
                System.arraycopy(bArr2, 0, bArr3, 0, iArr[0]);
                return bArr3;
            } catch (Exception unused) {
                throw new Exception("in data can not getbytes(UTF-8)");
            }
        }
    }

    public String basesec_EncDataToHex(String str, int i) throws Exception {
        return ByteToHex(basesec_EncDataToBinary(str, i));
    }
}